import { createApp } from 'vue'
import App from './App.vue'
import 'element-plus/dist/index.css'
// import 'element-plus/theme-chalk/el-message.css'

const app = createApp(App)

import router from './router'
app.use(router)
app.config.globalProperties.$routerAry = []

import ElementPlus from 'element-plus'
import zhCn from 'element-plus/es/locale/lang/zh-cn'
app.use(ElementPlus, {
  locale: zhCn,
})

import vue3videoPlay from "vue3-video-play"; // 引入组件
import "vue3-video-play/dist/style.css"; // 引入css
app.use(vue3videoPlay);


//global mixin
import elementMixin from './mixins/elementMixin.js'
app.mixin(elementMixin)
import routerMixin from './mixins/routerMixin.js'
app.mixin(routerMixin)
import dataMixin from './mixins/dataMixin.js'
app.mixin(dataMixin)
import hebeiMixin from './mixins/hebeiMixin.js'
app.mixin(hebeiMixin)

//ajax
import axiosReq from './utils/axiosReq.js'
app.config.globalProperties.$ajax = axiosReq
import utils from './utils/utils.js'
app.config.globalProperties.$utils = utils


import settings from './setting'
app.config.globalProperties.$setting = settings

//echarts
import * as echarts from 'echarts'
app.config.globalProperties.$echarts = echarts;

import Websocket from './websocket'
app.config.globalProperties.$Websocket = Websocket



//router
// import './permission'

app.mount('#app')
