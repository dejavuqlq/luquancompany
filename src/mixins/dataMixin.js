import settings from '../setting'
import { ref } from 'vue'
const mixin = {
    data() {
        let test = () =>{

        }
        return {
            globalUrl: settings.globalUrl, //这样就能直接用了
            chartdata: ref({}),
            globalData:{
                topicObj: {
                    repair_verify: {type: 'success',title: '物业报修已制定维修人员',message: '',url:'repair',params:{}},
                    repair_complete: {type: 'success',title: '物业报修已完成',message: '',url:'repair',params:{}},
                    job_apply: {type: 'success',title: '有人才预约您的企业',message: '',url:'applyfor',params:{}},
                    policy_add: {type: 'success',title: '有新的金融政策发布',message: '',url:'policy',params:{}},
                    banking_add: {type: 'success',title: '有新的金融产品发布',message: '',url:'banking',params:{}},
                    train_add: {type: 'success',title: '有新的培训课程发布',message: '',url:'train',params:{}},
                    video_add: {type: 'success',title: '有新的视频课程发布',message: '',url:'video',params:{}},
                    insurance_add: {type: 'success',title: '有新的保险产品发布',message: '',url:'insurance',params:{}},
                    talk_train: {type: 'success',title: '有新的培训课程咨询',message: '',url:'',params:{tabname:''}},
                    talk_banking: {type: 'success',title: '有新的金融产品咨询',message: '',url:'',params:{tabname:''}},
                    talk_insurance: {type: 'success',title: '有新的保险产品咨询',message: '',url:'',params:{tabname:''}},
                },
                sexAry: [
                    {value:"0",text:"未知"},
                    {value:"1",text:"男"},
                    {value:"2",text:"女"}
                ],
                stateTypeAry:[
                    {value:"0",text:"停用"},
                    {value:"1",text:"正常"}
                ],
                verifyTypeAry:[
                    {value:"0",text:"未审核"},
                    {value:"1",text:"已审核"}
                ],
                verifyTypeAry1:[
                    {value:"",text:"未报名"},
                    {value:"0",text:"待审核"},
                    {value:"1",text:"通过"},
                    {value:"2",text:"拒绝"}
                ],
                parkTypeAry:[
                    {value:"0",text:"不限"},
                    {value:"1",text:"固定"},
                    {value:"2",text:"非固定"}
                ],
                trainTypeAry:[
                    {value:"0",text:"线上"},
                    {value:"1",text:"线下"},
                ],
                issignTypeAry:[
                    {value:"0",text:"未报名"},
                    {value:"1",text:"已报名"},
                ],
                salaryAry:[
                    {value:[0,4000],text:"4千以下"},
                    {value:[4000,6000],text:"4千~6千"},
                    {value:[6000,8000],text:"6千~8千"},
                    {value:[8000,10000],text:"8千~1万"},
                    {value:[10000,15000],text:"1万~1.5万"},
                    {value:[15000,25000],text:"1.5万~2.5万"},
                    {value:[25000,35000],text:"2.5万~3.5万"},
                    {value:[35000,50000],text:"3.5万~5万"},
                    {value:[50000,9999999999],text:"5万以上"},
                ],
                workyearAry:[
                    {value:[0,1],text:"1年以下"},
                    {value:[1,3],text:"1～3年"},
                    {value:[3,5],text:"3～5年"},
                    {value:[5,10],text:"5～10年"},
                    {value:[10,200],text:"10年以上"},
                ],
                industryList:[
                    { "value": 100000000, "text": "互联网/IT/电子/通信"}, 
                    { "value": 400000000, "text": "房地产/建筑" }, 
                    { "value": 300000000, "text": "金融业" }, 
                    { "value": 1200000000, "text": "教育培训/科研" }, 
                    { "value": 900000000, "text": "广告/传媒/文化/体育" }, 
                    { "value": 1300000000, "text": "制药/医疗" }, 
                    { "value": 700000000, "text": "批发/零售/贸易" }, 
                    { "value": 500000000, "text": "制造业" }, 
                    { "value": 1600000000, "text": "汽车" },
                    { "value": 1000000000, "text": "交通运输/仓储/物流" }, 
                    { "value": 800000000, "text": "专业服务" }, 
                    { "value": 1500000000, "text": "生活服务" },
                    { "value": 1100000000, "text": "能源/环保/矿产" }, 
                    { "value": 1400000000, "text": "政府/非盈利机构" }, 
                    { "value": 600000000, "text": "农/林/牧/渔" }
                ],
                recruitTypes: [
                    { "value": '100000000', "text": "生活/服务业"}, 
                    { "value": '200000000', "text": "人力/行政/管理"},
                    { "value": '300000000', "text": "销售/客服/采购/电商"},
                    { "value": '400000000', "text": "酒店"},
                    { "value": '500000000', "text": "市场/媒介/广告/设计"},
                    { "value": '600000000', "text": "生产/物流/质控/汽车"},
                    { "value": '700000000', "text": "网络/通信/电子"},
                    { "value": '800000000', "text": "法律/教育/翻译/出版"},
                    { "value": '900000000', "text": "财会/金融/保险"},
                    { "value": '1000000000', "text": "医疗/制药/环保"},
                    { "value": '110000000', "text": "建筑/物业/其他/招聘会"},
                ],
                companyTypeList:[
                    { text:"类型一", value:0},
                    { text:"类型二", value:1}
                ],
                operatingStatusList:[
                    { text:"经营状态一", value:0},
                    { text:"经营状态二", value:1}
                ],
                nationList:[
                    {value:'1' ,text:'汉族'},
                    {value:'2' ,text:'蒙古族'},
                    {value:'3' ,text:'回族'},
                    {value:'4' ,text:'藏族'},
                    {value:'5' ,text:'维吾尔族'},
                    {value:'6' ,text:'苗族'},
                    {value:'7' ,text:'彝族'},
                    {value:'8' ,text:'壮族'},
                    {value:'9' ,text:'布依族'},
                    {value:'10',text:'朝鲜族'},
                    {value:'11',text:'满族'},
                    {value:'12',text:'侗族'},
                    {value:'13',text:'瑶族'},
                    {value:'14',text:'白族'},
                    {value:'15',text:'土家族'},
                    {value:'16',text:'哈尼族'},
                    {value:'17',text:'哈萨克族'},
                    {value:'18',text:'傣族'},
                    {value:'19',text:'黎族'},
                    {value:'20',text:'傈僳族'},
                    {value:'21',text:'佤族'},
                    {value:'22',text:'畲族'},
                    {value:'23',text:'高山族'},
                    {value:'24',text:'拉祜族'},
                    {value:'25',text:'水族'},
                    {value:'26',text:'东乡族'},
                    {value:'27',text:'纳西族'},
                    {value:'28',text:'景颇族'},
                    {value:'29',text:'柯尔克孜族'},
                    {value:'30',text:'土族'},
                    {value:'31',text:'达翰尔族'},
                    {value:'32',text:'么佬族'},
                    {value:'33',text:'羌族'},
                    {value:'34',text:'布朗族'},
                    {value:'35',text:'撒拉族'},
                    {value:'36',text:'毛南族'},
                    {value:'37',text:'仡佬族'},
                    {value:'38',text:'锡伯族'},
                    {value:'39',text:'阿昌族'},
                    {value:'40',text:'普米族'},
                    {value:'41',text:'塔吉克族'},
                    {value:'42',text:'怒族'},
                    {value:'43',text:'乌孜别克族'},
                    {value:'44',text:'俄罗斯族'},
                    {value:'45',text:'鄂温克族'},
                    {value:'46',text:'德昂族'},
                    {value:'47',text:'保安族'},
                    {value:'48',text:'裕固族'},
                    {value:'49',text:'京族'},
                    {value:'50',text:'塔塔尔族'},
                    {value:'51',text:'独龙族'},
                    {value:'52',text:'鄂伦春族'},
                    {value:'53',text:'赫哲族'},
                    {value:'54',text:'门巴族'},
                    {value:'55',text:'珞巴族'},
                    {value:'56',text:'基诺族'},
                ],
                eduList:[
                    {value:"1" ,text:'博士'},
                    {value:"2" ,text:'MBA/EMBA'},
                    {value:"3" ,text:'硕士'},
                    {value:"4" ,text:'本科'},
                    {value:"5" ,text:'大专'},
                    {value:"6" ,text:'中专/中技'},
                    {value:"7" ,text:'高中'},
                    {value:"8" ,text:'初中及以下'},
                ],
                jobTypeList:[
                    { text:"全职", value:'0'},
                    { text:"兼职", value:'1'},
                ],
                subjectTypeList:[
                    { text:"选择题", value:0},
                    { text:"判断题", value:1},
                ],
                houseTypeList:[
                    { text:"空置", value:0},
                    { text:"预定租", value:1},
                    { text:"预定买", value:2},
                    { text:"已租", value:3},
                    { text:"已售", value:4},
                ],
                staffTypeList:[
                    { text:"行政", value:0 },
                    { text:"保洁", value:1 },
                    { text:"安保", value:2 },
                    { text:"客服", value:3 },
                    { text:"维修", value:4 },
                ],
                priorityList:[
                    { text:"低级", value:0},
                    { text:"普通", value:1},
                    { text:"高级", value:2},
                ],
                timeTypeList:[
                    { text:"未开始", value:0},
                    { text:"进行中", value:1},
                    { text:"已完成", value:2},
                ],
                newsTargetList:[
                    { text:"个人用户", value:1 },
                    { text:"管委会", value:2 },
                    { text:"园区", value:4 },
                    { text:"企业", value:8 },
                    { text:"机构", value:16 },
                ],
                backUserTypeList:[ //这个暂时没用到
                    { text:"个人用户", value:1 }, //要登录小程序 小程序要加身份切换
                    { text:"总后台", value:2 },
                    { text:"管委会", value:4 },
                    { text:"园区", value:8 }, //员工要登录小程序
                    { text:"企业", value:16 }, //企业要登录小程序，企业员工也要登录小程序，还要支持身份切换
                    { text:"机构", value:32 },
                ],
                faultStateList:[ //维修状态
                    { text:"待处理", value:0 },
                    { text:"维修完成", value:1 },
                    { text:"维修失败", value:2 },
                ],
                borrowStateList:[
                    { text:"未还", value:0 },
                    { text:"在库", value:1 },
                ],
                repairStateList:[
                    { text:"维修中", value:0 },
                    { text:"正常", value:1 },
                ],
                financeTypeList:[
                    { text:"收入", value:0 },
                    { text:"支出", value:1 },
                ],
                orderTypeList:[
                    { text:"订水", value:0 },
                    { text:"订餐", value:1 },
                ],
                trainTypeList:[
                    { text:"线上", value:0 },
                    { text:"线下", value:1 }
                ],
                ismeTypeList:[
                    { text:"是", value:1 },
                    { text:"否", value:0 }
                ]
            }
        }
    },
    methods: {
    }
}
export default mixin