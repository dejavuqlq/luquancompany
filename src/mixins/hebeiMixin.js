const mixin = {
    data() {
        return {
            hebeicitys: [
                    {
                        "value": "1301",
                        "values": "130100000000",
                        "label": "石家庄市",
                        "children": [
                            {
                                "value": "130101",
                                "values": "130101000000",
                                "label": "市辖区",
                                "children": []
                            },
                            {
                                "value": "130102",
                                "values": "130102000000",
                                "label": "长安区",
                                "children": [
                                    {
                                        "value": "130102001",
                                        "values": "130102001000",
                                        "label": "建北街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130102002",
                                        "values": "130102002000",
                                        "label": "青园街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130102003",
                                        "values": "130102003000",
                                        "label": "广安街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130102004",
                                        "values": "130102004000",
                                        "label": "育才街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130102005",
                                        "values": "130102005000",
                                        "label": "跃进街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130102007",
                                        "values": "130102007000",
                                        "label": "河东街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130102010",
                                        "values": "130102010000",
                                        "label": "长丰街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130102011",
                                        "values": "130102011000",
                                        "label": "谈固街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130102012",
                                        "values": "130102012000",
                                        "label": "中山东路街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130102013",
                                        "values": "130102013000",
                                        "label": "阜康街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130102014",
                                        "values": "130102014000",
                                        "label": "建安街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130102015",
                                        "values": "130102015000",
                                        "label": "胜利北街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130102100",
                                        "values": "130102100000",
                                        "label": "西兆通镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130102101",
                                        "values": "130102101000",
                                        "label": "南村镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130102102",
                                        "values": "130102102000",
                                        "label": "高营镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130102103",
                                        "values": "130102103000",
                                        "label": "桃园镇",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130104",
                                "values": "130104000000",
                                "label": "桥西区",
                                "children": [
                                    {
                                        "value": "130104001",
                                        "values": "130104001000",
                                        "label": "东里街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130104002",
                                        "values": "130104002000",
                                        "label": "中山街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130104004",
                                        "values": "130104004000",
                                        "label": "南长街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130104005",
                                        "values": "130104005000",
                                        "label": "维明街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130104007",
                                        "values": "130104007000",
                                        "label": "友谊街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130104008",
                                        "values": "130104008000",
                                        "label": "红旗街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130104009",
                                        "values": "130104009000",
                                        "label": "新石街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130104010",
                                        "values": "130104010000",
                                        "label": "苑东街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130104011",
                                        "values": "130104011000",
                                        "label": "西里街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130104012",
                                        "values": "130104012000",
                                        "label": "振头街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130104013",
                                        "values": "130104013000",
                                        "label": "留营街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130104014",
                                        "values": "130104014000",
                                        "label": "长兴街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130104015",
                                        "values": "130104015000",
                                        "label": "彭后街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130104016",
                                        "values": "130104016000",
                                        "label": "东风街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130104017",
                                        "values": "130104017000",
                                        "label": "东华街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130104018",
                                        "values": "130104018000",
                                        "label": "休门街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130104019",
                                        "values": "130104019000",
                                        "label": "汇通街道",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130105",
                                "values": "130105000000",
                                "label": "新华区",
                                "children": [
                                    {
                                        "value": "130105001",
                                        "values": "130105001000",
                                        "label": "革新街街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130105003",
                                        "values": "130105003000",
                                        "label": "新华路街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130105004",
                                        "values": "130105004000",
                                        "label": "宁安街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130105005",
                                        "values": "130105005000",
                                        "label": "东焦街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130105006",
                                        "values": "130105006000",
                                        "label": "西苑街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130105007",
                                        "values": "130105007000",
                                        "label": "合作路街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130105008",
                                        "values": "130105008000",
                                        "label": "联盟街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130105009",
                                        "values": "130105009000",
                                        "label": "石岗街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130105011",
                                        "values": "130105011000",
                                        "label": "天苑街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130105012",
                                        "values": "130105012000",
                                        "label": "北苑街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130105013",
                                        "values": "130105013000",
                                        "label": "赵陵铺路街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130105014",
                                        "values": "130105014000",
                                        "label": "西三庄街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130105015",
                                        "values": "130105015000",
                                        "label": "大郭街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130105016",
                                        "values": "130105016000",
                                        "label": "杜北街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130105017",
                                        "values": "130105017000",
                                        "label": "赵佗路街道",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130107",
                                "values": "130107000000",
                                "label": "井陉矿区",
                                "children": [
                                    {
                                        "value": "130107001",
                                        "values": "130107001000",
                                        "label": "矿市街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130107002",
                                        "values": "130107002000",
                                        "label": "四微街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130107100",
                                        "values": "130107100000",
                                        "label": "贾庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130107101",
                                        "values": "130107101000",
                                        "label": "凤山镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130107200",
                                        "values": "130107200000",
                                        "label": "横涧乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130108",
                                "values": "130108000000",
                                "label": "裕华区",
                                "children": [
                                    {
                                        "value": "130108001",
                                        "values": "130108001000",
                                        "label": "裕兴街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130108002",
                                        "values": "130108002000",
                                        "label": "裕强街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130108003",
                                        "values": "130108003000",
                                        "label": "东苑街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130108004",
                                        "values": "130108004000",
                                        "label": "建通街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130108005",
                                        "values": "130108005000",
                                        "label": "槐底街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130108006",
                                        "values": "130108006000",
                                        "label": "裕华路街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130108007",
                                        "values": "130108007000",
                                        "label": "裕东街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130108008",
                                        "values": "130108008000",
                                        "label": "裕翔街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130108009",
                                        "values": "130108009000",
                                        "label": "建华南街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130108101",
                                        "values": "130108101000",
                                        "label": "方村镇",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130109",
                                "values": "130109000000",
                                "label": "藁城区",
                                "children": [
                                    {
                                        "value": "130109100",
                                        "values": "130109100000",
                                        "label": "廉州镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130109101",
                                        "values": "130109101000",
                                        "label": "兴安镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130109102",
                                        "values": "130109102000",
                                        "label": "贾市庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130109103",
                                        "values": "130109103000",
                                        "label": "南营镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130109104",
                                        "values": "130109104000",
                                        "label": "梅花镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130109105",
                                        "values": "130109105000",
                                        "label": "岗上镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130109107",
                                        "values": "130109107000",
                                        "label": "南董镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130109108",
                                        "values": "130109108000",
                                        "label": "张家庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130109109",
                                        "values": "130109109000",
                                        "label": "南孟镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130109110",
                                        "values": "130109110000",
                                        "label": "增村镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130109111",
                                        "values": "130109111000",
                                        "label": "常安镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130109112",
                                        "values": "130109112000",
                                        "label": "西关镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130109200",
                                        "values": "130109200000",
                                        "label": "九门回族乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130109500",
                                        "values": "130109500000",
                                        "label": "石家庄经济技术开发区",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130110",
                                "values": "130110000000",
                                "label": "鹿泉区",
                                "children": [
                                    {
                                        "value": "130110100",
                                        "values": "130110100000",
                                        "label": "获鹿镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130110101",
                                        "values": "130110101000",
                                        "label": "铜冶镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130110102",
                                        "values": "130110102000",
                                        "label": "寺家庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130110103",
                                        "values": "130110103000",
                                        "label": "上庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130110104",
                                        "values": "130110104000",
                                        "label": "李村镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130110105",
                                        "values": "130110105000",
                                        "label": "宜安镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130110106",
                                        "values": "130110106000",
                                        "label": "黄壁庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130110107",
                                        "values": "130110107000",
                                        "label": "大河镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130110108",
                                        "values": "130110108000",
                                        "label": "山尹村镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130110200",
                                        "values": "130110200000",
                                        "label": "石井乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130110201",
                                        "values": "130110201000",
                                        "label": "白鹿泉乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130110202",
                                        "values": "130110202000",
                                        "label": "上寨乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130110500",
                                        "values": "130110500000",
                                        "label": "河北鹿泉经济开发区",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130111",
                                "values": "130111000000",
                                "label": "栾城区",
                                "children": [
                                    {
                                        "value": "130111100",
                                        "values": "130111100000",
                                        "label": "栾城镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130111103",
                                        "values": "130111103000",
                                        "label": "冶河镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130111104",
                                        "values": "130111104000",
                                        "label": "窦妪镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130111105",
                                        "values": "130111105000",
                                        "label": "楼底镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130111200",
                                        "values": "130111200000",
                                        "label": "南高乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130111201",
                                        "values": "130111201000",
                                        "label": "柳林屯乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130111202",
                                        "values": "130111202000",
                                        "label": "西营乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130121",
                                "values": "130121000000",
                                "label": "井陉县",
                                "children": [
                                    {
                                        "value": "130121100",
                                        "values": "130121100000",
                                        "label": "微水镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130121101",
                                        "values": "130121101000",
                                        "label": "上安镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130121102",
                                        "values": "130121102000",
                                        "label": "天长镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130121103",
                                        "values": "130121103000",
                                        "label": "秀林镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130121104",
                                        "values": "130121104000",
                                        "label": "南峪镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130121105",
                                        "values": "130121105000",
                                        "label": "威州镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130121106",
                                        "values": "130121106000",
                                        "label": "小作镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130121107",
                                        "values": "130121107000",
                                        "label": "南障城镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130121108",
                                        "values": "130121108000",
                                        "label": "苍岩山镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130121109",
                                        "values": "130121109000",
                                        "label": "测鱼镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130121200",
                                        "values": "130121200000",
                                        "label": "吴家窑乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130121201",
                                        "values": "130121201000",
                                        "label": "北正乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130121202",
                                        "values": "130121202000",
                                        "label": "于家乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130121203",
                                        "values": "130121203000",
                                        "label": "孙庄乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130121204",
                                        "values": "130121204000",
                                        "label": "南陉乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130121205",
                                        "values": "130121205000",
                                        "label": "辛庄乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130121206",
                                        "values": "130121206000",
                                        "label": "南王庄乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130123",
                                "values": "130123000000",
                                "label": "正定县",
                                "children": [
                                    {
                                        "value": "130123001",
                                        "values": "130123001000",
                                        "label": "诸福屯街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130123002",
                                        "values": "130123002000",
                                        "label": "三里屯街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130123100",
                                        "values": "130123100000",
                                        "label": "正定镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130123102",
                                        "values": "130123102000",
                                        "label": "新城铺镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130123103",
                                        "values": "130123103000",
                                        "label": "新安镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130123200",
                                        "values": "130123200000",
                                        "label": "南牛乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130123201",
                                        "values": "130123201000",
                                        "label": "南楼乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130123202",
                                        "values": "130123202000",
                                        "label": "西平乐乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130123203",
                                        "values": "130123203000",
                                        "label": "北早现乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130123204",
                                        "values": "130123204000",
                                        "label": "曲阳桥乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130125",
                                "values": "130125000000",
                                "label": "行唐县",
                                "children": [
                                    {
                                        "value": "130125100",
                                        "values": "130125100000",
                                        "label": "龙州镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130125101",
                                        "values": "130125101000",
                                        "label": "南桥镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130125102",
                                        "values": "130125102000",
                                        "label": "上碑镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130125103",
                                        "values": "130125103000",
                                        "label": "口头镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130125200",
                                        "values": "130125200000",
                                        "label": "独羊岗乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130125201",
                                        "values": "130125201000",
                                        "label": "安香乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130125202",
                                        "values": "130125202000",
                                        "label": "只里乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130125203",
                                        "values": "130125203000",
                                        "label": "市同乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130125204",
                                        "values": "130125204000",
                                        "label": "翟营乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130125205",
                                        "values": "130125205000",
                                        "label": "城寨乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130125206",
                                        "values": "130125206000",
                                        "label": "上方乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130125207",
                                        "values": "130125207000",
                                        "label": "玉亭乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130125208",
                                        "values": "130125208000",
                                        "label": "北河乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130125209",
                                        "values": "130125209000",
                                        "label": "上闫庄乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130125210",
                                        "values": "130125210000",
                                        "label": "九口子乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130125400",
                                        "values": "130125400000",
                                        "label": "开发区",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130126",
                                "values": "130126000000",
                                "label": "灵寿县",
                                "children": [
                                    {
                                        "value": "130126100",
                                        "values": "130126100000",
                                        "label": "灵寿镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130126101",
                                        "values": "130126101000",
                                        "label": "青同镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130126102",
                                        "values": "130126102000",
                                        "label": "塔上镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130126103",
                                        "values": "130126103000",
                                        "label": "陈庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130126104",
                                        "values": "130126104000",
                                        "label": "慈峪镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130126105",
                                        "values": "130126105000",
                                        "label": "岔头镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130126200",
                                        "values": "130126200000",
                                        "label": "三圣院乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130126201",
                                        "values": "130126201000",
                                        "label": "北洼乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130126202",
                                        "values": "130126202000",
                                        "label": "牛城乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130126203",
                                        "values": "130126203000",
                                        "label": "狗台乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130126204",
                                        "values": "130126204000",
                                        "label": "南寨乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130126205",
                                        "values": "130126205000",
                                        "label": "南燕川乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130126206",
                                        "values": "130126206000",
                                        "label": "北谭庄乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130126207",
                                        "values": "130126207000",
                                        "label": "寨头乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130126208",
                                        "values": "130126208000",
                                        "label": "南营乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130127",
                                "values": "130127000000",
                                "label": "高邑县",
                                "children": [
                                    {
                                        "value": "130127100",
                                        "values": "130127100000",
                                        "label": "高邑镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130127101",
                                        "values": "130127101000",
                                        "label": "大营镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130127102",
                                        "values": "130127102000",
                                        "label": "富村镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130127103",
                                        "values": "130127103000",
                                        "label": "万城镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130127201",
                                        "values": "130127201000",
                                        "label": "中韩乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130128",
                                "values": "130128000000",
                                "label": "深泽县",
                                "children": [
                                    {
                                        "value": "130128100",
                                        "values": "130128100000",
                                        "label": "深泽镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130128101",
                                        "values": "130128101000",
                                        "label": "铁杆镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130128102",
                                        "values": "130128102000",
                                        "label": "赵八镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130128200",
                                        "values": "130128200000",
                                        "label": "白庄乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130128201",
                                        "values": "130128201000",
                                        "label": "留村乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130128203",
                                        "values": "130128203000",
                                        "label": "桥头乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130129",
                                "values": "130129000000",
                                "label": "赞皇县",
                                "children": [
                                    {
                                        "value": "130129100",
                                        "values": "130129100000",
                                        "label": "赞皇镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130129101",
                                        "values": "130129101000",
                                        "label": "院头镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130129102",
                                        "values": "130129102000",
                                        "label": "南邢郭镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130129103",
                                        "values": "130129103000",
                                        "label": "嶂石岩镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130129200",
                                        "values": "130129200000",
                                        "label": "西龙门乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130129202",
                                        "values": "130129202000",
                                        "label": "南清河乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130129203",
                                        "values": "130129203000",
                                        "label": "西阳泽乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130129204",
                                        "values": "130129204000",
                                        "label": "土门乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130129205",
                                        "values": "130129205000",
                                        "label": "黄北坪乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130129207",
                                        "values": "130129207000",
                                        "label": "许亭乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130129208",
                                        "values": "130129208000",
                                        "label": "张楞乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130130",
                                "values": "130130000000",
                                "label": "无极县",
                                "children": [
                                    {
                                        "value": "130130100",
                                        "values": "130130100000",
                                        "label": "无极镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130130101",
                                        "values": "130130101000",
                                        "label": "七汲镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130130102",
                                        "values": "130130102000",
                                        "label": "张段固镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130130103",
                                        "values": "130130103000",
                                        "label": "北苏镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130130104",
                                        "values": "130130104000",
                                        "label": "郭庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130130105",
                                        "values": "130130105000",
                                        "label": "大陈镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130130200",
                                        "values": "130130200000",
                                        "label": "高头回族乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130130201",
                                        "values": "130130201000",
                                        "label": "郝庄乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130130202",
                                        "values": "130130202000",
                                        "label": "东侯坊乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130130203",
                                        "values": "130130203000",
                                        "label": "里城道乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130130204",
                                        "values": "130130204000",
                                        "label": "南流乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130131",
                                "values": "130131000000",
                                "label": "平山县",
                                "children": [
                                    {
                                        "value": "130131100",
                                        "values": "130131100000",
                                        "label": "平山镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130131101",
                                        "values": "130131101000",
                                        "label": "东回舍镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130131102",
                                        "values": "130131102000",
                                        "label": "温塘镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130131103",
                                        "values": "130131103000",
                                        "label": "南甸镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130131104",
                                        "values": "130131104000",
                                        "label": "岗南镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130131105",
                                        "values": "130131105000",
                                        "label": "古月镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130131106",
                                        "values": "130131106000",
                                        "label": "下槐镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130131107",
                                        "values": "130131107000",
                                        "label": "孟家庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130131108",
                                        "values": "130131108000",
                                        "label": "小觉镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130131109",
                                        "values": "130131109000",
                                        "label": "蛟潭庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130131110",
                                        "values": "130131110000",
                                        "label": "西柏坡镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130131111",
                                        "values": "130131111000",
                                        "label": "下口镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130131200",
                                        "values": "130131200000",
                                        "label": "西大吾乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130131201",
                                        "values": "130131201000",
                                        "label": "上三汲乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130131202",
                                        "values": "130131202000",
                                        "label": "两河乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130131203",
                                        "values": "130131203000",
                                        "label": "东王坡乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130131204",
                                        "values": "130131204000",
                                        "label": "苏家庄乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130131205",
                                        "values": "130131205000",
                                        "label": "宅北乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130131207",
                                        "values": "130131207000",
                                        "label": "北冶乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130131209",
                                        "values": "130131209000",
                                        "label": "上观音堂乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130131210",
                                        "values": "130131210000",
                                        "label": "杨家桥乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130131211",
                                        "values": "130131211000",
                                        "label": "营里乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130131212",
                                        "values": "130131212000",
                                        "label": "合河口乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130132",
                                "values": "130132000000",
                                "label": "元氏县",
                                "children": [
                                    {
                                        "value": "130132001",
                                        "values": "130132001000",
                                        "label": "城区街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130132100",
                                        "values": "130132100000",
                                        "label": "槐阳镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130132101",
                                        "values": "130132101000",
                                        "label": "殷村镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130132102",
                                        "values": "130132102000",
                                        "label": "南佐镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130132103",
                                        "values": "130132103000",
                                        "label": "宋曹镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130132104",
                                        "values": "130132104000",
                                        "label": "南因镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130132105",
                                        "values": "130132105000",
                                        "label": "姬村镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130132106",
                                        "values": "130132106000",
                                        "label": "北褚镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130132107",
                                        "values": "130132107000",
                                        "label": "马村镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130132201",
                                        "values": "130132201000",
                                        "label": "东张乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130132202",
                                        "values": "130132202000",
                                        "label": "赵同乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130132204",
                                        "values": "130132204000",
                                        "label": "苏村乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130132205",
                                        "values": "130132205000",
                                        "label": "苏阳乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130132206",
                                        "values": "130132206000",
                                        "label": "北正乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130132207",
                                        "values": "130132207000",
                                        "label": "前仙乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130132208",
                                        "values": "130132208000",
                                        "label": "黑水河乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130133",
                                "values": "130133000000",
                                "label": "赵县",
                                "children": [
                                    {
                                        "value": "130133100",
                                        "values": "130133100000",
                                        "label": "赵州镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130133101",
                                        "values": "130133101000",
                                        "label": "范庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130133102",
                                        "values": "130133102000",
                                        "label": "北王里镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130133103",
                                        "values": "130133103000",
                                        "label": "新寨店镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130133104",
                                        "values": "130133104000",
                                        "label": "韩村镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130133105",
                                        "values": "130133105000",
                                        "label": "南柏舍镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130133106",
                                        "values": "130133106000",
                                        "label": "沙河店镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130133200",
                                        "values": "130133200000",
                                        "label": "前大章乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130133201",
                                        "values": "130133201000",
                                        "label": "谢庄乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130133202",
                                        "values": "130133202000",
                                        "label": "高村乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130133203",
                                        "values": "130133203000",
                                        "label": "王西章乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130171",
                                "values": "130171000000",
                                "label": "石家庄高新技术产业开发区",
                                "children": [
                                    {
                                        "value": "130171010",
                                        "values": "130171010000",
                                        "label": "长江街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130171011",
                                        "values": "130171011000",
                                        "label": "太行街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130171100",
                                        "values": "130171100000",
                                        "label": "宋营镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130171101",
                                        "values": "130171101000",
                                        "label": "郄马镇",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130172",
                                "values": "130172000000",
                                "label": "石家庄循环化工园区",
                                "children": [
                                    {
                                        "value": "130172100",
                                        "values": "130172100000",
                                        "label": "丘头镇",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130181",
                                "values": "130181000000",
                                "label": "辛集市",
                                "children": [
                                    {
                                        "value": "130181100",
                                        "values": "130181100000",
                                        "label": "辛集镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130181101",
                                        "values": "130181101000",
                                        "label": "旧城镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130181102",
                                        "values": "130181102000",
                                        "label": "张古庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130181103",
                                        "values": "130181103000",
                                        "label": "位伯镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130181104",
                                        "values": "130181104000",
                                        "label": "新垒头镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130181105",
                                        "values": "130181105000",
                                        "label": "新城镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130181106",
                                        "values": "130181106000",
                                        "label": "南智邱镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130181107",
                                        "values": "130181107000",
                                        "label": "王口镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130181200",
                                        "values": "130181200000",
                                        "label": "天宫营乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130181201",
                                        "values": "130181201000",
                                        "label": "前营乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130181202",
                                        "values": "130181202000",
                                        "label": "马庄乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130181203",
                                        "values": "130181203000",
                                        "label": "和睦井乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130181204",
                                        "values": "130181204000",
                                        "label": "田家庄乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130181205",
                                        "values": "130181205000",
                                        "label": "中里厢乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130181206",
                                        "values": "130181206000",
                                        "label": "小辛庄乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130181500",
                                        "values": "130181500000",
                                        "label": "辛集经济开发区",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130183",
                                "values": "130183000000",
                                "label": "晋州市",
                                "children": [
                                    {
                                        "value": "130183100",
                                        "values": "130183100000",
                                        "label": "晋州镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130183101",
                                        "values": "130183101000",
                                        "label": "总十庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130183102",
                                        "values": "130183102000",
                                        "label": "营里镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130183103",
                                        "values": "130183103000",
                                        "label": "桃园镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130183104",
                                        "values": "130183104000",
                                        "label": "东卓宿镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130183105",
                                        "values": "130183105000",
                                        "label": "马于镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130183106",
                                        "values": "130183106000",
                                        "label": "小樵镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130183107",
                                        "values": "130183107000",
                                        "label": "槐树镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130183108",
                                        "values": "130183108000",
                                        "label": "东里庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130183201",
                                        "values": "130183201000",
                                        "label": "周家庄乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130184",
                                "values": "130184000000",
                                "label": "新乐市",
                                "children": [
                                    {
                                        "value": "130184001",
                                        "values": "130184001000",
                                        "label": "长寿街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130184101",
                                        "values": "130184101000",
                                        "label": "化皮镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130184102",
                                        "values": "130184102000",
                                        "label": "承安镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130184103",
                                        "values": "130184103000",
                                        "label": "正莫镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130184104",
                                        "values": "130184104000",
                                        "label": "南大岳镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130184105",
                                        "values": "130184105000",
                                        "label": "杜固镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130184106",
                                        "values": "130184106000",
                                        "label": "邯邰镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130184107",
                                        "values": "130184107000",
                                        "label": "东王镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130184108",
                                        "values": "130184108000",
                                        "label": "马头铺镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130184200",
                                        "values": "130184200000",
                                        "label": "协神乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130184201",
                                        "values": "130184201000",
                                        "label": "木村乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130184202",
                                        "values": "130184202000",
                                        "label": "彭家庄回族乡",
                                        "children": []
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "value": "1302",
                        "values": "130200000000",
                        "label": "唐山市",
                        "children": [
                            {
                                "value": "130201",
                                "values": "130201000000",
                                "label": "市辖区",
                                "children": []
                            },
                            {
                                "value": "130202",
                                "values": "130202000000",
                                "label": "路南区",
                                "children": [
                                    {
                                        "value": "130202001",
                                        "values": "130202001000",
                                        "label": "学院南路街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130202002",
                                        "values": "130202002000",
                                        "label": "友谊街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130202003",
                                        "values": "130202003000",
                                        "label": "广场街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130202004",
                                        "values": "130202004000",
                                        "label": "永红桥街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130202005",
                                        "values": "130202005000",
                                        "label": "小山街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130202006",
                                        "values": "130202006000",
                                        "label": "文北街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130202007",
                                        "values": "130202007000",
                                        "label": "钱营街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130202008",
                                        "values": "130202008000",
                                        "label": "惠民道街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130202009",
                                        "values": "130202009000",
                                        "label": "梁家屯路街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130202102",
                                        "values": "130202102000",
                                        "label": "稻地镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130202200",
                                        "values": "130202200000",
                                        "label": "女织寨乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130203",
                                "values": "130203000000",
                                "label": "路北区",
                                "children": [
                                    {
                                        "value": "130203001",
                                        "values": "130203001000",
                                        "label": "乔屯街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130203002",
                                        "values": "130203002000",
                                        "label": "文化路街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130203003",
                                        "values": "130203003000",
                                        "label": "钓鱼台街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130203004",
                                        "values": "130203004000",
                                        "label": "东新村街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130203005",
                                        "values": "130203005000",
                                        "label": "缸窑街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130203006",
                                        "values": "130203006000",
                                        "label": "机场路街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130203007",
                                        "values": "130203007000",
                                        "label": "河北路街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130203008",
                                        "values": "130203008000",
                                        "label": "龙东街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130203009",
                                        "values": "130203009000",
                                        "label": "大里街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130203010",
                                        "values": "130203010000",
                                        "label": "光明街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130203011",
                                        "values": "130203011000",
                                        "label": "翔云道街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130203100",
                                        "values": "130203100000",
                                        "label": "韩城镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130203200",
                                        "values": "130203200000",
                                        "label": "果园乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130204",
                                "values": "130204000000",
                                "label": "古冶区",
                                "children": [
                                    {
                                        "value": "130204001",
                                        "values": "130204001000",
                                        "label": "林西街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130204002",
                                        "values": "130204002000",
                                        "label": "唐家庄街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130204003",
                                        "values": "130204003000",
                                        "label": "古冶街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130204004",
                                        "values": "130204004000",
                                        "label": "赵各庄街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130204007",
                                        "values": "130204007000",
                                        "label": "京华街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130204100",
                                        "values": "130204100000",
                                        "label": "范各庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130204101",
                                        "values": "130204101000",
                                        "label": "卑家店镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130204201",
                                        "values": "130204201000",
                                        "label": "王辇庄乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130204202",
                                        "values": "130204202000",
                                        "label": "习家套乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130204203",
                                        "values": "130204203000",
                                        "label": "大庄坨乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130205",
                                "values": "130205000000",
                                "label": "开平区",
                                "children": [
                                    {
                                        "value": "130205001",
                                        "values": "130205001000",
                                        "label": "马家沟街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130205002",
                                        "values": "130205002000",
                                        "label": "开平街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130205003",
                                        "values": "130205003000",
                                        "label": "税务庄街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130205005",
                                        "values": "130205005000",
                                        "label": "陡电街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130205006",
                                        "values": "130205006000",
                                        "label": "荆各庄街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130205100",
                                        "values": "130205100000",
                                        "label": "开平镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130205101",
                                        "values": "130205101000",
                                        "label": "栗园镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130205102",
                                        "values": "130205102000",
                                        "label": "郑庄子镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130205103",
                                        "values": "130205103000",
                                        "label": "双桥镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130205104",
                                        "values": "130205104000",
                                        "label": "洼里镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130205105",
                                        "values": "130205105000",
                                        "label": "越河镇",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130207",
                                "values": "130207000000",
                                "label": "丰南区",
                                "children": [
                                    {
                                        "value": "130207103",
                                        "values": "130207103000",
                                        "label": "小集镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130207104",
                                        "values": "130207104000",
                                        "label": "黄各庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130207105",
                                        "values": "130207105000",
                                        "label": "西葛镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130207106",
                                        "values": "130207106000",
                                        "label": "大新庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130207107",
                                        "values": "130207107000",
                                        "label": "钱营镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130207108",
                                        "values": "130207108000",
                                        "label": "唐坊镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130207109",
                                        "values": "130207109000",
                                        "label": "王兰庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130207110",
                                        "values": "130207110000",
                                        "label": "柳树酄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130207111",
                                        "values": "130207111000",
                                        "label": "黑沿子镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130207113",
                                        "values": "130207113000",
                                        "label": "胥各庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130207114",
                                        "values": "130207114000",
                                        "label": "大齐各庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130207115",
                                        "values": "130207115000",
                                        "label": "岔河镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130207201",
                                        "values": "130207201000",
                                        "label": "南孙庄乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130207202",
                                        "values": "130207202000",
                                        "label": "东田庄乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130207203",
                                        "values": "130207203000",
                                        "label": "尖字沽乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130208",
                                "values": "130208000000",
                                "label": "丰润区",
                                "children": [
                                    {
                                        "value": "130208001",
                                        "values": "130208001000",
                                        "label": "太平路街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130208002",
                                        "values": "130208002000",
                                        "label": "燕山路街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130208003",
                                        "values": "130208003000",
                                        "label": "浭阳街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130208100",
                                        "values": "130208100000",
                                        "label": "丰润镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130208102",
                                        "values": "130208102000",
                                        "label": "任各庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130208103",
                                        "values": "130208103000",
                                        "label": "左家坞镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130208104",
                                        "values": "130208104000",
                                        "label": "泉河头镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130208105",
                                        "values": "130208105000",
                                        "label": "王官营镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130208106",
                                        "values": "130208106000",
                                        "label": "火石营镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130208109",
                                        "values": "130208109000",
                                        "label": "新军屯镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130208110",
                                        "values": "130208110000",
                                        "label": "小张各庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130208111",
                                        "values": "130208111000",
                                        "label": "丰登坞镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130208112",
                                        "values": "130208112000",
                                        "label": "李钊庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130208113",
                                        "values": "130208113000",
                                        "label": "白官屯镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130208114",
                                        "values": "130208114000",
                                        "label": "石各庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130208115",
                                        "values": "130208115000",
                                        "label": "沙流河镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130208116",
                                        "values": "130208116000",
                                        "label": "七树庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130208117",
                                        "values": "130208117000",
                                        "label": "杨官林镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130208118",
                                        "values": "130208118000",
                                        "label": "银城铺镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130208119",
                                        "values": "130208119000",
                                        "label": "常庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130208202",
                                        "values": "130208202000",
                                        "label": "姜家营乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130208205",
                                        "values": "130208205000",
                                        "label": "欢喜庄乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130208208",
                                        "values": "130208208000",
                                        "label": "刘家营乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130209",
                                "values": "130209000000",
                                "label": "曹妃甸区",
                                "children": [
                                    {
                                        "value": "130209100",
                                        "values": "130209100000",
                                        "label": "唐海镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130209112",
                                        "values": "130209112000",
                                        "label": "滨海镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130209113",
                                        "values": "130209113000",
                                        "label": "柳赞镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130209401",
                                        "values": "130209401000",
                                        "label": "一农场",
                                        "children": []
                                    },
                                    {
                                        "value": "130209403",
                                        "values": "130209403000",
                                        "label": "三农场",
                                        "children": []
                                    },
                                    {
                                        "value": "130209404",
                                        "values": "130209404000",
                                        "label": "四农场",
                                        "children": []
                                    },
                                    {
                                        "value": "130209405",
                                        "values": "130209405000",
                                        "label": "五农场",
                                        "children": []
                                    },
                                    {
                                        "value": "130209406",
                                        "values": "130209406000",
                                        "label": "六农场",
                                        "children": []
                                    },
                                    {
                                        "value": "130209407",
                                        "values": "130209407000",
                                        "label": "七农场",
                                        "children": []
                                    },
                                    {
                                        "value": "130209408",
                                        "values": "130209408000",
                                        "label": "八农场",
                                        "children": []
                                    },
                                    {
                                        "value": "130209409",
                                        "values": "130209409000",
                                        "label": "九农场",
                                        "children": []
                                    },
                                    {
                                        "value": "130209410",
                                        "values": "130209410000",
                                        "label": "十农场",
                                        "children": []
                                    },
                                    {
                                        "value": "130209411",
                                        "values": "130209411000",
                                        "label": "十一农场",
                                        "children": []
                                    },
                                    {
                                        "value": "130209412",
                                        "values": "130209412000",
                                        "label": "八里滩养殖场",
                                        "children": []
                                    },
                                    {
                                        "value": "130209413",
                                        "values": "130209413000",
                                        "label": "十里海养殖场",
                                        "children": []
                                    },
                                    {
                                        "value": "130209450",
                                        "values": "130209450000",
                                        "label": "南堡经济开发区",
                                        "children": []
                                    },
                                    {
                                        "value": "130209451",
                                        "values": "130209451000",
                                        "label": "曹妃甸工业区",
                                        "children": []
                                    },
                                    {
                                        "value": "130209452",
                                        "values": "130209452000",
                                        "label": "曹妃甸新城",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130224",
                                "values": "130224000000",
                                "label": "滦南县",
                                "children": [
                                    {
                                        "value": "130224001",
                                        "values": "130224001000",
                                        "label": "友谊路街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130224100",
                                        "values": "130224100000",
                                        "label": "倴城镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130224101",
                                        "values": "130224101000",
                                        "label": "宋道口镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130224102",
                                        "values": "130224102000",
                                        "label": "长凝镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130224103",
                                        "values": "130224103000",
                                        "label": "胡各庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130224104",
                                        "values": "130224104000",
                                        "label": "坨里镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130224105",
                                        "values": "130224105000",
                                        "label": "姚王庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130224106",
                                        "values": "130224106000",
                                        "label": "司各庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130224107",
                                        "values": "130224107000",
                                        "label": "安各庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130224108",
                                        "values": "130224108000",
                                        "label": "扒齿港镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130224109",
                                        "values": "130224109000",
                                        "label": "程庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130224110",
                                        "values": "130224110000",
                                        "label": "青坨营镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130224111",
                                        "values": "130224111000",
                                        "label": "柏各庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130224114",
                                        "values": "130224114000",
                                        "label": "南堡镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130224115",
                                        "values": "130224115000",
                                        "label": "方各庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130224116",
                                        "values": "130224116000",
                                        "label": "东黄坨镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130224117",
                                        "values": "130224117000",
                                        "label": "马城镇",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130225",
                                "values": "130225000000",
                                "label": "乐亭县",
                                "children": [
                                    {
                                        "value": "130225001",
                                        "values": "130225001000",
                                        "label": "乐安街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130225100",
                                        "values": "130225100000",
                                        "label": "乐亭镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130225101",
                                        "values": "130225101000",
                                        "label": "汤家河镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130225102",
                                        "values": "130225102000",
                                        "label": "胡家坨镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130225104",
                                        "values": "130225104000",
                                        "label": "阎各庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130225105",
                                        "values": "130225105000",
                                        "label": "马头营镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130225106",
                                        "values": "130225106000",
                                        "label": "新寨镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130225107",
                                        "values": "130225107000",
                                        "label": "汀流河镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130225108",
                                        "values": "130225108000",
                                        "label": "姜各庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130225109",
                                        "values": "130225109000",
                                        "label": "毛庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130225110",
                                        "values": "130225110000",
                                        "label": "中堡镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130225201",
                                        "values": "130225201000",
                                        "label": "庞各庄乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130225202",
                                        "values": "130225202000",
                                        "label": "大相各庄乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130225203",
                                        "values": "130225203000",
                                        "label": "古河乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130225453",
                                        "values": "130225453000",
                                        "label": "河北乐亭经济开发区",
                                        "children": []
                                    },
                                    {
                                        "value": "130225454",
                                        "values": "130225454000",
                                        "label": "乐亭县城区工业聚集区管理委员会",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130227",
                                "values": "130227000000",
                                "label": "迁西县",
                                "children": [
                                    {
                                        "value": "130227001",
                                        "values": "130227001000",
                                        "label": "迁西县栗乡街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130227100",
                                        "values": "130227100000",
                                        "label": "兴城镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130227101",
                                        "values": "130227101000",
                                        "label": "金厂峪镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130227102",
                                        "values": "130227102000",
                                        "label": "洒河桥镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130227103",
                                        "values": "130227103000",
                                        "label": "太平寨镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130227104",
                                        "values": "130227104000",
                                        "label": "罗家屯镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130227105",
                                        "values": "130227105000",
                                        "label": "东荒峪镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130227106",
                                        "values": "130227106000",
                                        "label": "新集镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130227107",
                                        "values": "130227107000",
                                        "label": "三屯营镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130227108",
                                        "values": "130227108000",
                                        "label": "滦阳镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130227200",
                                        "values": "130227200000",
                                        "label": "白庙子乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130227201",
                                        "values": "130227201000",
                                        "label": "上营乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130227202",
                                        "values": "130227202000",
                                        "label": "汉儿庄乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130227203",
                                        "values": "130227203000",
                                        "label": "渔户寨乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130227204",
                                        "values": "130227204000",
                                        "label": "旧城乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130227205",
                                        "values": "130227205000",
                                        "label": "尹庄乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130227206",
                                        "values": "130227206000",
                                        "label": "东莲花院乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130227207",
                                        "values": "130227207000",
                                        "label": "新庄子乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130227450",
                                        "values": "130227450000",
                                        "label": "迁西县栗乡工业产业聚集区",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130229",
                                "values": "130229000000",
                                "label": "玉田县",
                                "children": [
                                    {
                                        "value": "130229001",
                                        "values": "130229001000",
                                        "label": "无终街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130229100",
                                        "values": "130229100000",
                                        "label": "玉田镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130229101",
                                        "values": "130229101000",
                                        "label": "亮甲店镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130229102",
                                        "values": "130229102000",
                                        "label": "鸦鸿桥镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130229103",
                                        "values": "130229103000",
                                        "label": "窝洛沽镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130229104",
                                        "values": "130229104000",
                                        "label": "石臼窝镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130229105",
                                        "values": "130229105000",
                                        "label": "虹桥镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130229106",
                                        "values": "130229106000",
                                        "label": "散水头镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130229107",
                                        "values": "130229107000",
                                        "label": "林南仓镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130229108",
                                        "values": "130229108000",
                                        "label": "林西镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130229109",
                                        "values": "130229109000",
                                        "label": "杨家板桥镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130229110",
                                        "values": "130229110000",
                                        "label": "彩亭桥镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130229111",
                                        "values": "130229111000",
                                        "label": "孤树镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130229112",
                                        "values": "130229112000",
                                        "label": "大安镇镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130229113",
                                        "values": "130229113000",
                                        "label": "唐自头镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130229114",
                                        "values": "130229114000",
                                        "label": "郭家屯镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130229115",
                                        "values": "130229115000",
                                        "label": "杨家套镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130229201",
                                        "values": "130229201000",
                                        "label": "林头屯乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130229203",
                                        "values": "130229203000",
                                        "label": "潮洛窝乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130229204",
                                        "values": "130229204000",
                                        "label": "陈家铺乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130229205",
                                        "values": "130229205000",
                                        "label": "郭家桥乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130229400",
                                        "values": "130229400000",
                                        "label": "河北玉田经济开发区",
                                        "children": []
                                    },
                                    {
                                        "value": "130229401",
                                        "values": "130229401000",
                                        "label": "河北唐山国家农业科技园区",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130271",
                                "values": "130271000000",
                                "label": "河北唐山芦台经济开发区",
                                "children": [
                                    {
                                        "value": "130271100",
                                        "values": "130271100000",
                                        "label": "芦台开发区海北镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130271450",
                                        "values": "130271450000",
                                        "label": "芦台开发区新华路街道",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130272",
                                "values": "130272000000",
                                "label": "唐山市汉沽管理区",
                                "children": [
                                    {
                                        "value": "130272101",
                                        "values": "130272101000",
                                        "label": "汉沽管理区汉丰镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130272451",
                                        "values": "130272451000",
                                        "label": "汉沽管理区兴农街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130272452",
                                        "values": "130272452000",
                                        "label": "汉沽管理区振兴街道",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130273",
                                "values": "130273000000",
                                "label": "唐山高新技术产业开发区",
                                "children": [
                                    {
                                        "value": "130273001",
                                        "values": "130273001000",
                                        "label": "街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130273101",
                                        "values": "130273101000",
                                        "label": "老庄子镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130273401",
                                        "values": "130273401000",
                                        "label": "庆北办事处",
                                        "children": []
                                    },
                                    {
                                        "value": "130273402",
                                        "values": "130273402000",
                                        "label": "三女河办事处",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130274",
                                "values": "130274000000",
                                "label": "河北唐山海港经济开发区",
                                "children": [
                                    {
                                        "value": "130274103",
                                        "values": "130274103000",
                                        "label": "唐山海港经济开发区王滩镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130274451",
                                        "values": "130274451000",
                                        "label": "河北唐山海港经济开发区综合执法大队",
                                        "children": []
                                    },
                                    {
                                        "value": "130274452",
                                        "values": "130274452000",
                                        "label": "唐山湾国际旅游岛",
                                        "children": []
                                    },
                                    {
                                        "value": "130274455",
                                        "values": "130274455000",
                                        "label": "大清河",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130281",
                                "values": "130281000000",
                                "label": "遵化市",
                                "children": [
                                    {
                                        "value": "130281001",
                                        "values": "130281001000",
                                        "label": "华明路街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130281002",
                                        "values": "130281002000",
                                        "label": "文化路街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130281100",
                                        "values": "130281100000",
                                        "label": "遵化镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130281101",
                                        "values": "130281101000",
                                        "label": "堡子店镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130281102",
                                        "values": "130281102000",
                                        "label": "马兰峪镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130281103",
                                        "values": "130281103000",
                                        "label": "平安城镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130281104",
                                        "values": "130281104000",
                                        "label": "东新庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130281105",
                                        "values": "130281105000",
                                        "label": "新店子镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130281106",
                                        "values": "130281106000",
                                        "label": "党峪镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130281107",
                                        "values": "130281107000",
                                        "label": "地北头镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130281108",
                                        "values": "130281108000",
                                        "label": "东旧寨镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130281109",
                                        "values": "130281109000",
                                        "label": "铁厂镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130281110",
                                        "values": "130281110000",
                                        "label": "苏家洼镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130281111",
                                        "values": "130281111000",
                                        "label": "建明镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130281112",
                                        "values": "130281112000",
                                        "label": "石门镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130281200",
                                        "values": "130281200000",
                                        "label": "西留村乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130281201",
                                        "values": "130281201000",
                                        "label": "崔家庄乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130281202",
                                        "values": "130281202000",
                                        "label": "兴旺寨乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130281203",
                                        "values": "130281203000",
                                        "label": "西下营满族乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130281204",
                                        "values": "130281204000",
                                        "label": "汤泉满族乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130281205",
                                        "values": "130281205000",
                                        "label": "东陵满族乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130281206",
                                        "values": "130281206000",
                                        "label": "刘备寨乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130281207",
                                        "values": "130281207000",
                                        "label": "团瓢庄乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130281208",
                                        "values": "130281208000",
                                        "label": "娘娘庄乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130281209",
                                        "values": "130281209000",
                                        "label": "西三里乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130281210",
                                        "values": "130281210000",
                                        "label": "侯家寨乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130281211",
                                        "values": "130281211000",
                                        "label": "小厂乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130283",
                                "values": "130283000000",
                                "label": "迁安市",
                                "children": [
                                    {
                                        "value": "130283001",
                                        "values": "130283001000",
                                        "label": "永顺街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130283002",
                                        "values": "130283002000",
                                        "label": "兴安街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130283003",
                                        "values": "130283003000",
                                        "label": "滨河街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130283004",
                                        "values": "130283004000",
                                        "label": "杨店子街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130283101",
                                        "values": "130283101000",
                                        "label": "夏官营镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130283102",
                                        "values": "130283102000",
                                        "label": "杨各庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130283103",
                                        "values": "130283103000",
                                        "label": "建昌营镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130283104",
                                        "values": "130283104000",
                                        "label": "赵店子镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130283105",
                                        "values": "130283105000",
                                        "label": "野鸡坨镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130283106",
                                        "values": "130283106000",
                                        "label": "大崔庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130283108",
                                        "values": "130283108000",
                                        "label": "蔡园镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130283109",
                                        "values": "130283109000",
                                        "label": "马兰庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130283110",
                                        "values": "130283110000",
                                        "label": "沙河驿镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130283111",
                                        "values": "130283111000",
                                        "label": "木厂口镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130283200",
                                        "values": "130283200000",
                                        "label": "扣庄乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130283201",
                                        "values": "130283201000",
                                        "label": "彭店子乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130283202",
                                        "values": "130283202000",
                                        "label": "上射雁庄乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130283203",
                                        "values": "130283203000",
                                        "label": "闫家店乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130283204",
                                        "values": "130283204000",
                                        "label": "五重安乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130283205",
                                        "values": "130283205000",
                                        "label": "大五里乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130283206",
                                        "values": "130283206000",
                                        "label": "太平庄乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130284",
                                "values": "130284000000",
                                "label": "滦州市",
                                "children": [
                                    {
                                        "value": "130284001",
                                        "values": "130284001000",
                                        "label": "滦河街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130284002",
                                        "values": "130284002000",
                                        "label": "古城街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130284003",
                                        "values": "130284003000",
                                        "label": "滦城路街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130284004",
                                        "values": "130284004000",
                                        "label": "响堂街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130284102",
                                        "values": "130284102000",
                                        "label": "东安各庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130284103",
                                        "values": "130284103000",
                                        "label": "雷庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130284104",
                                        "values": "130284104000",
                                        "label": "茨榆坨镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130284105",
                                        "values": "130284105000",
                                        "label": "榛子镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130284106",
                                        "values": "130284106000",
                                        "label": "杨柳庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130284107",
                                        "values": "130284107000",
                                        "label": "油榨镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130284108",
                                        "values": "130284108000",
                                        "label": "古马镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130284109",
                                        "values": "130284109000",
                                        "label": "小马庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130284110",
                                        "values": "130284110000",
                                        "label": "九百户镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130284111",
                                        "values": "130284111000",
                                        "label": "王店子镇",
                                        "children": []
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "value": "1303",
                        "values": "130300000000",
                        "label": "秦皇岛市",
                        "children": [
                            {
                                "value": "130301",
                                "values": "130301000000",
                                "label": "市辖区",
                                "children": []
                            },
                            {
                                "value": "130302",
                                "values": "130302000000",
                                "label": "海港区",
                                "children": [
                                    {
                                        "value": "130302001",
                                        "values": "130302001000",
                                        "label": "文化路街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130302002",
                                        "values": "130302002000",
                                        "label": "海滨路街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130302003",
                                        "values": "130302003000",
                                        "label": "北环路街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130302004",
                                        "values": "130302004000",
                                        "label": "建设大街街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130302005",
                                        "values": "130302005000",
                                        "label": "河东街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130302006",
                                        "values": "130302006000",
                                        "label": "西港路街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130302007",
                                        "values": "130302007000",
                                        "label": "燕山大街街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130302008",
                                        "values": "130302008000",
                                        "label": "港城大街街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130302009",
                                        "values": "130302009000",
                                        "label": "东环路街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130302010",
                                        "values": "130302010000",
                                        "label": "白塔岭街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130302100",
                                        "values": "130302100000",
                                        "label": "东港镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130302101",
                                        "values": "130302101000",
                                        "label": "海港镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130302102",
                                        "values": "130302102000",
                                        "label": "西港镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130302103",
                                        "values": "130302103000",
                                        "label": "海阳镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130302104",
                                        "values": "130302104000",
                                        "label": "北港镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130302105",
                                        "values": "130302105000",
                                        "label": "杜庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130302106",
                                        "values": "130302106000",
                                        "label": "石门寨镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130302107",
                                        "values": "130302107000",
                                        "label": "驻操营镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130302403",
                                        "values": "130302403000",
                                        "label": "临港物流园区",
                                        "children": []
                                    },
                                    {
                                        "value": "130302404",
                                        "values": "130302404000",
                                        "label": "海港经济开发区",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130303",
                                "values": "130303000000",
                                "label": "山海关区",
                                "children": [
                                    {
                                        "value": "130303001",
                                        "values": "130303001000",
                                        "label": "南关街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130303002",
                                        "values": "130303002000",
                                        "label": "古城街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130303003",
                                        "values": "130303003000",
                                        "label": "西关街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130303004",
                                        "values": "130303004000",
                                        "label": "路南街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130303100",
                                        "values": "130303100000",
                                        "label": "第一关镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130303101",
                                        "values": "130303101000",
                                        "label": "石河镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130303102",
                                        "values": "130303102000",
                                        "label": "孟姜镇",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130304",
                                "values": "130304000000",
                                "label": "北戴河区",
                                "children": [
                                    {
                                        "value": "130304001",
                                        "values": "130304001000",
                                        "label": "西山街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130304002",
                                        "values": "130304002000",
                                        "label": "东山街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130304100",
                                        "values": "130304100000",
                                        "label": "海滨镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130304101",
                                        "values": "130304101000",
                                        "label": "戴河镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130304103",
                                        "values": "130304103000",
                                        "label": "牛头崖镇",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130306",
                                "values": "130306000000",
                                "label": "抚宁区",
                                "children": [
                                    {
                                        "value": "130306001",
                                        "values": "130306001000",
                                        "label": "骊城街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130306100",
                                        "values": "130306100000",
                                        "label": "抚宁镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130306101",
                                        "values": "130306101000",
                                        "label": "留守营镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130306102",
                                        "values": "130306102000",
                                        "label": "榆关镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130306105",
                                        "values": "130306105000",
                                        "label": "台营镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130306106",
                                        "values": "130306106000",
                                        "label": "大新寨镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130306200",
                                        "values": "130306200000",
                                        "label": "茶棚乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130306202",
                                        "values": "130306202000",
                                        "label": "深河乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130321",
                                "values": "130321000000",
                                "label": "青龙满族自治县",
                                "children": [
                                    {
                                        "value": "130321001",
                                        "values": "130321001000",
                                        "label": "都阳路街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130321100",
                                        "values": "130321100000",
                                        "label": "青龙镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130321101",
                                        "values": "130321101000",
                                        "label": "祖山镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130321102",
                                        "values": "130321102000",
                                        "label": "木头凳镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130321103",
                                        "values": "130321103000",
                                        "label": "双山子镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130321104",
                                        "values": "130321104000",
                                        "label": "马圈子镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130321105",
                                        "values": "130321105000",
                                        "label": "肖营子镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130321106",
                                        "values": "130321106000",
                                        "label": "大巫岚镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130321107",
                                        "values": "130321107000",
                                        "label": "土门子镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130321108",
                                        "values": "130321108000",
                                        "label": "八道河镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130321109",
                                        "values": "130321109000",
                                        "label": "隔河头镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130321110",
                                        "values": "130321110000",
                                        "label": "娄杖子镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130321200",
                                        "values": "130321200000",
                                        "label": "凤凰山乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130321201",
                                        "values": "130321201000",
                                        "label": "龙王庙乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130321202",
                                        "values": "130321202000",
                                        "label": "三星口乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130321203",
                                        "values": "130321203000",
                                        "label": "干沟乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130321206",
                                        "values": "130321206000",
                                        "label": "大石岭乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130321208",
                                        "values": "130321208000",
                                        "label": "官场乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130321209",
                                        "values": "130321209000",
                                        "label": "茨榆山乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130321210",
                                        "values": "130321210000",
                                        "label": "平方子乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130321211",
                                        "values": "130321211000",
                                        "label": "安子岭乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130321212",
                                        "values": "130321212000",
                                        "label": "朱杖子乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130321213",
                                        "values": "130321213000",
                                        "label": "草碾乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130321214",
                                        "values": "130321214000",
                                        "label": "七道河乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130321217",
                                        "values": "130321217000",
                                        "label": "三拨子乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130321218",
                                        "values": "130321218000",
                                        "label": "凉水河乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130322",
                                "values": "130322000000",
                                "label": "昌黎县",
                                "children": [
                                    {
                                        "value": "130322100",
                                        "values": "130322100000",
                                        "label": "昌黎镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130322101",
                                        "values": "130322101000",
                                        "label": "靖安镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130322102",
                                        "values": "130322102000",
                                        "label": "安山镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130322103",
                                        "values": "130322103000",
                                        "label": "龙家店镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130322104",
                                        "values": "130322104000",
                                        "label": "泥井镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130322105",
                                        "values": "130322105000",
                                        "label": "大蒲河镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130322106",
                                        "values": "130322106000",
                                        "label": "新集镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130322107",
                                        "values": "130322107000",
                                        "label": "刘台庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130322108",
                                        "values": "130322108000",
                                        "label": "茹荷镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130322109",
                                        "values": "130322109000",
                                        "label": "朱各庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130322110",
                                        "values": "130322110000",
                                        "label": "荒佃庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130322201",
                                        "values": "130322201000",
                                        "label": "团林乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130322202",
                                        "values": "130322202000",
                                        "label": "葛条港乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130322203",
                                        "values": "130322203000",
                                        "label": "马坨店乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130322206",
                                        "values": "130322206000",
                                        "label": "两山乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130322207",
                                        "values": "130322207000",
                                        "label": "十里铺乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130324",
                                "values": "130324000000",
                                "label": "卢龙县",
                                "children": [
                                    {
                                        "value": "130324100",
                                        "values": "130324100000",
                                        "label": "卢龙镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130324101",
                                        "values": "130324101000",
                                        "label": "潘庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130324102",
                                        "values": "130324102000",
                                        "label": "燕河营镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130324103",
                                        "values": "130324103000",
                                        "label": "双望镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130324104",
                                        "values": "130324104000",
                                        "label": "刘田各庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130324105",
                                        "values": "130324105000",
                                        "label": "石门镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130324106",
                                        "values": "130324106000",
                                        "label": "木井镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130324107",
                                        "values": "130324107000",
                                        "label": "陈官屯镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130324108",
                                        "values": "130324108000",
                                        "label": "蛤泊镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130324200",
                                        "values": "130324200000",
                                        "label": "下寨乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130324201",
                                        "values": "130324201000",
                                        "label": "刘家营乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130324203",
                                        "values": "130324203000",
                                        "label": "印庄乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130324400",
                                        "values": "130324400000",
                                        "label": "河北卢龙经济开发区",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130371",
                                "values": "130371000000",
                                "label": "秦皇岛市经济技术开发区",
                                "children": [
                                    {
                                        "value": "130371005",
                                        "values": "130371005000",
                                        "label": "船厂路街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130371011",
                                        "values": "130371011000",
                                        "label": "珠江道街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130371012",
                                        "values": "130371012000",
                                        "label": "黄河道街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130371013",
                                        "values": "130371013000",
                                        "label": "腾飞路街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130371200",
                                        "values": "130371200000",
                                        "label": "渤海乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130372",
                                "values": "130372000000",
                                "label": "北戴河新区",
                                "children": [
                                    {
                                        "value": "130372001",
                                        "values": "130372001000",
                                        "label": "南戴河街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130372400",
                                        "values": "130372400000",
                                        "label": "大蒲河管理处",
                                        "children": []
                                    },
                                    {
                                        "value": "130372401",
                                        "values": "130372401000",
                                        "label": "团林管理处",
                                        "children": []
                                    },
                                    {
                                        "value": "130372402",
                                        "values": "130372402000",
                                        "label": "留守营管理处",
                                        "children": []
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "value": "1304",
                        "values": "130400000000",
                        "label": "邯郸市",
                        "children": [
                            {
                                "value": "130401",
                                "values": "130401000000",
                                "label": "市辖区",
                                "children": []
                            },
                            {
                                "value": "130402",
                                "values": "130402000000",
                                "label": "邯山区",
                                "children": [
                                    {
                                        "value": "130402001",
                                        "values": "130402001000",
                                        "label": "火磨街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130402002",
                                        "values": "130402002000",
                                        "label": "陵园路街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130402003",
                                        "values": "130402003000",
                                        "label": "光明路街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130402004",
                                        "values": "130402004000",
                                        "label": "滏东街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130402005",
                                        "values": "130402005000",
                                        "label": "罗城头街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130402006",
                                        "values": "130402006000",
                                        "label": "渚河路街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130402007",
                                        "values": "130402007000",
                                        "label": "浴新南街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130402008",
                                        "values": "130402008000",
                                        "label": "农林路街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130402009",
                                        "values": "130402009000",
                                        "label": "贸东街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130402010",
                                        "values": "130402010000",
                                        "label": "贸西街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130402011",
                                        "values": "130402011000",
                                        "label": "盛和路街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130402101",
                                        "values": "130402101000",
                                        "label": "北张庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130402102",
                                        "values": "130402102000",
                                        "label": "河沙镇镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130402200",
                                        "values": "130402200000",
                                        "label": "马庄乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130402201",
                                        "values": "130402201000",
                                        "label": "南堡乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130402202",
                                        "values": "130402202000",
                                        "label": "代召乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130403",
                                "values": "130403000000",
                                "label": "丛台区",
                                "children": [
                                    {
                                        "value": "130403001",
                                        "values": "130403001000",
                                        "label": "丛台西街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130403002",
                                        "values": "130403002000",
                                        "label": "联纺西街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130403003",
                                        "values": "130403003000",
                                        "label": "联纺东街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130403004",
                                        "values": "130403004000",
                                        "label": "光明桥街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130403005",
                                        "values": "130403005000",
                                        "label": "丛台东街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130403006",
                                        "values": "130403006000",
                                        "label": "四季青街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130403007",
                                        "values": "130403007000",
                                        "label": "和平街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130403008",
                                        "values": "130403008000",
                                        "label": "中华街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130403009",
                                        "values": "130403009000",
                                        "label": "人民路街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130403010",
                                        "values": "130403010000",
                                        "label": "柳林桥街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130403101",
                                        "values": "130403101000",
                                        "label": "黄粱梦镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130403200",
                                        "values": "130403200000",
                                        "label": "苏曹乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130403205",
                                        "values": "130403205000",
                                        "label": "三陵乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130403206",
                                        "values": "130403206000",
                                        "label": "南吕固乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130403207",
                                        "values": "130403207000",
                                        "label": "兼庄乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130404",
                                "values": "130404000000",
                                "label": "复兴区",
                                "children": [
                                    {
                                        "value": "130404001",
                                        "values": "130404001000",
                                        "label": "胜利桥街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130404002",
                                        "values": "130404002000",
                                        "label": "百家村街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130404003",
                                        "values": "130404003000",
                                        "label": "铁路大院街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130404004",
                                        "values": "130404004000",
                                        "label": "化林路街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130404005",
                                        "values": "130404005000",
                                        "label": "庞村街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130404006",
                                        "values": "130404006000",
                                        "label": "二六七二街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130404007",
                                        "values": "130404007000",
                                        "label": "石化街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130404100",
                                        "values": "130404100000",
                                        "label": "户村镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130404200",
                                        "values": "130404200000",
                                        "label": "彭家寨乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130404201",
                                        "values": "130404201000",
                                        "label": "康庄乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130406",
                                "values": "130406000000",
                                "label": "峰峰矿区",
                                "children": [
                                    {
                                        "value": "130406001",
                                        "values": "130406001000",
                                        "label": "滏阳东路街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130406100",
                                        "values": "130406100000",
                                        "label": "临水镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130406101",
                                        "values": "130406101000",
                                        "label": "峰峰镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130406102",
                                        "values": "130406102000",
                                        "label": "新坡镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130406103",
                                        "values": "130406103000",
                                        "label": "大社镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130406104",
                                        "values": "130406104000",
                                        "label": "和村镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130406105",
                                        "values": "130406105000",
                                        "label": "义井镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130406106",
                                        "values": "130406106000",
                                        "label": "彭城镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130406107",
                                        "values": "130406107000",
                                        "label": "界城镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130406108",
                                        "values": "130406108000",
                                        "label": "大峪镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130406200",
                                        "values": "130406200000",
                                        "label": "西固义乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130407",
                                "values": "130407000000",
                                "label": "肥乡区",
                                "children": [
                                    {
                                        "value": "130407100",
                                        "values": "130407100000",
                                        "label": "肥乡镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130407101",
                                        "values": "130407101000",
                                        "label": "天台山镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130407102",
                                        "values": "130407102000",
                                        "label": "辛安镇镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130407103",
                                        "values": "130407103000",
                                        "label": "大寺上镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130407104",
                                        "values": "130407104000",
                                        "label": "东漳堡镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130407202",
                                        "values": "130407202000",
                                        "label": "毛演堡乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130407203",
                                        "values": "130407203000",
                                        "label": "元固乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130407204",
                                        "values": "130407204000",
                                        "label": "屯庄营乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130407206",
                                        "values": "130407206000",
                                        "label": "旧店乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130408",
                                "values": "130408000000",
                                "label": "永年区",
                                "children": [
                                    {
                                        "value": "130408100",
                                        "values": "130408100000",
                                        "label": "临洺关镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130408101",
                                        "values": "130408101000",
                                        "label": "大北汪镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130408102",
                                        "values": "130408102000",
                                        "label": "张西堡镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130408103",
                                        "values": "130408103000",
                                        "label": "广府镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130408105",
                                        "values": "130408105000",
                                        "label": "永合会镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130408106",
                                        "values": "130408106000",
                                        "label": "刘营镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130408107",
                                        "values": "130408107000",
                                        "label": "西苏镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130408108",
                                        "values": "130408108000",
                                        "label": "讲武镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130408109",
                                        "values": "130408109000",
                                        "label": "东杨庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130408201",
                                        "values": "130408201000",
                                        "label": "界河店乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130408203",
                                        "values": "130408203000",
                                        "label": "刘汉乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130408204",
                                        "values": "130408204000",
                                        "label": "正西乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130408206",
                                        "values": "130408206000",
                                        "label": "曲陌乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130408207",
                                        "values": "130408207000",
                                        "label": "辛庄堡乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130408208",
                                        "values": "130408208000",
                                        "label": "小龙马乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130408211",
                                        "values": "130408211000",
                                        "label": "西河庄乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130408213",
                                        "values": "130408213000",
                                        "label": "西阳城乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130423",
                                "values": "130423000000",
                                "label": "临漳县",
                                "children": [
                                    {
                                        "value": "130423100",
                                        "values": "130423100000",
                                        "label": "临漳镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130423101",
                                        "values": "130423101000",
                                        "label": "南东坊镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130423102",
                                        "values": "130423102000",
                                        "label": "孙陶集镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130423103",
                                        "values": "130423103000",
                                        "label": "柳园镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130423104",
                                        "values": "130423104000",
                                        "label": "称勾集镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130423105",
                                        "values": "130423105000",
                                        "label": "邺城镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130423106",
                                        "values": "130423106000",
                                        "label": "章里集镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130423200",
                                        "values": "130423200000",
                                        "label": "狄邱乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130423201",
                                        "values": "130423201000",
                                        "label": "张村集乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130423202",
                                        "values": "130423202000",
                                        "label": "西羊羔乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130423204",
                                        "values": "130423204000",
                                        "label": "杜村集乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130423206",
                                        "values": "130423206000",
                                        "label": "习文乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130423207",
                                        "values": "130423207000",
                                        "label": "砖寨营乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130423208",
                                        "values": "130423208000",
                                        "label": "柏鹤集乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130424",
                                "values": "130424000000",
                                "label": "成安县",
                                "children": [
                                    {
                                        "value": "130424100",
                                        "values": "130424100000",
                                        "label": "成安镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130424101",
                                        "values": "130424101000",
                                        "label": "商城镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130424102",
                                        "values": "130424102000",
                                        "label": "漳河店镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130424103",
                                        "values": "130424103000",
                                        "label": "李家疃镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130424104",
                                        "values": "130424104000",
                                        "label": "北乡义镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130424200",
                                        "values": "130424200000",
                                        "label": "辛义乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130424201",
                                        "values": "130424201000",
                                        "label": "柏寺营乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130424202",
                                        "values": "130424202000",
                                        "label": "道东堡乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130424204",
                                        "values": "130424204000",
                                        "label": "长巷乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130425",
                                "values": "130425000000",
                                "label": "大名县",
                                "children": [
                                    {
                                        "value": "130425100",
                                        "values": "130425100000",
                                        "label": "大名镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130425101",
                                        "values": "130425101000",
                                        "label": "杨桥镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130425102",
                                        "values": "130425102000",
                                        "label": "万堤镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130425103",
                                        "values": "130425103000",
                                        "label": "龙王庙镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130425104",
                                        "values": "130425104000",
                                        "label": "束馆镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130425105",
                                        "values": "130425105000",
                                        "label": "金滩镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130425106",
                                        "values": "130425106000",
                                        "label": "沙圪塔镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130425107",
                                        "values": "130425107000",
                                        "label": "大街镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130425108",
                                        "values": "130425108000",
                                        "label": "铺上镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130425109",
                                        "values": "130425109000",
                                        "label": "孙甘店镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130425201",
                                        "values": "130425201000",
                                        "label": "王村乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130425203",
                                        "values": "130425203000",
                                        "label": "黄金堤乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130425205",
                                        "values": "130425205000",
                                        "label": "旧治乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130425206",
                                        "values": "130425206000",
                                        "label": "西未庄乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130425208",
                                        "values": "130425208000",
                                        "label": "西付集乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130425209",
                                        "values": "130425209000",
                                        "label": "埝头乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130425210",
                                        "values": "130425210000",
                                        "label": "北峰乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130425211",
                                        "values": "130425211000",
                                        "label": "张铁集乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130425212",
                                        "values": "130425212000",
                                        "label": "红庙乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130425213",
                                        "values": "130425213000",
                                        "label": "营镇回族乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130426",
                                "values": "130426000000",
                                "label": "涉县",
                                "children": [
                                    {
                                        "value": "130426001",
                                        "values": "130426001000",
                                        "label": "平安街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130426101",
                                        "values": "130426101000",
                                        "label": "河南店镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130426102",
                                        "values": "130426102000",
                                        "label": "索堡镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130426103",
                                        "values": "130426103000",
                                        "label": "西戌镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130426104",
                                        "values": "130426104000",
                                        "label": "井店镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130426105",
                                        "values": "130426105000",
                                        "label": "更乐镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130426106",
                                        "values": "130426106000",
                                        "label": "固新镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130426107",
                                        "values": "130426107000",
                                        "label": "西达镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130426108",
                                        "values": "130426108000",
                                        "label": "偏城镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130426200",
                                        "values": "130426200000",
                                        "label": "神头乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130426201",
                                        "values": "130426201000",
                                        "label": "辽城乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130426202",
                                        "values": "130426202000",
                                        "label": "偏店乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130426203",
                                        "values": "130426203000",
                                        "label": "龙虎乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130426204",
                                        "values": "130426204000",
                                        "label": "木井乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130426205",
                                        "values": "130426205000",
                                        "label": "关防乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130426206",
                                        "values": "130426206000",
                                        "label": "合漳乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130426207",
                                        "values": "130426207000",
                                        "label": "鹿头乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130426400",
                                        "values": "130426400000",
                                        "label": "涉城镇",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130427",
                                "values": "130427000000",
                                "label": "磁县",
                                "children": [
                                    {
                                        "value": "130427100",
                                        "values": "130427100000",
                                        "label": "磁州镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130427103",
                                        "values": "130427103000",
                                        "label": "讲武城镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130427104",
                                        "values": "130427104000",
                                        "label": "岳城镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130427105",
                                        "values": "130427105000",
                                        "label": "观台镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130427107",
                                        "values": "130427107000",
                                        "label": "白土镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130427108",
                                        "values": "130427108000",
                                        "label": "黄沙镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130427200",
                                        "values": "130427200000",
                                        "label": "路村营乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130427204",
                                        "values": "130427204000",
                                        "label": "时村营乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130427207",
                                        "values": "130427207000",
                                        "label": "陶泉乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130427208",
                                        "values": "130427208000",
                                        "label": "都党乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130427209",
                                        "values": "130427209000",
                                        "label": "北贾壁乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130430",
                                "values": "130430000000",
                                "label": "邱县",
                                "children": [
                                    {
                                        "value": "130430100",
                                        "values": "130430100000",
                                        "label": "新马头镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130430101",
                                        "values": "130430101000",
                                        "label": "邱城镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130430102",
                                        "values": "130430102000",
                                        "label": "梁二庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130430103",
                                        "values": "130430103000",
                                        "label": "香城固镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130430104",
                                        "values": "130430104000",
                                        "label": "古城营镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130430201",
                                        "values": "130430201000",
                                        "label": "南辛店乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130430204",
                                        "values": "130430204000",
                                        "label": "陈村回族乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130431",
                                "values": "130431000000",
                                "label": "鸡泽县",
                                "children": [
                                    {
                                        "value": "130431100",
                                        "values": "130431100000",
                                        "label": "鸡泽镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130431101",
                                        "values": "130431101000",
                                        "label": "小寨镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130431102",
                                        "values": "130431102000",
                                        "label": "双塔镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130431103",
                                        "values": "130431103000",
                                        "label": "曹庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130431200",
                                        "values": "130431200000",
                                        "label": "浮图店乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130431201",
                                        "values": "130431201000",
                                        "label": "吴官营乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130431202",
                                        "values": "130431202000",
                                        "label": "风正乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130432",
                                "values": "130432000000",
                                "label": "广平县",
                                "children": [
                                    {
                                        "value": "130432100",
                                        "values": "130432100000",
                                        "label": "广平镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130432101",
                                        "values": "130432101000",
                                        "label": "平固店镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130432102",
                                        "values": "130432102000",
                                        "label": "胜营镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130432103",
                                        "values": "130432103000",
                                        "label": "南阳堡镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130432104",
                                        "values": "130432104000",
                                        "label": "十里铺镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130432105",
                                        "values": "130432105000",
                                        "label": "南韩镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130432106",
                                        "values": "130432106000",
                                        "label": "东张孟镇",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130433",
                                "values": "130433000000",
                                "label": "馆陶县",
                                "children": [
                                    {
                                        "value": "130433100",
                                        "values": "130433100000",
                                        "label": "馆陶镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130433101",
                                        "values": "130433101000",
                                        "label": "房寨镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130433102",
                                        "values": "130433102000",
                                        "label": "柴堡镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130433103",
                                        "values": "130433103000",
                                        "label": "魏僧寨镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130433200",
                                        "values": "130433200000",
                                        "label": "寿山寺乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130433201",
                                        "values": "130433201000",
                                        "label": "王桥乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130433202",
                                        "values": "130433202000",
                                        "label": "南徐村乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130433203",
                                        "values": "130433203000",
                                        "label": "路桥乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130434",
                                "values": "130434000000",
                                "label": "魏县",
                                "children": [
                                    {
                                        "value": "130434100",
                                        "values": "130434100000",
                                        "label": "魏城镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130434101",
                                        "values": "130434101000",
                                        "label": "德政镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130434102",
                                        "values": "130434102000",
                                        "label": "北皋镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130434103",
                                        "values": "130434103000",
                                        "label": "双井镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130434104",
                                        "values": "130434104000",
                                        "label": "牙里镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130434105",
                                        "values": "130434105000",
                                        "label": "车往镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130434106",
                                        "values": "130434106000",
                                        "label": "回隆镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130434107",
                                        "values": "130434107000",
                                        "label": "张二庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130434108",
                                        "values": "130434108000",
                                        "label": "东代固镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130434109",
                                        "values": "130434109000",
                                        "label": "院堡镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130434110",
                                        "values": "130434110000",
                                        "label": "棘针寨镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130434111",
                                        "values": "130434111000",
                                        "label": "南双庙镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130434202",
                                        "values": "130434202000",
                                        "label": "沙口集乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130434203",
                                        "values": "130434203000",
                                        "label": "野胡拐乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130434204",
                                        "values": "130434204000",
                                        "label": "仕望集乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130434205",
                                        "values": "130434205000",
                                        "label": "前大磨乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130434208",
                                        "values": "130434208000",
                                        "label": "大辛庄乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130434209",
                                        "values": "130434209000",
                                        "label": "大马村乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130434210",
                                        "values": "130434210000",
                                        "label": "边马乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130434212",
                                        "values": "130434212000",
                                        "label": "北台头乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130434213",
                                        "values": "130434213000",
                                        "label": "泊口乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130435",
                                "values": "130435000000",
                                "label": "曲周县",
                                "children": [
                                    {
                                        "value": "130435100",
                                        "values": "130435100000",
                                        "label": "曲周镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130435101",
                                        "values": "130435101000",
                                        "label": "安寨镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130435102",
                                        "values": "130435102000",
                                        "label": "侯村镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130435103",
                                        "values": "130435103000",
                                        "label": "河南疃镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130435104",
                                        "values": "130435104000",
                                        "label": "第四疃镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130435105",
                                        "values": "130435105000",
                                        "label": "白寨镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130435200",
                                        "values": "130435200000",
                                        "label": "槐桥乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130435201",
                                        "values": "130435201000",
                                        "label": "南里岳乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130435203",
                                        "values": "130435203000",
                                        "label": "大河道乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130435204",
                                        "values": "130435204000",
                                        "label": "依庄乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130471",
                                "values": "130471000000",
                                "label": "邯郸经济技术开发区",
                                "children": [
                                    {
                                        "value": "130471001",
                                        "values": "130471001000",
                                        "label": "开发区街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130471100",
                                        "values": "130471100000",
                                        "label": "尚璧镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130471101",
                                        "values": "130471101000",
                                        "label": "南沿村镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130471200",
                                        "values": "130471200000",
                                        "label": "小西堡乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130471201",
                                        "values": "130471201000",
                                        "label": "姚寨乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130473",
                                "values": "130473000000",
                                "label": "邯郸冀南新区",
                                "children": [
                                    {
                                        "value": "130473001",
                                        "values": "130473001000",
                                        "label": "城南街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130473100",
                                        "values": "130473100000",
                                        "label": "高臾镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130473101",
                                        "values": "130473101000",
                                        "label": "西光禄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130473102",
                                        "values": "130473102000",
                                        "label": "林坛镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130473103",
                                        "values": "130473103000",
                                        "label": "马头镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130473200",
                                        "values": "130473200000",
                                        "label": "辛庄营乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130473201",
                                        "values": "130473201000",
                                        "label": "花官营乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130473202",
                                        "values": "130473202000",
                                        "label": "台城乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130473203",
                                        "values": "130473203000",
                                        "label": "南城乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130481",
                                "values": "130481000000",
                                "label": "武安市",
                                "children": [
                                    {
                                        "value": "130481100",
                                        "values": "130481100000",
                                        "label": "武安镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130481101",
                                        "values": "130481101000",
                                        "label": "康二城镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130481102",
                                        "values": "130481102000",
                                        "label": "午汲镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130481103",
                                        "values": "130481103000",
                                        "label": "磁山镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130481104",
                                        "values": "130481104000",
                                        "label": "伯延镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130481105",
                                        "values": "130481105000",
                                        "label": "淑村镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130481106",
                                        "values": "130481106000",
                                        "label": "大同镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130481107",
                                        "values": "130481107000",
                                        "label": "邑城镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130481108",
                                        "values": "130481108000",
                                        "label": "矿山镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130481109",
                                        "values": "130481109000",
                                        "label": "贺进镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130481110",
                                        "values": "130481110000",
                                        "label": "阳邑镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130481111",
                                        "values": "130481111000",
                                        "label": "徘徊镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130481112",
                                        "values": "130481112000",
                                        "label": "冶陶镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130481200",
                                        "values": "130481200000",
                                        "label": "上团城乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130481201",
                                        "values": "130481201000",
                                        "label": "北安庄乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130481202",
                                        "values": "130481202000",
                                        "label": "北安乐乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130481203",
                                        "values": "130481203000",
                                        "label": "西土山乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130481204",
                                        "values": "130481204000",
                                        "label": "西寺庄乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130481205",
                                        "values": "130481205000",
                                        "label": "活水乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130481206",
                                        "values": "130481206000",
                                        "label": "石洞乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130481207",
                                        "values": "130481207000",
                                        "label": "管陶乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130481208",
                                        "values": "130481208000",
                                        "label": "马家庄乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130481400",
                                        "values": "130481400000",
                                        "label": "河北武安工业园区",
                                        "children": []
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "value": "1305",
                        "values": "130500000000",
                        "label": "邢台市",
                        "children": [
                            {
                                "value": "130501",
                                "values": "130501000000",
                                "label": "市辖区",
                                "children": []
                            },
                            {
                                "value": "130502",
                                "values": "130502000000",
                                "label": "襄都区",
                                "children": [
                                    {
                                        "value": "130502001",
                                        "values": "130502001000",
                                        "label": "南长街街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130502002",
                                        "values": "130502002000",
                                        "label": "北大街街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130502003",
                                        "values": "130502003000",
                                        "label": "西大街街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130502004",
                                        "values": "130502004000",
                                        "label": "西门里街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130502005",
                                        "values": "130502005000",
                                        "label": "泉东街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130502006",
                                        "values": "130502006000",
                                        "label": "豫让桥街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130502100",
                                        "values": "130502100000",
                                        "label": "东郭村镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130502101",
                                        "values": "130502101000",
                                        "label": "祝村镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130502103",
                                        "values": "130502103000",
                                        "label": "晏家屯镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130502200",
                                        "values": "130502200000",
                                        "label": "大梁庄乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130503",
                                "values": "130503000000",
                                "label": "信都区",
                                "children": [
                                    {
                                        "value": "130503001",
                                        "values": "130503001000",
                                        "label": "钢铁路街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130503002",
                                        "values": "130503002000",
                                        "label": "中兴路街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130503003",
                                        "values": "130503003000",
                                        "label": "达活泉街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130503004",
                                        "values": "130503004000",
                                        "label": "张宽街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130503005",
                                        "values": "130503005000",
                                        "label": "章村街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130503006",
                                        "values": "130503006000",
                                        "label": "中华大街街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130503007",
                                        "values": "130503007000",
                                        "label": "团结路街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130503008",
                                        "values": "130503008000",
                                        "label": "泉西街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130503100",
                                        "values": "130503100000",
                                        "label": "南大郭镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130503101",
                                        "values": "130503101000",
                                        "label": "李村镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130503102",
                                        "values": "130503102000",
                                        "label": "南石门镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130503103",
                                        "values": "130503103000",
                                        "label": "羊范镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130503104",
                                        "values": "130503104000",
                                        "label": "皇寺镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130503105",
                                        "values": "130503105000",
                                        "label": "会宁镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130503106",
                                        "values": "130503106000",
                                        "label": "西黄村镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130503107",
                                        "values": "130503107000",
                                        "label": "路罗镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130503108",
                                        "values": "130503108000",
                                        "label": "将军墓镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130503109",
                                        "values": "130503109000",
                                        "label": "浆水镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130503110",
                                        "values": "130503110000",
                                        "label": "宋家庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130503202",
                                        "values": "130503202000",
                                        "label": "太子井乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130503203",
                                        "values": "130503203000",
                                        "label": "龙泉寺乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130503204",
                                        "values": "130503204000",
                                        "label": "北小庄乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130503205",
                                        "values": "130503205000",
                                        "label": "城计头乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130503206",
                                        "values": "130503206000",
                                        "label": "白岸乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130503207",
                                        "values": "130503207000",
                                        "label": "冀家村乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130505",
                                "values": "130505000000",
                                "label": "任泽区",
                                "children": [
                                    {
                                        "value": "130505100",
                                        "values": "130505100000",
                                        "label": "任城镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130505101",
                                        "values": "130505101000",
                                        "label": "邢家湾镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130505102",
                                        "values": "130505102000",
                                        "label": "辛店镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130505103",
                                        "values": "130505103000",
                                        "label": "天口镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130505200",
                                        "values": "130505200000",
                                        "label": "西固城乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130505201",
                                        "values": "130505201000",
                                        "label": "永福庄乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130505202",
                                        "values": "130505202000",
                                        "label": "大屯乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130505203",
                                        "values": "130505203000",
                                        "label": "骆庄乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130506",
                                "values": "130506000000",
                                "label": "南和区",
                                "children": [
                                    {
                                        "value": "130506100",
                                        "values": "130506100000",
                                        "label": "和阳镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130506101",
                                        "values": "130506101000",
                                        "label": "贾宋镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130506102",
                                        "values": "130506102000",
                                        "label": "郝桥镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130506200",
                                        "values": "130506200000",
                                        "label": "东三召乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130506201",
                                        "values": "130506201000",
                                        "label": "阎里乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130506202",
                                        "values": "130506202000",
                                        "label": "河郭乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130506203",
                                        "values": "130506203000",
                                        "label": "史召乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130506204",
                                        "values": "130506204000",
                                        "label": "三思乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130522",
                                "values": "130522000000",
                                "label": "临城县",
                                "children": [
                                    {
                                        "value": "130522100",
                                        "values": "130522100000",
                                        "label": "临城镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130522101",
                                        "values": "130522101000",
                                        "label": "东镇镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130522102",
                                        "values": "130522102000",
                                        "label": "西竖镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130522103",
                                        "values": "130522103000",
                                        "label": "郝庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130522200",
                                        "values": "130522200000",
                                        "label": "黑城乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130522201",
                                        "values": "130522201000",
                                        "label": "鸭鸽营乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130522203",
                                        "values": "130522203000",
                                        "label": "石城乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130522205",
                                        "values": "130522205000",
                                        "label": "赵庄乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130523",
                                "values": "130523000000",
                                "label": "内丘县",
                                "children": [
                                    {
                                        "value": "130523100",
                                        "values": "130523100000",
                                        "label": "内丘镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130523101",
                                        "values": "130523101000",
                                        "label": "大孟村镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130523102",
                                        "values": "130523102000",
                                        "label": "金店镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130523103",
                                        "values": "130523103000",
                                        "label": "官庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130523104",
                                        "values": "130523104000",
                                        "label": "柳林镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130523200",
                                        "values": "130523200000",
                                        "label": "五郭店乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130523203",
                                        "values": "130523203000",
                                        "label": "南赛乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130523204",
                                        "values": "130523204000",
                                        "label": "獐獏乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130523205",
                                        "values": "130523205000",
                                        "label": "侯家庄乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130524",
                                "values": "130524000000",
                                "label": "柏乡县",
                                "children": [
                                    {
                                        "value": "130524100",
                                        "values": "130524100000",
                                        "label": "柏乡镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130524101",
                                        "values": "130524101000",
                                        "label": "固城店镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130524102",
                                        "values": "130524102000",
                                        "label": "西汪镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130524103",
                                        "values": "130524103000",
                                        "label": "龙华镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130524200",
                                        "values": "130524200000",
                                        "label": "王家庄乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130524203",
                                        "values": "130524203000",
                                        "label": "内步乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130525",
                                "values": "130525000000",
                                "label": "隆尧县",
                                "children": [
                                    {
                                        "value": "130525100",
                                        "values": "130525100000",
                                        "label": "隆尧镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130525101",
                                        "values": "130525101000",
                                        "label": "魏家庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130525102",
                                        "values": "130525102000",
                                        "label": "尹村镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130525103",
                                        "values": "130525103000",
                                        "label": "山口镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130525104",
                                        "values": "130525104000",
                                        "label": "莲子镇镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130525105",
                                        "values": "130525105000",
                                        "label": "固城镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130525106",
                                        "values": "130525106000",
                                        "label": "东良镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130525200",
                                        "values": "130525200000",
                                        "label": "北楼乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130525202",
                                        "values": "130525202000",
                                        "label": "双碑乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130525203",
                                        "values": "130525203000",
                                        "label": "牛家桥乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130525204",
                                        "values": "130525204000",
                                        "label": "千户营乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130525205",
                                        "values": "130525205000",
                                        "label": "大张庄乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130525400",
                                        "values": "130525400000",
                                        "label": "柳行农场",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130528",
                                "values": "130528000000",
                                "label": "宁晋县",
                                "children": [
                                    {
                                        "value": "130528001",
                                        "values": "130528001000",
                                        "label": "宁北街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130528100",
                                        "values": "130528100000",
                                        "label": "凤凰镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130528101",
                                        "values": "130528101000",
                                        "label": "河渠镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130528102",
                                        "values": "130528102000",
                                        "label": "北河庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130528103",
                                        "values": "130528103000",
                                        "label": "耿庄桥镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130528104",
                                        "values": "130528104000",
                                        "label": "东汪镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130528105",
                                        "values": "130528105000",
                                        "label": "贾家口镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130528106",
                                        "values": "130528106000",
                                        "label": "四芝兰镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130528107",
                                        "values": "130528107000",
                                        "label": "大陆村镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130528108",
                                        "values": "130528108000",
                                        "label": "苏家庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130528109",
                                        "values": "130528109000",
                                        "label": "换马店镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130528110",
                                        "values": "130528110000",
                                        "label": "唐邱镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130528200",
                                        "values": "130528200000",
                                        "label": "侯口乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130528202",
                                        "values": "130528202000",
                                        "label": "纪昌庄乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130528205",
                                        "values": "130528205000",
                                        "label": "北鱼乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130528208",
                                        "values": "130528208000",
                                        "label": "徐家河乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130528209",
                                        "values": "130528209000",
                                        "label": "大曹庄乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130528401",
                                        "values": "130528401000",
                                        "label": "大曹庄管理区",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130529",
                                "values": "130529000000",
                                "label": "巨鹿县",
                                "children": [
                                    {
                                        "value": "130529100",
                                        "values": "130529100000",
                                        "label": "巨鹿镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130529101",
                                        "values": "130529101000",
                                        "label": "王虎寨镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130529102",
                                        "values": "130529102000",
                                        "label": "西郭城镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130529103",
                                        "values": "130529103000",
                                        "label": "官亭镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130529104",
                                        "values": "130529104000",
                                        "label": "阎疃镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130529105",
                                        "values": "130529105000",
                                        "label": "小吕寨镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130529106",
                                        "values": "130529106000",
                                        "label": "苏家营镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130529200",
                                        "values": "130529200000",
                                        "label": "堤村乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130529201",
                                        "values": "130529201000",
                                        "label": "张王疃乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130529202",
                                        "values": "130529202000",
                                        "label": "观寨乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130530",
                                "values": "130530000000",
                                "label": "新河县",
                                "children": [
                                    {
                                        "value": "130530100",
                                        "values": "130530100000",
                                        "label": "新河镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130530101",
                                        "values": "130530101000",
                                        "label": "寻寨镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130530200",
                                        "values": "130530200000",
                                        "label": "白神首乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130530201",
                                        "values": "130530201000",
                                        "label": "荆家庄乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130530202",
                                        "values": "130530202000",
                                        "label": "西流乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130530203",
                                        "values": "130530203000",
                                        "label": "仁让里乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130531",
                                "values": "130531000000",
                                "label": "广宗县",
                                "children": [
                                    {
                                        "value": "130531100",
                                        "values": "130531100000",
                                        "label": "广宗镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130531101",
                                        "values": "130531101000",
                                        "label": "冯家寨镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130531102",
                                        "values": "130531102000",
                                        "label": "北塘疃镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130531103",
                                        "values": "130531103000",
                                        "label": "核桃园镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130531200",
                                        "values": "130531200000",
                                        "label": "葫芦乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130531201",
                                        "values": "130531201000",
                                        "label": "大平台乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130531202",
                                        "values": "130531202000",
                                        "label": "件只乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130531204",
                                        "values": "130531204000",
                                        "label": "东召乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130532",
                                "values": "130532000000",
                                "label": "平乡县",
                                "children": [
                                    {
                                        "value": "130532001",
                                        "values": "130532001000",
                                        "label": "平乡县中华路街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130532101",
                                        "values": "130532101000",
                                        "label": "平乡镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130532102",
                                        "values": "130532102000",
                                        "label": "河古庙镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130532200",
                                        "values": "130532200000",
                                        "label": "节固乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130532201",
                                        "values": "130532201000",
                                        "label": "油召乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130532202",
                                        "values": "130532202000",
                                        "label": "田付村乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130532203",
                                        "values": "130532203000",
                                        "label": "寻召乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130533",
                                "values": "130533000000",
                                "label": "威县",
                                "children": [
                                    {
                                        "value": "130533100",
                                        "values": "130533100000",
                                        "label": "洺州镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130533101",
                                        "values": "130533101000",
                                        "label": "梨园屯镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130533102",
                                        "values": "130533102000",
                                        "label": "章台镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130533103",
                                        "values": "130533103000",
                                        "label": "侯贯镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130533104",
                                        "values": "130533104000",
                                        "label": "七级镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130533105",
                                        "values": "130533105000",
                                        "label": "贺营镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130533106",
                                        "values": "130533106000",
                                        "label": "方家营镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130533107",
                                        "values": "130533107000",
                                        "label": "常庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130533108",
                                        "values": "130533108000",
                                        "label": "第什营镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130533109",
                                        "values": "130533109000",
                                        "label": "贺钊镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130533110",
                                        "values": "130533110000",
                                        "label": "赵村镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130533202",
                                        "values": "130533202000",
                                        "label": "枣园乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130533203",
                                        "values": "130533203000",
                                        "label": "固献乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130533206",
                                        "values": "130533206000",
                                        "label": "张家营乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130533207",
                                        "values": "130533207000",
                                        "label": "常屯乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130533209",
                                        "values": "130533209000",
                                        "label": "高公庄乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130534",
                                "values": "130534000000",
                                "label": "清河县",
                                "children": [
                                    {
                                        "value": "130534100",
                                        "values": "130534100000",
                                        "label": "葛仙庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130534101",
                                        "values": "130534101000",
                                        "label": "连庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130534102",
                                        "values": "130534102000",
                                        "label": "油坊镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130534103",
                                        "values": "130534103000",
                                        "label": "谢炉镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130534104",
                                        "values": "130534104000",
                                        "label": "王官庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130534105",
                                        "values": "130534105000",
                                        "label": "坝营镇",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130535",
                                "values": "130535000000",
                                "label": "临西县",
                                "children": [
                                    {
                                        "value": "130535100",
                                        "values": "130535100000",
                                        "label": "临西镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130535101",
                                        "values": "130535101000",
                                        "label": "河西镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130535102",
                                        "values": "130535102000",
                                        "label": "下堡寺镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130535103",
                                        "values": "130535103000",
                                        "label": "尖冢镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130535104",
                                        "values": "130535104000",
                                        "label": "老官寨镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130535105",
                                        "values": "130535105000",
                                        "label": "吕寨镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130535200",
                                        "values": "130535200000",
                                        "label": "东枣园乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130535203",
                                        "values": "130535203000",
                                        "label": "摇鞍镇乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130535204",
                                        "values": "130535204000",
                                        "label": "大刘庄乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130535400",
                                        "values": "130535400000",
                                        "label": "轴承工业园区",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130571",
                                "values": "130571000000",
                                "label": "河北邢台经济开发区",
                                "children": [
                                    {
                                        "value": "130571100",
                                        "values": "130571100000",
                                        "label": "东汪镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130571101",
                                        "values": "130571101000",
                                        "label": "王快镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130571103",
                                        "values": "130571103000",
                                        "label": "沙河城镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130571104",
                                        "values": "130571104000",
                                        "label": "留村镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130571400",
                                        "values": "130571400000",
                                        "label": "市高新技术开发区",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130581",
                                "values": "130581000000",
                                "label": "南宫市",
                                "children": [
                                    {
                                        "value": "130581001",
                                        "values": "130581001000",
                                        "label": "凤岗街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130581002",
                                        "values": "130581002000",
                                        "label": "南杜街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130581003",
                                        "values": "130581003000",
                                        "label": "北胡街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130581004",
                                        "values": "130581004000",
                                        "label": "西丁街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130581100",
                                        "values": "130581100000",
                                        "label": "苏村镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130581101",
                                        "values": "130581101000",
                                        "label": "大高村镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130581102",
                                        "values": "130581102000",
                                        "label": "垂杨镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130581103",
                                        "values": "130581103000",
                                        "label": "明化镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130581104",
                                        "values": "130581104000",
                                        "label": "段芦头镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130581105",
                                        "values": "130581105000",
                                        "label": "紫冢镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130581200",
                                        "values": "130581200000",
                                        "label": "大村乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130581201",
                                        "values": "130581201000",
                                        "label": "南便村乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130581202",
                                        "values": "130581202000",
                                        "label": "大屯乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130581203",
                                        "values": "130581203000",
                                        "label": "王道寨乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130581204",
                                        "values": "130581204000",
                                        "label": "薛吴村乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130582",
                                "values": "130582000000",
                                "label": "沙河市",
                                "children": [
                                    {
                                        "value": "130582001",
                                        "values": "130582001000",
                                        "label": "褡裢街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130582002",
                                        "values": "130582002000",
                                        "label": "桥东街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130582003",
                                        "values": "130582003000",
                                        "label": "桥西街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130582004",
                                        "values": "130582004000",
                                        "label": "赞善街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130582005",
                                        "values": "130582005000",
                                        "label": "周庄街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130582101",
                                        "values": "130582101000",
                                        "label": "新城镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130582102",
                                        "values": "130582102000",
                                        "label": "白塔镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130582103",
                                        "values": "130582103000",
                                        "label": "十里亭镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130582104",
                                        "values": "130582104000",
                                        "label": "綦村镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130582201",
                                        "values": "130582201000",
                                        "label": "册井乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130582202",
                                        "values": "130582202000",
                                        "label": "刘石岗乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130582203",
                                        "values": "130582203000",
                                        "label": "柴关乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130582204",
                                        "values": "130582204000",
                                        "label": "蝉房乡",
                                        "children": []
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "value": "1306",
                        "values": "130600000000",
                        "label": "保定市",
                        "children": [
                            {
                                "value": "130601",
                                "values": "130601000000",
                                "label": "市辖区",
                                "children": []
                            },
                            {
                                "value": "130602",
                                "values": "130602000000",
                                "label": "竞秀区",
                                "children": [
                                    {
                                        "value": "130602001",
                                        "values": "130602001000",
                                        "label": "先锋街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130602002",
                                        "values": "130602002000",
                                        "label": "新市场街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130602003",
                                        "values": "130602003000",
                                        "label": "东风街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130602004",
                                        "values": "130602004000",
                                        "label": "建南街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130602005",
                                        "values": "130602005000",
                                        "label": "韩村北路街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130602200",
                                        "values": "130602200000",
                                        "label": "颉庄乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130602201",
                                        "values": "130602201000",
                                        "label": "富昌乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130602202",
                                        "values": "130602202000",
                                        "label": "韩村乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130602203",
                                        "values": "130602203000",
                                        "label": "南奇乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130602204",
                                        "values": "130602204000",
                                        "label": "江城乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130606",
                                "values": "130606000000",
                                "label": "莲池区",
                                "children": [
                                    {
                                        "value": "130606001",
                                        "values": "130606001000",
                                        "label": "和平里街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130606002",
                                        "values": "130606002000",
                                        "label": "五四路街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130606003",
                                        "values": "130606003000",
                                        "label": "西关街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130606004",
                                        "values": "130606004000",
                                        "label": "中华路街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130606005",
                                        "values": "130606005000",
                                        "label": "东关街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130606006",
                                        "values": "130606006000",
                                        "label": "联盟街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130606007",
                                        "values": "130606007000",
                                        "label": "红星街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130606008",
                                        "values": "130606008000",
                                        "label": "裕华街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130606009",
                                        "values": "130606009000",
                                        "label": "永华街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130606010",
                                        "values": "130606010000",
                                        "label": "南关街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130606100",
                                        "values": "130606100000",
                                        "label": "百楼镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130606200",
                                        "values": "130606200000",
                                        "label": "韩庄乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130606201",
                                        "values": "130606201000",
                                        "label": "东金庄乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130606203",
                                        "values": "130606203000",
                                        "label": "杨庄乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130606204",
                                        "values": "130606204000",
                                        "label": "南大园乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130606205",
                                        "values": "130606205000",
                                        "label": "焦庄乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130606206",
                                        "values": "130606206000",
                                        "label": "五尧乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130607",
                                "values": "130607000000",
                                "label": "满城区",
                                "children": [
                                    {
                                        "value": "130607001",
                                        "values": "130607001000",
                                        "label": "惠阳街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130607100",
                                        "values": "130607100000",
                                        "label": "满城镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130607101",
                                        "values": "130607101000",
                                        "label": "大册营镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130607102",
                                        "values": "130607102000",
                                        "label": "神星镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130607103",
                                        "values": "130607103000",
                                        "label": "南韩村镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130607104",
                                        "values": "130607104000",
                                        "label": "方顺桥镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130607201",
                                        "values": "130607201000",
                                        "label": "于家庄乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130607203",
                                        "values": "130607203000",
                                        "label": "要庄乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130607204",
                                        "values": "130607204000",
                                        "label": "白龙乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130607205",
                                        "values": "130607205000",
                                        "label": "石井乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130607206",
                                        "values": "130607206000",
                                        "label": "坨南乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130607207",
                                        "values": "130607207000",
                                        "label": "刘家台乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130607400",
                                        "values": "130607400000",
                                        "label": "河北满城经济开发区",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130608",
                                "values": "130608000000",
                                "label": "清苑区",
                                "children": [
                                    {
                                        "value": "130608100",
                                        "values": "130608100000",
                                        "label": "清苑镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130608101",
                                        "values": "130608101000",
                                        "label": "冉庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130608102",
                                        "values": "130608102000",
                                        "label": "阳城镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130608103",
                                        "values": "130608103000",
                                        "label": "魏村镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130608104",
                                        "values": "130608104000",
                                        "label": "温仁镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130608105",
                                        "values": "130608105000",
                                        "label": "张登镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130608106",
                                        "values": "130608106000",
                                        "label": "大庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130608107",
                                        "values": "130608107000",
                                        "label": "臧村镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130608108",
                                        "values": "130608108000",
                                        "label": "望亭镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130608200",
                                        "values": "130608200000",
                                        "label": "白团乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130608201",
                                        "values": "130608201000",
                                        "label": "北店乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130608202",
                                        "values": "130608202000",
                                        "label": "石桥乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130608203",
                                        "values": "130608203000",
                                        "label": "李庄乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130608204",
                                        "values": "130608204000",
                                        "label": "北王力乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130608205",
                                        "values": "130608205000",
                                        "label": "东闾乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130608206",
                                        "values": "130608206000",
                                        "label": "何桥乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130608207",
                                        "values": "130608207000",
                                        "label": "孙村乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130608208",
                                        "values": "130608208000",
                                        "label": "阎庄乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130608400",
                                        "values": "130608400000",
                                        "label": "河北清苑经济开发区",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130609",
                                "values": "130609000000",
                                "label": "徐水区",
                                "children": [
                                    {
                                        "value": "130609100",
                                        "values": "130609100000",
                                        "label": "安肃镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130609101",
                                        "values": "130609101000",
                                        "label": "崔庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130609102",
                                        "values": "130609102000",
                                        "label": "大因镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130609103",
                                        "values": "130609103000",
                                        "label": "遂城镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130609104",
                                        "values": "130609104000",
                                        "label": "高林村镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130609105",
                                        "values": "130609105000",
                                        "label": "大王店镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130609106",
                                        "values": "130609106000",
                                        "label": "漕河镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130609107",
                                        "values": "130609107000",
                                        "label": "东史端镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130609108",
                                        "values": "130609108000",
                                        "label": "留村镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130609109",
                                        "values": "130609109000",
                                        "label": "正村镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130609203",
                                        "values": "130609203000",
                                        "label": "户木乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130609204",
                                        "values": "130609204000",
                                        "label": "瀑河乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130609205",
                                        "values": "130609205000",
                                        "label": "东釜山乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130609206",
                                        "values": "130609206000",
                                        "label": "义联庄乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130623",
                                "values": "130623000000",
                                "label": "涞水县",
                                "children": [
                                    {
                                        "value": "130623001",
                                        "values": "130623001000",
                                        "label": "城区社区管理办公室街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130623100",
                                        "values": "130623100000",
                                        "label": "涞水镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130623101",
                                        "values": "130623101000",
                                        "label": "永阳镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130623102",
                                        "values": "130623102000",
                                        "label": "义安镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130623103",
                                        "values": "130623103000",
                                        "label": "石亭镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130623104",
                                        "values": "130623104000",
                                        "label": "赵各庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130623105",
                                        "values": "130623105000",
                                        "label": "九龙镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130623106",
                                        "values": "130623106000",
                                        "label": "三坡镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130623107",
                                        "values": "130623107000",
                                        "label": "一渡镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130623108",
                                        "values": "130623108000",
                                        "label": "明义镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130623109",
                                        "values": "130623109000",
                                        "label": "王村镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130623110",
                                        "values": "130623110000",
                                        "label": "娄村镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130623202",
                                        "values": "130623202000",
                                        "label": "东文山乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130623205",
                                        "values": "130623205000",
                                        "label": "其中口乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130623206",
                                        "values": "130623206000",
                                        "label": "龙门乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130623207",
                                        "values": "130623207000",
                                        "label": "胡家庄乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130624",
                                "values": "130624000000",
                                "label": "阜平县",
                                "children": [
                                    {
                                        "value": "130624100",
                                        "values": "130624100000",
                                        "label": "阜平镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130624101",
                                        "values": "130624101000",
                                        "label": "龙泉关镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130624102",
                                        "values": "130624102000",
                                        "label": "平阳镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130624103",
                                        "values": "130624103000",
                                        "label": "城南庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130624104",
                                        "values": "130624104000",
                                        "label": "天生桥镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130624105",
                                        "values": "130624105000",
                                        "label": "王林口镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130624202",
                                        "values": "130624202000",
                                        "label": "台峪乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130624203",
                                        "values": "130624203000",
                                        "label": "大台乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130624204",
                                        "values": "130624204000",
                                        "label": "史家寨乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130624205",
                                        "values": "130624205000",
                                        "label": "砂窝乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130624206",
                                        "values": "130624206000",
                                        "label": "吴王口乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130624207",
                                        "values": "130624207000",
                                        "label": "夏庄乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130624208",
                                        "values": "130624208000",
                                        "label": "北果元乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130624400",
                                        "values": "130624400000",
                                        "label": "河北阜平经济开发区",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130626",
                                "values": "130626000000",
                                "label": "定兴县",
                                "children": [
                                    {
                                        "value": "130626100",
                                        "values": "130626100000",
                                        "label": "定兴镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130626101",
                                        "values": "130626101000",
                                        "label": "固城镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130626102",
                                        "values": "130626102000",
                                        "label": "贤寓镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130626103",
                                        "values": "130626103000",
                                        "label": "北河镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130626104",
                                        "values": "130626104000",
                                        "label": "天宫寺镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130626105",
                                        "values": "130626105000",
                                        "label": "小朱庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130626106",
                                        "values": "130626106000",
                                        "label": "姚村镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130626200",
                                        "values": "130626200000",
                                        "label": "东落堡乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130626201",
                                        "values": "130626201000",
                                        "label": "高里乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130626202",
                                        "values": "130626202000",
                                        "label": "张家庄乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130626204",
                                        "values": "130626204000",
                                        "label": "肖村乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130626205",
                                        "values": "130626205000",
                                        "label": "柳卓乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130626206",
                                        "values": "130626206000",
                                        "label": "杨村乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130626207",
                                        "values": "130626207000",
                                        "label": "北田乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130626208",
                                        "values": "130626208000",
                                        "label": "北南蔡乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130626209",
                                        "values": "130626209000",
                                        "label": "李郁庄乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130627",
                                "values": "130627000000",
                                "label": "唐县",
                                "children": [
                                    {
                                        "value": "130627100",
                                        "values": "130627100000",
                                        "label": "仁厚镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130627101",
                                        "values": "130627101000",
                                        "label": "王京镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130627102",
                                        "values": "130627102000",
                                        "label": "高昌镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130627103",
                                        "values": "130627103000",
                                        "label": "北罗镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130627104",
                                        "values": "130627104000",
                                        "label": "白合镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130627105",
                                        "values": "130627105000",
                                        "label": "军城镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130627106",
                                        "values": "130627106000",
                                        "label": "川里镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130627107",
                                        "values": "130627107000",
                                        "label": "长古城镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130627108",
                                        "values": "130627108000",
                                        "label": "罗庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130627201",
                                        "values": "130627201000",
                                        "label": "都亭乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130627202",
                                        "values": "130627202000",
                                        "label": "南店头乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130627203",
                                        "values": "130627203000",
                                        "label": "北店头乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130627205",
                                        "values": "130627205000",
                                        "label": "雹水乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130627206",
                                        "values": "130627206000",
                                        "label": "大洋乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130627207",
                                        "values": "130627207000",
                                        "label": "迷城乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130627208",
                                        "values": "130627208000",
                                        "label": "齐家佐乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130627209",
                                        "values": "130627209000",
                                        "label": "羊角乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130627210",
                                        "values": "130627210000",
                                        "label": "石门乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130627211",
                                        "values": "130627211000",
                                        "label": "黄石口乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130627212",
                                        "values": "130627212000",
                                        "label": "倒马关乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130628",
                                "values": "130628000000",
                                "label": "高阳县",
                                "children": [
                                    {
                                        "value": "130628001",
                                        "values": "130628001000",
                                        "label": "高阳县锦华街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130628101",
                                        "values": "130628101000",
                                        "label": "庞口镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130628102",
                                        "values": "130628102000",
                                        "label": "西演镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130628103",
                                        "values": "130628103000",
                                        "label": "邢家南镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130628104",
                                        "values": "130628104000",
                                        "label": "晋庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130628105",
                                        "values": "130628105000",
                                        "label": "小王果庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130628202",
                                        "values": "130628202000",
                                        "label": "蒲口乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130628205",
                                        "values": "130628205000",
                                        "label": "庞家佐乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130628400",
                                        "values": "130628400000",
                                        "label": "高阳县经济开发区",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130629",
                                "values": "130629000000",
                                "label": "容城县",
                                "children": [
                                    {
                                        "value": "130629100",
                                        "values": "130629100000",
                                        "label": "容城镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130629101",
                                        "values": "130629101000",
                                        "label": "小里镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130629102",
                                        "values": "130629102000",
                                        "label": "南张镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130629103",
                                        "values": "130629103000",
                                        "label": "大河镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130629104",
                                        "values": "130629104000",
                                        "label": "晾马台镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130629200",
                                        "values": "130629200000",
                                        "label": "八于乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130629201",
                                        "values": "130629201000",
                                        "label": "贾光乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130629203",
                                        "values": "130629203000",
                                        "label": "平王乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130630",
                                "values": "130630000000",
                                "label": "涞源县",
                                "children": [
                                    {
                                        "value": "130630100",
                                        "values": "130630100000",
                                        "label": "涞源镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130630101",
                                        "values": "130630101000",
                                        "label": "银坊镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130630102",
                                        "values": "130630102000",
                                        "label": "走马驿镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130630103",
                                        "values": "130630103000",
                                        "label": "水堡镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130630104",
                                        "values": "130630104000",
                                        "label": "王安镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130630105",
                                        "values": "130630105000",
                                        "label": "杨家庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130630106",
                                        "values": "130630106000",
                                        "label": "白石山镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130630107",
                                        "values": "130630107000",
                                        "label": "南屯镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130630201",
                                        "values": "130630201000",
                                        "label": "南马庄乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130630202",
                                        "values": "130630202000",
                                        "label": "北石佛乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130630203",
                                        "values": "130630203000",
                                        "label": "金家井乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130630204",
                                        "values": "130630204000",
                                        "label": "留家庄乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130630205",
                                        "values": "130630205000",
                                        "label": "上庄乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130630206",
                                        "values": "130630206000",
                                        "label": "东团堡乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130630207",
                                        "values": "130630207000",
                                        "label": "塔崖驿乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130630208",
                                        "values": "130630208000",
                                        "label": "乌龙沟乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130630209",
                                        "values": "130630209000",
                                        "label": "烟煤洞乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130631",
                                "values": "130631000000",
                                "label": "望都县",
                                "children": [
                                    {
                                        "value": "130631100",
                                        "values": "130631100000",
                                        "label": "望都镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130631101",
                                        "values": "130631101000",
                                        "label": "固店镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130631102",
                                        "values": "130631102000",
                                        "label": "贾村镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130631103",
                                        "values": "130631103000",
                                        "label": "中韩庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130631104",
                                        "values": "130631104000",
                                        "label": "寺庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130631105",
                                        "values": "130631105000",
                                        "label": "赵庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130631202",
                                        "values": "130631202000",
                                        "label": "黑堡乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130631204",
                                        "values": "130631204000",
                                        "label": "高岭乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130631400",
                                        "values": "130631400000",
                                        "label": "望都经济开发区",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130632",
                                "values": "130632000000",
                                "label": "安新县",
                                "children": [
                                    {
                                        "value": "130632100",
                                        "values": "130632100000",
                                        "label": "安新镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130632101",
                                        "values": "130632101000",
                                        "label": "大王镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130632102",
                                        "values": "130632102000",
                                        "label": "三台镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130632103",
                                        "values": "130632103000",
                                        "label": "端村镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130632104",
                                        "values": "130632104000",
                                        "label": "赵北口镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130632105",
                                        "values": "130632105000",
                                        "label": "同口镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130632106",
                                        "values": "130632106000",
                                        "label": "刘李庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130632107",
                                        "values": "130632107000",
                                        "label": "安州镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130632108",
                                        "values": "130632108000",
                                        "label": "老河头镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130632200",
                                        "values": "130632200000",
                                        "label": "圈头乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130632201",
                                        "values": "130632201000",
                                        "label": "寨里乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130632202",
                                        "values": "130632202000",
                                        "label": "芦庄乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130632203",
                                        "values": "130632203000",
                                        "label": "龙化乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130633",
                                "values": "130633000000",
                                "label": "易县",
                                "children": [
                                    {
                                        "value": "130633100",
                                        "values": "130633100000",
                                        "label": "易州镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130633101",
                                        "values": "130633101000",
                                        "label": "梁格庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130633102",
                                        "values": "130633102000",
                                        "label": "西陵镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130633103",
                                        "values": "130633103000",
                                        "label": "裴山镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130633104",
                                        "values": "130633104000",
                                        "label": "塘湖镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130633105",
                                        "values": "130633105000",
                                        "label": "狼牙山镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130633106",
                                        "values": "130633106000",
                                        "label": "良岗镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130633107",
                                        "values": "130633107000",
                                        "label": "紫荆关镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130633108",
                                        "values": "130633108000",
                                        "label": "高村镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130633200",
                                        "values": "130633200000",
                                        "label": "桥头乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130633201",
                                        "values": "130633201000",
                                        "label": "白马乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130633202",
                                        "values": "130633202000",
                                        "label": "流井乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130633204",
                                        "values": "130633204000",
                                        "label": "高陌乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130633205",
                                        "values": "130633205000",
                                        "label": "大龙华乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130633206",
                                        "values": "130633206000",
                                        "label": "安格庄乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130633207",
                                        "values": "130633207000",
                                        "label": "凌云册乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130633208",
                                        "values": "130633208000",
                                        "label": "西山北乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130633209",
                                        "values": "130633209000",
                                        "label": "尉都乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130633210",
                                        "values": "130633210000",
                                        "label": "独乐乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130633211",
                                        "values": "130633211000",
                                        "label": "七峪乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130633212",
                                        "values": "130633212000",
                                        "label": "富岗乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130633213",
                                        "values": "130633213000",
                                        "label": "坡仓乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130633214",
                                        "values": "130633214000",
                                        "label": "牛岗乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130633215",
                                        "values": "130633215000",
                                        "label": "桥家河乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130633216",
                                        "values": "130633216000",
                                        "label": "甘河净乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130633217",
                                        "values": "130633217000",
                                        "label": "蔡家峪乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130633218",
                                        "values": "130633218000",
                                        "label": "南城司乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130634",
                                "values": "130634000000",
                                "label": "曲阳县",
                                "children": [
                                    {
                                        "value": "130634100",
                                        "values": "130634100000",
                                        "label": "恒州镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130634101",
                                        "values": "130634101000",
                                        "label": "灵山镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130634102",
                                        "values": "130634102000",
                                        "label": "燕赵镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130634103",
                                        "values": "130634103000",
                                        "label": "羊平镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130634104",
                                        "values": "130634104000",
                                        "label": "文德镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130634105",
                                        "values": "130634105000",
                                        "label": "晓林镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130634106",
                                        "values": "130634106000",
                                        "label": "邸村镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130634107",
                                        "values": "130634107000",
                                        "label": "齐村镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130634108",
                                        "values": "130634108000",
                                        "label": "孝墓镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130634200",
                                        "values": "130634200000",
                                        "label": "路庄子乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130634201",
                                        "values": "130634201000",
                                        "label": "下河乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130634202",
                                        "values": "130634202000",
                                        "label": "庄窠乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130634205",
                                        "values": "130634205000",
                                        "label": "东旺乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130634208",
                                        "values": "130634208000",
                                        "label": "产德乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130634210",
                                        "values": "130634210000",
                                        "label": "党城乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130634211",
                                        "values": "130634211000",
                                        "label": "郎家庄乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130634212",
                                        "values": "130634212000",
                                        "label": "范家庄乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130634213",
                                        "values": "130634213000",
                                        "label": "北台乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130635",
                                "values": "130635000000",
                                "label": "蠡县",
                                "children": [
                                    {
                                        "value": "130635100",
                                        "values": "130635100000",
                                        "label": "蠡吾镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130635101",
                                        "values": "130635101000",
                                        "label": "留史镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130635102",
                                        "values": "130635102000",
                                        "label": "大百尺镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130635103",
                                        "values": "130635103000",
                                        "label": "辛兴镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130635104",
                                        "values": "130635104000",
                                        "label": "北郭丹镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130635105",
                                        "values": "130635105000",
                                        "label": "万安镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130635106",
                                        "values": "130635106000",
                                        "label": "桑园镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130635107",
                                        "values": "130635107000",
                                        "label": "南庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130635108",
                                        "values": "130635108000",
                                        "label": "大曲堤镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130635109",
                                        "values": "130635109000",
                                        "label": "鲍墟镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130635200",
                                        "values": "130635200000",
                                        "label": "小陈乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130635201",
                                        "values": "130635201000",
                                        "label": "林堡乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130635202",
                                        "values": "130635202000",
                                        "label": "北埝头乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130636",
                                "values": "130636000000",
                                "label": "顺平县",
                                "children": [
                                    {
                                        "value": "130636100",
                                        "values": "130636100000",
                                        "label": "蒲阳镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130636101",
                                        "values": "130636101000",
                                        "label": "高于铺镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130636102",
                                        "values": "130636102000",
                                        "label": "腰山镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130636103",
                                        "values": "130636103000",
                                        "label": "蒲上镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130636104",
                                        "values": "130636104000",
                                        "label": "神南镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130636201",
                                        "values": "130636201000",
                                        "label": "白云乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130636202",
                                        "values": "130636202000",
                                        "label": "河口乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130636203",
                                        "values": "130636203000",
                                        "label": "安阳乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130636204",
                                        "values": "130636204000",
                                        "label": "台鱼乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130636205",
                                        "values": "130636205000",
                                        "label": "大悲乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130637",
                                "values": "130637000000",
                                "label": "博野县",
                                "children": [
                                    {
                                        "value": "130637100",
                                        "values": "130637100000",
                                        "label": "博野镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130637101",
                                        "values": "130637101000",
                                        "label": "小店镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130637102",
                                        "values": "130637102000",
                                        "label": "程委镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130637103",
                                        "values": "130637103000",
                                        "label": "东墟镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130637104",
                                        "values": "130637104000",
                                        "label": "北杨镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130637105",
                                        "values": "130637105000",
                                        "label": "城东镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130637106",
                                        "values": "130637106000",
                                        "label": "南小王镇",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130638",
                                "values": "130638000000",
                                "label": "雄县",
                                "children": [
                                    {
                                        "value": "130638100",
                                        "values": "130638100000",
                                        "label": "雄州镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130638101",
                                        "values": "130638101000",
                                        "label": "昝岗镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130638102",
                                        "values": "130638102000",
                                        "label": "大营镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130638103",
                                        "values": "130638103000",
                                        "label": "龙湾镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130638104",
                                        "values": "130638104000",
                                        "label": "朱各庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130638105",
                                        "values": "130638105000",
                                        "label": "米家务镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130638106",
                                        "values": "130638106000",
                                        "label": "鄚州镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130638107",
                                        "values": "130638107000",
                                        "label": "苟各庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130638200",
                                        "values": "130638200000",
                                        "label": "北沙口乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130638203",
                                        "values": "130638203000",
                                        "label": "双堂乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130638204",
                                        "values": "130638204000",
                                        "label": "张岗乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130638205",
                                        "values": "130638205000",
                                        "label": "七间房乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130671",
                                "values": "130671000000",
                                "label": "保定高新技术产业开发区",
                                "children": [
                                    {
                                        "value": "130671202",
                                        "values": "130671202000",
                                        "label": "贤台乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130671205",
                                        "values": "130671205000",
                                        "label": "大马坊乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130671400",
                                        "values": "130671400000",
                                        "label": "保定国家高新区",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130672",
                                "values": "130672000000",
                                "label": "保定白沟新城",
                                "children": [
                                    {
                                        "value": "130672103",
                                        "values": "130672103000",
                                        "label": "白沟镇",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130681",
                                "values": "130681000000",
                                "label": "涿州市",
                                "children": [
                                    {
                                        "value": "130681001",
                                        "values": "130681001000",
                                        "label": "双塔街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130681002",
                                        "values": "130681002000",
                                        "label": "桃园街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130681003",
                                        "values": "130681003000",
                                        "label": "清凉寺街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130681100",
                                        "values": "130681100000",
                                        "label": "松林店镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130681101",
                                        "values": "130681101000",
                                        "label": "码头镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130681102",
                                        "values": "130681102000",
                                        "label": "东城坊镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130681103",
                                        "values": "130681103000",
                                        "label": "高官庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130681104",
                                        "values": "130681104000",
                                        "label": "东仙坡镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130681105",
                                        "values": "130681105000",
                                        "label": "百尺竿镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130681106",
                                        "values": "130681106000",
                                        "label": "义和庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130681107",
                                        "values": "130681107000",
                                        "label": "刁窝镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130681108",
                                        "values": "130681108000",
                                        "label": "林家屯镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130681109",
                                        "values": "130681109000",
                                        "label": "豆庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130681204",
                                        "values": "130681204000",
                                        "label": "孙家庄乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130681400",
                                        "values": "130681400000",
                                        "label": "高新技术产业开发区管理委员会",
                                        "children": []
                                    },
                                    {
                                        "value": "130681401",
                                        "values": "130681401000",
                                        "label": "京南经济开发区管理委员会",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130682",
                                "values": "130682000000",
                                "label": "定州市",
                                "children": [
                                    {
                                        "value": "130682001",
                                        "values": "130682001000",
                                        "label": "南城区街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130682002",
                                        "values": "130682002000",
                                        "label": "北城区街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130682003",
                                        "values": "130682003000",
                                        "label": "西城区街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130682004",
                                        "values": "130682004000",
                                        "label": "长安路街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130682100",
                                        "values": "130682100000",
                                        "label": "留早镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130682101",
                                        "values": "130682101000",
                                        "label": "清风店镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130682102",
                                        "values": "130682102000",
                                        "label": "庞村镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130682103",
                                        "values": "130682103000",
                                        "label": "砖路镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130682104",
                                        "values": "130682104000",
                                        "label": "明月店镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130682105",
                                        "values": "130682105000",
                                        "label": "叮咛店镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130682106",
                                        "values": "130682106000",
                                        "label": "东亭镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130682107",
                                        "values": "130682107000",
                                        "label": "大辛庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130682108",
                                        "values": "130682108000",
                                        "label": "东旺镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130682109",
                                        "values": "130682109000",
                                        "label": "高蓬镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130682110",
                                        "values": "130682110000",
                                        "label": "邢邑镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130682111",
                                        "values": "130682111000",
                                        "label": "李亲顾镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130682112",
                                        "values": "130682112000",
                                        "label": "子位镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130682113",
                                        "values": "130682113000",
                                        "label": "开元镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130682115",
                                        "values": "130682115000",
                                        "label": "周村镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130682116",
                                        "values": "130682116000",
                                        "label": "息冢镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130682203",
                                        "values": "130682203000",
                                        "label": "东留春乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130682204",
                                        "values": "130682204000",
                                        "label": "号头庄回族乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130682205",
                                        "values": "130682205000",
                                        "label": "杨家庄乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130682206",
                                        "values": "130682206000",
                                        "label": "大鹿庄乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130682208",
                                        "values": "130682208000",
                                        "label": "西城乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130683",
                                "values": "130683000000",
                                "label": "安国市",
                                "children": [
                                    {
                                        "value": "130683001",
                                        "values": "130683001000",
                                        "label": "药都街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130683002",
                                        "values": "130683002000",
                                        "label": "祁州路街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130683101",
                                        "values": "130683101000",
                                        "label": "伍仁桥镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130683102",
                                        "values": "130683102000",
                                        "label": "石佛镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130683103",
                                        "values": "130683103000",
                                        "label": "郑章镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130683104",
                                        "values": "130683104000",
                                        "label": "大五女镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130683105",
                                        "values": "130683105000",
                                        "label": "西佛落镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130683106",
                                        "values": "130683106000",
                                        "label": "西城镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130683200",
                                        "values": "130683200000",
                                        "label": "明官店乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130683201",
                                        "values": "130683201000",
                                        "label": "南娄底乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130683204",
                                        "values": "130683204000",
                                        "label": "北段村乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130683400",
                                        "values": "130683400000",
                                        "label": "现代中药工业园区",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130684",
                                "values": "130684000000",
                                "label": "高碑店市",
                                "children": [
                                    {
                                        "value": "130684001",
                                        "values": "130684001000",
                                        "label": "和平街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130684002",
                                        "values": "130684002000",
                                        "label": "军城街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130684003",
                                        "values": "130684003000",
                                        "label": "东盛街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130684004",
                                        "values": "130684004000",
                                        "label": "北城街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130684005",
                                        "values": "130684005000",
                                        "label": "兴华路街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130684100",
                                        "values": "130684100000",
                                        "label": "方官镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130684101",
                                        "values": "130684101000",
                                        "label": "新城镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130684102",
                                        "values": "130684102000",
                                        "label": "泗庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130684104",
                                        "values": "130684104000",
                                        "label": "辛立庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130684105",
                                        "values": "130684105000",
                                        "label": "东马营镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130684106",
                                        "values": "130684106000",
                                        "label": "辛桥镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130684107",
                                        "values": "130684107000",
                                        "label": "肖官营镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130684108",
                                        "values": "130684108000",
                                        "label": "张六庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130684109",
                                        "values": "130684109000",
                                        "label": "梁家营镇",
                                        "children": []
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "value": "1307",
                        "values": "130700000000",
                        "label": "张家口市",
                        "children": [
                            {
                                "value": "130701",
                                "values": "130701000000",
                                "label": "市辖区",
                                "children": []
                            },
                            {
                                "value": "130702",
                                "values": "130702000000",
                                "label": "桥东区",
                                "children": [
                                    {
                                        "value": "130702001",
                                        "values": "130702001000",
                                        "label": "红旗楼街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130702002",
                                        "values": "130702002000",
                                        "label": "胜利北路街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130702003",
                                        "values": "130702003000",
                                        "label": "五一路街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130702004",
                                        "values": "130702004000",
                                        "label": "花园街街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130702005",
                                        "values": "130702005000",
                                        "label": "工业路街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130702101",
                                        "values": "130702101000",
                                        "label": "姚家庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130702102",
                                        "values": "130702102000",
                                        "label": "大仓盖镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130702202",
                                        "values": "130702202000",
                                        "label": "东望山乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130703",
                                "values": "130703000000",
                                "label": "桥西区",
                                "children": [
                                    {
                                        "value": "130703001",
                                        "values": "130703001000",
                                        "label": "新华街街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130703002",
                                        "values": "130703002000",
                                        "label": "大境门街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130703003",
                                        "values": "130703003000",
                                        "label": "明德北街街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130703004",
                                        "values": "130703004000",
                                        "label": "明德南街街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130703005",
                                        "values": "130703005000",
                                        "label": "堡子里街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130703006",
                                        "values": "130703006000",
                                        "label": "南营坊街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130703007",
                                        "values": "130703007000",
                                        "label": "工人新村街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130703100",
                                        "values": "130703100000",
                                        "label": "东窑子镇",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130705",
                                "values": "130705000000",
                                "label": "宣化区",
                                "children": [
                                    {
                                        "value": "130705001",
                                        "values": "130705001000",
                                        "label": "天泰寺街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130705002",
                                        "values": "130705002000",
                                        "label": "皇城街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130705003",
                                        "values": "130705003000",
                                        "label": "南关街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130705004",
                                        "values": "130705004000",
                                        "label": "南大街街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130705005",
                                        "values": "130705005000",
                                        "label": "大北街街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130705006",
                                        "values": "130705006000",
                                        "label": "工业街街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130705007",
                                        "values": "130705007000",
                                        "label": "建国街街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130705100",
                                        "values": "130705100000",
                                        "label": "庞家堡镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130705101",
                                        "values": "130705101000",
                                        "label": "深井镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130705102",
                                        "values": "130705102000",
                                        "label": "崞村镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130705104",
                                        "values": "130705104000",
                                        "label": "洋河南镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130705106",
                                        "values": "130705106000",
                                        "label": "贾家营镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130705107",
                                        "values": "130705107000",
                                        "label": "顾家营镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130705108",
                                        "values": "130705108000",
                                        "label": "赵川镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130705200",
                                        "values": "130705200000",
                                        "label": "河子西乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130705201",
                                        "values": "130705201000",
                                        "label": "春光乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130705202",
                                        "values": "130705202000",
                                        "label": "侯家庙乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130705204",
                                        "values": "130705204000",
                                        "label": "李家堡乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130705205",
                                        "values": "130705205000",
                                        "label": "王家湾乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130705206",
                                        "values": "130705206000",
                                        "label": "塔儿村乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130705207",
                                        "values": "130705207000",
                                        "label": "江家屯乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130706",
                                "values": "130706000000",
                                "label": "下花园区",
                                "children": [
                                    {
                                        "value": "130706001",
                                        "values": "130706001000",
                                        "label": "城镇街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130706002",
                                        "values": "130706002000",
                                        "label": "煤矿街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130706200",
                                        "values": "130706200000",
                                        "label": "花园乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130706201",
                                        "values": "130706201000",
                                        "label": "辛庄子乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130706202",
                                        "values": "130706202000",
                                        "label": "定方水乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130706203",
                                        "values": "130706203000",
                                        "label": "段家堡乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130708",
                                "values": "130708000000",
                                "label": "万全区",
                                "children": [
                                    {
                                        "value": "130708001",
                                        "values": "130708001000",
                                        "label": "孔家庄街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130708100",
                                        "values": "130708100000",
                                        "label": "孔家庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130708101",
                                        "values": "130708101000",
                                        "label": "万全镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130708102",
                                        "values": "130708102000",
                                        "label": "洗马林镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130708103",
                                        "values": "130708103000",
                                        "label": "郭磊庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130708200",
                                        "values": "130708200000",
                                        "label": "膳房堡乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130708201",
                                        "values": "130708201000",
                                        "label": "北新屯乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130708202",
                                        "values": "130708202000",
                                        "label": "宣平堡乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130708203",
                                        "values": "130708203000",
                                        "label": "高庙堡乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130708204",
                                        "values": "130708204000",
                                        "label": "旧堡乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130708205",
                                        "values": "130708205000",
                                        "label": "安家堡乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130708206",
                                        "values": "130708206000",
                                        "label": "北沙城乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130709",
                                "values": "130709000000",
                                "label": "崇礼区",
                                "children": [
                                    {
                                        "value": "130709001",
                                        "values": "130709001000",
                                        "label": "西湾子街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130709100",
                                        "values": "130709100000",
                                        "label": "西湾子镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130709101",
                                        "values": "130709101000",
                                        "label": "高家营镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130709200",
                                        "values": "130709200000",
                                        "label": "四台嘴乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130709201",
                                        "values": "130709201000",
                                        "label": "红旗营乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130709202",
                                        "values": "130709202000",
                                        "label": "石窑子乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130709203",
                                        "values": "130709203000",
                                        "label": "驿马图乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130709204",
                                        "values": "130709204000",
                                        "label": "石嘴子乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130709205",
                                        "values": "130709205000",
                                        "label": "狮子沟乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130709206",
                                        "values": "130709206000",
                                        "label": "清三营乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130709207",
                                        "values": "130709207000",
                                        "label": "白旗乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130722",
                                "values": "130722000000",
                                "label": "张北县",
                                "children": [
                                    {
                                        "value": "130722100",
                                        "values": "130722100000",
                                        "label": "张北镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130722101",
                                        "values": "130722101000",
                                        "label": "公会镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130722102",
                                        "values": "130722102000",
                                        "label": "二台镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130722103",
                                        "values": "130722103000",
                                        "label": "大囫囵镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130722104",
                                        "values": "130722104000",
                                        "label": "小二台镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130722106",
                                        "values": "130722106000",
                                        "label": "油篓沟镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130722107",
                                        "values": "130722107000",
                                        "label": "大河镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130722200",
                                        "values": "130722200000",
                                        "label": "台路沟乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130722202",
                                        "values": "130722202000",
                                        "label": "馒头营乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130722203",
                                        "values": "130722203000",
                                        "label": "二泉井乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130722204",
                                        "values": "130722204000",
                                        "label": "单晶河乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130722206",
                                        "values": "130722206000",
                                        "label": "海流图乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130722207",
                                        "values": "130722207000",
                                        "label": "两面井乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130722208",
                                        "values": "130722208000",
                                        "label": "大西湾乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130722209",
                                        "values": "130722209000",
                                        "label": "郝家营乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130722210",
                                        "values": "130722210000",
                                        "label": "白庙滩乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130722212",
                                        "values": "130722212000",
                                        "label": "战海乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130722213",
                                        "values": "130722213000",
                                        "label": "三号乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130723",
                                "values": "130723000000",
                                "label": "康保县",
                                "children": [
                                    {
                                        "value": "130723100",
                                        "values": "130723100000",
                                        "label": "康保镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130723101",
                                        "values": "130723101000",
                                        "label": "张纪镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130723102",
                                        "values": "130723102000",
                                        "label": "土城子镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130723103",
                                        "values": "130723103000",
                                        "label": "邓油坊镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130723104",
                                        "values": "130723104000",
                                        "label": "李家地镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130723105",
                                        "values": "130723105000",
                                        "label": "照阳河镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130723106",
                                        "values": "130723106000",
                                        "label": "屯垦镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130723200",
                                        "values": "130723200000",
                                        "label": "闫油坊乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130723201",
                                        "values": "130723201000",
                                        "label": "丹清河乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130723202",
                                        "values": "130723202000",
                                        "label": "哈咇嘎乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130723203",
                                        "values": "130723203000",
                                        "label": "二号卜乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130723204",
                                        "values": "130723204000",
                                        "label": "芦家营乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130723205",
                                        "values": "130723205000",
                                        "label": "忠义乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130723206",
                                        "values": "130723206000",
                                        "label": "处长地乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130723207",
                                        "values": "130723207000",
                                        "label": "满德堂乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130723500",
                                        "values": "130723500000",
                                        "label": "康保牧场",
                                        "children": []
                                    },
                                    {
                                        "value": "130723501",
                                        "values": "130723501000",
                                        "label": "屯垦林场",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130724",
                                "values": "130724000000",
                                "label": "沽源县",
                                "children": [
                                    {
                                        "value": "130724100",
                                        "values": "130724100000",
                                        "label": "平定堡镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130724101",
                                        "values": "130724101000",
                                        "label": "小厂镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130724102",
                                        "values": "130724102000",
                                        "label": "黄盖淖镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130724103",
                                        "values": "130724103000",
                                        "label": "九连城镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130724200",
                                        "values": "130724200000",
                                        "label": "高山堡乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130724201",
                                        "values": "130724201000",
                                        "label": "小河子乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130724202",
                                        "values": "130724202000",
                                        "label": "二道渠乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130724203",
                                        "values": "130724203000",
                                        "label": "大二号回族乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130724204",
                                        "values": "130724204000",
                                        "label": "闪电河乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130724205",
                                        "values": "130724205000",
                                        "label": "长梁乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130724206",
                                        "values": "130724206000",
                                        "label": "丰源店乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130724207",
                                        "values": "130724207000",
                                        "label": "西辛营乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130724208",
                                        "values": "130724208000",
                                        "label": "莲花滩乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130724209",
                                        "values": "130724209000",
                                        "label": "白土窑乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130725",
                                "values": "130725000000",
                                "label": "尚义县",
                                "children": [
                                    {
                                        "value": "130725100",
                                        "values": "130725100000",
                                        "label": "南壕堑镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130725101",
                                        "values": "130725101000",
                                        "label": "大青沟镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130725102",
                                        "values": "130725102000",
                                        "label": "八道沟镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130725103",
                                        "values": "130725103000",
                                        "label": "红土梁镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130725104",
                                        "values": "130725104000",
                                        "label": "小蒜沟镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130725105",
                                        "values": "130725105000",
                                        "label": "三工地镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130725106",
                                        "values": "130725106000",
                                        "label": "满井镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130725200",
                                        "values": "130725200000",
                                        "label": "大营盘乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130725201",
                                        "values": "130725201000",
                                        "label": "大苏计乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130725202",
                                        "values": "130725202000",
                                        "label": "石井乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130725205",
                                        "values": "130725205000",
                                        "label": "七甲乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130725206",
                                        "values": "130725206000",
                                        "label": "套里庄乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130725207",
                                        "values": "130725207000",
                                        "label": "甲石河乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130725208",
                                        "values": "130725208000",
                                        "label": "下马圈乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130726",
                                "values": "130726000000",
                                "label": "蔚县",
                                "children": [
                                    {
                                        "value": "130726100",
                                        "values": "130726100000",
                                        "label": "蔚州镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130726101",
                                        "values": "130726101000",
                                        "label": "代王城镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130726102",
                                        "values": "130726102000",
                                        "label": "西合营镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130726103",
                                        "values": "130726103000",
                                        "label": "吉家庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130726104",
                                        "values": "130726104000",
                                        "label": "白乐镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130726105",
                                        "values": "130726105000",
                                        "label": "暖泉镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130726106",
                                        "values": "130726106000",
                                        "label": "南留庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130726107",
                                        "values": "130726107000",
                                        "label": "北水泉镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130726108",
                                        "values": "130726108000",
                                        "label": "桃花镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130726109",
                                        "values": "130726109000",
                                        "label": "阳眷镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130726110",
                                        "values": "130726110000",
                                        "label": "宋家庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130726200",
                                        "values": "130726200000",
                                        "label": "下宫村乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130726202",
                                        "values": "130726202000",
                                        "label": "南杨庄乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130726203",
                                        "values": "130726203000",
                                        "label": "柏树乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130726204",
                                        "values": "130726204000",
                                        "label": "常宁乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130726205",
                                        "values": "130726205000",
                                        "label": "涌泉庄乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130726206",
                                        "values": "130726206000",
                                        "label": "杨庄窠乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130726207",
                                        "values": "130726207000",
                                        "label": "南岭庄乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130726208",
                                        "values": "130726208000",
                                        "label": "陈家洼乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130726209",
                                        "values": "130726209000",
                                        "label": "黄梅乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130726210",
                                        "values": "130726210000",
                                        "label": "白草村乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130726211",
                                        "values": "130726211000",
                                        "label": "草沟堡乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130727",
                                "values": "130727000000",
                                "label": "阳原县",
                                "children": [
                                    {
                                        "value": "130727100",
                                        "values": "130727100000",
                                        "label": "西城镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130727101",
                                        "values": "130727101000",
                                        "label": "东城镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130727102",
                                        "values": "130727102000",
                                        "label": "化稍营镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130727103",
                                        "values": "130727103000",
                                        "label": "揣骨疃镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130727104",
                                        "values": "130727104000",
                                        "label": "东井集镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130727200",
                                        "values": "130727200000",
                                        "label": "要家庄乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130727201",
                                        "values": "130727201000",
                                        "label": "东坊城堡乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130727202",
                                        "values": "130727202000",
                                        "label": "井儿沟乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130727203",
                                        "values": "130727203000",
                                        "label": "三马坊乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130727204",
                                        "values": "130727204000",
                                        "label": "高墙乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130727205",
                                        "values": "130727205000",
                                        "label": "大田洼乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130727206",
                                        "values": "130727206000",
                                        "label": "辛堡乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130727207",
                                        "values": "130727207000",
                                        "label": "马圈堡乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130727208",
                                        "values": "130727208000",
                                        "label": "浮图讲乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130728",
                                "values": "130728000000",
                                "label": "怀安县",
                                "children": [
                                    {
                                        "value": "130728100",
                                        "values": "130728100000",
                                        "label": "柴沟堡镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130728101",
                                        "values": "130728101000",
                                        "label": "左卫镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130728102",
                                        "values": "130728102000",
                                        "label": "头百户镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130728103",
                                        "values": "130728103000",
                                        "label": "怀安城镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130728200",
                                        "values": "130728200000",
                                        "label": "渡口堡乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130728201",
                                        "values": "130728201000",
                                        "label": "第六屯乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130728202",
                                        "values": "130728202000",
                                        "label": "西湾堡乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130728203",
                                        "values": "130728203000",
                                        "label": "西沙城乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130728204",
                                        "values": "130728204000",
                                        "label": "太平庄乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130728205",
                                        "values": "130728205000",
                                        "label": "王虎屯乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130728206",
                                        "values": "130728206000",
                                        "label": "第三堡乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130730",
                                "values": "130730000000",
                                "label": "怀来县",
                                "children": [
                                    {
                                        "value": "130730100",
                                        "values": "130730100000",
                                        "label": "沙城镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130730101",
                                        "values": "130730101000",
                                        "label": "北辛堡镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130730102",
                                        "values": "130730102000",
                                        "label": "新保安镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130730103",
                                        "values": "130730103000",
                                        "label": "东花园镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130730104",
                                        "values": "130730104000",
                                        "label": "官厅镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130730105",
                                        "values": "130730105000",
                                        "label": "桑园镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130730106",
                                        "values": "130730106000",
                                        "label": "存瑞镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130730107",
                                        "values": "130730107000",
                                        "label": "土木镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130730108",
                                        "values": "130730108000",
                                        "label": "大黄庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130730109",
                                        "values": "130730109000",
                                        "label": "西八里镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130730110",
                                        "values": "130730110000",
                                        "label": "小南辛堡镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130730200",
                                        "values": "130730200000",
                                        "label": "狼山乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130730203",
                                        "values": "130730203000",
                                        "label": "鸡鸣驿乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130730205",
                                        "values": "130730205000",
                                        "label": "东八里乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130730207",
                                        "values": "130730207000",
                                        "label": "瑞云观乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130730208",
                                        "values": "130730208000",
                                        "label": "孙庄子乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130730210",
                                        "values": "130730210000",
                                        "label": "王家楼回族乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130731",
                                "values": "130731000000",
                                "label": "涿鹿县",
                                "children": [
                                    {
                                        "value": "130731100",
                                        "values": "130731100000",
                                        "label": "涿鹿镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130731101",
                                        "values": "130731101000",
                                        "label": "张家堡镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130731102",
                                        "values": "130731102000",
                                        "label": "武家沟镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130731103",
                                        "values": "130731103000",
                                        "label": "五堡镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130731104",
                                        "values": "130731104000",
                                        "label": "保岱镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130731105",
                                        "values": "130731105000",
                                        "label": "矾山镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130731106",
                                        "values": "130731106000",
                                        "label": "大堡镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130731107",
                                        "values": "130731107000",
                                        "label": "河东镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130731108",
                                        "values": "130731108000",
                                        "label": "东小庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130731109",
                                        "values": "130731109000",
                                        "label": "辉耀镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130731110",
                                        "values": "130731110000",
                                        "label": "大河南镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130731111",
                                        "values": "130731111000",
                                        "label": "温泉屯镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130731112",
                                        "values": "130731112000",
                                        "label": "蟒石口镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130731201",
                                        "values": "130731201000",
                                        "label": "栾庄乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130731204",
                                        "values": "130731204000",
                                        "label": "黑山寺乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130731205",
                                        "values": "130731205000",
                                        "label": "卧佛寺乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130731206",
                                        "values": "130731206000",
                                        "label": "谢家堡乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130732",
                                "values": "130732000000",
                                "label": "赤城县",
                                "children": [
                                    {
                                        "value": "130732100",
                                        "values": "130732100000",
                                        "label": "赤城镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130732101",
                                        "values": "130732101000",
                                        "label": "田家窑镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130732102",
                                        "values": "130732102000",
                                        "label": "龙关镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130732103",
                                        "values": "130732103000",
                                        "label": "雕鹗镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130732104",
                                        "values": "130732104000",
                                        "label": "独石口镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130732105",
                                        "values": "130732105000",
                                        "label": "白草镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130732106",
                                        "values": "130732106000",
                                        "label": "龙门所镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130732107",
                                        "values": "130732107000",
                                        "label": "后城镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130732108",
                                        "values": "130732108000",
                                        "label": "东卯镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130732200",
                                        "values": "130732200000",
                                        "label": "炮梁乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130732201",
                                        "values": "130732201000",
                                        "label": "大海陀乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130732202",
                                        "values": "130732202000",
                                        "label": "镇宁堡乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130732203",
                                        "values": "130732203000",
                                        "label": "马营乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130732204",
                                        "values": "130732204000",
                                        "label": "云州乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130732205",
                                        "values": "130732205000",
                                        "label": "三道川乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130732206",
                                        "values": "130732206000",
                                        "label": "东万口乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130732207",
                                        "values": "130732207000",
                                        "label": "茨营子乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130732208",
                                        "values": "130732208000",
                                        "label": "样田乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130771",
                                "values": "130771000000",
                                "label": "张家口经济开发区",
                                "children": [
                                    {
                                        "value": "130771001",
                                        "values": "130771001000",
                                        "label": "南站街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130771002",
                                        "values": "130771002000",
                                        "label": "马路东街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130771100",
                                        "values": "130771100000",
                                        "label": "老鸦庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130771101",
                                        "values": "130771101000",
                                        "label": "沈家屯镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130771102",
                                        "values": "130771102000",
                                        "label": "姚家房镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130771103",
                                        "values": "130771103000",
                                        "label": "沙岭子镇",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130772",
                                "values": "130772000000",
                                "label": "张家口市察北管理区",
                                "children": [
                                    {
                                        "value": "130772105",
                                        "values": "130772105000",
                                        "label": "沙沟镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130772215",
                                        "values": "130772215000",
                                        "label": "宇宙营乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130772401",
                                        "values": "130772401000",
                                        "label": "察北管理区黄山管理处",
                                        "children": []
                                    },
                                    {
                                        "value": "130772402",
                                        "values": "130772402000",
                                        "label": "察北管理区石门管理处",
                                        "children": []
                                    },
                                    {
                                        "value": "130772403",
                                        "values": "130772403000",
                                        "label": "察北管理区乌兰管理处",
                                        "children": []
                                    },
                                    {
                                        "value": "130772404",
                                        "values": "130772404000",
                                        "label": "察北管理区金沙管理处",
                                        "children": []
                                    },
                                    {
                                        "value": "130772405",
                                        "values": "130772405000",
                                        "label": "察北管理区白塔管理处",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130773",
                                "values": "130773000000",
                                "label": "张家口市塞北管理区",
                                "children": [
                                    {
                                        "value": "130773501",
                                        "values": "130773501000",
                                        "label": "榆树沟管理处",
                                        "children": []
                                    },
                                    {
                                        "value": "130773502",
                                        "values": "130773502000",
                                        "label": "沙梁子管理处",
                                        "children": []
                                    },
                                    {
                                        "value": "130773503",
                                        "values": "130773503000",
                                        "label": "小城子管理处",
                                        "children": []
                                    },
                                    {
                                        "value": "130773504",
                                        "values": "130773504000",
                                        "label": "东大门管理处",
                                        "children": []
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "value": "1308",
                        "values": "130800000000",
                        "label": "承德市",
                        "children": [
                            {
                                "value": "130801",
                                "values": "130801000000",
                                "label": "市辖区",
                                "children": []
                            },
                            {
                                "value": "130802",
                                "values": "130802000000",
                                "label": "双桥区",
                                "children": [
                                    {
                                        "value": "130802001",
                                        "values": "130802001000",
                                        "label": "西大街街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130802002",
                                        "values": "130802002000",
                                        "label": "头道牌楼街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130802003",
                                        "values": "130802003000",
                                        "label": "潘家沟街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130802004",
                                        "values": "130802004000",
                                        "label": "中华路街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130802005",
                                        "values": "130802005000",
                                        "label": "新华路街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130802006",
                                        "values": "130802006000",
                                        "label": "石洞子沟街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130802007",
                                        "values": "130802007000",
                                        "label": "桥东街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130802100",
                                        "values": "130802100000",
                                        "label": "水泉沟镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130802101",
                                        "values": "130802101000",
                                        "label": "狮子沟镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130802102",
                                        "values": "130802102000",
                                        "label": "牛圈子沟镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130802103",
                                        "values": "130802103000",
                                        "label": "大石庙镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130802105",
                                        "values": "130802105000",
                                        "label": "双峰寺镇",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130803",
                                "values": "130803000000",
                                "label": "双滦区",
                                "children": [
                                    {
                                        "value": "130803001",
                                        "values": "130803001000",
                                        "label": "元宝山街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130803002",
                                        "values": "130803002000",
                                        "label": "钢城街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130803003",
                                        "values": "130803003000",
                                        "label": "秀水街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130803100",
                                        "values": "130803100000",
                                        "label": "双塔山镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130803101",
                                        "values": "130803101000",
                                        "label": "滦河镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130803102",
                                        "values": "130803102000",
                                        "label": "大庙镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130803103",
                                        "values": "130803103000",
                                        "label": "偏桥子镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130803104",
                                        "values": "130803104000",
                                        "label": "西地镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130803200",
                                        "values": "130803200000",
                                        "label": "陈栅子乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130804",
                                "values": "130804000000",
                                "label": "鹰手营子矿区",
                                "children": [
                                    {
                                        "value": "130804001",
                                        "values": "130804001000",
                                        "label": "鹰手营子矿区铁北路街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130804100",
                                        "values": "130804100000",
                                        "label": "鹰手营子镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130804101",
                                        "values": "130804101000",
                                        "label": "北马圈子镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130804102",
                                        "values": "130804102000",
                                        "label": "寿王坟镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130804103",
                                        "values": "130804103000",
                                        "label": "汪家庄镇",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130821",
                                "values": "130821000000",
                                "label": "承德县",
                                "children": [
                                    {
                                        "value": "130821100",
                                        "values": "130821100000",
                                        "label": "下板城镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130821102",
                                        "values": "130821102000",
                                        "label": "甲山镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130821103",
                                        "values": "130821103000",
                                        "label": "六沟镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130821104",
                                        "values": "130821104000",
                                        "label": "三沟镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130821105",
                                        "values": "130821105000",
                                        "label": "头沟镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130821106",
                                        "values": "130821106000",
                                        "label": "高寺台镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130821107",
                                        "values": "130821107000",
                                        "label": "鞍匠镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130821108",
                                        "values": "130821108000",
                                        "label": "三家镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130821109",
                                        "values": "130821109000",
                                        "label": "磴上镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130821110",
                                        "values": "130821110000",
                                        "label": "上谷镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130821111",
                                        "values": "130821111000",
                                        "label": "新杖子镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130821112",
                                        "values": "130821112000",
                                        "label": "石灰窑镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130821200",
                                        "values": "130821200000",
                                        "label": "东小白旗乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130821202",
                                        "values": "130821202000",
                                        "label": "刘杖子乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130821204",
                                        "values": "130821204000",
                                        "label": "孟家院乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130821205",
                                        "values": "130821205000",
                                        "label": "大营子乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130821206",
                                        "values": "130821206000",
                                        "label": "八家乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130821208",
                                        "values": "130821208000",
                                        "label": "满杖子乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130821210",
                                        "values": "130821210000",
                                        "label": "五道河乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130821211",
                                        "values": "130821211000",
                                        "label": "岔沟乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130821212",
                                        "values": "130821212000",
                                        "label": "岗子满族乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130821214",
                                        "values": "130821214000",
                                        "label": "两家满族乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130821216",
                                        "values": "130821216000",
                                        "label": "仓子乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130822",
                                "values": "130822000000",
                                "label": "兴隆县",
                                "children": [
                                    {
                                        "value": "130822100",
                                        "values": "130822100000",
                                        "label": "兴隆镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130822101",
                                        "values": "130822101000",
                                        "label": "半壁山镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130822102",
                                        "values": "130822102000",
                                        "label": "挂兰峪镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130822103",
                                        "values": "130822103000",
                                        "label": "青松岭镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130822104",
                                        "values": "130822104000",
                                        "label": "六道河镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130822105",
                                        "values": "130822105000",
                                        "label": "平安堡镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130822106",
                                        "values": "130822106000",
                                        "label": "北营房镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130822107",
                                        "values": "130822107000",
                                        "label": "孤山子镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130822108",
                                        "values": "130822108000",
                                        "label": "蓝旗营镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130822109",
                                        "values": "130822109000",
                                        "label": "雾灵山镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130822110",
                                        "values": "130822110000",
                                        "label": "李家营镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130822111",
                                        "values": "130822111000",
                                        "label": "大杖子镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130822112",
                                        "values": "130822112000",
                                        "label": "三道河镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130822113",
                                        "values": "130822113000",
                                        "label": "蘑菇峪镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130822114",
                                        "values": "130822114000",
                                        "label": "大水泉镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130822200",
                                        "values": "130822200000",
                                        "label": "南天门满族乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130822202",
                                        "values": "130822202000",
                                        "label": "八卦岭满族乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130822203",
                                        "values": "130822203000",
                                        "label": "陡子峪乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130822204",
                                        "values": "130822204000",
                                        "label": "上石洞乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130822211",
                                        "values": "130822211000",
                                        "label": "安子岭乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130824",
                                "values": "130824000000",
                                "label": "滦平县",
                                "children": [
                                    {
                                        "value": "130824001",
                                        "values": "130824001000",
                                        "label": "中兴路街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130824100",
                                        "values": "130824100000",
                                        "label": "滦平镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130824101",
                                        "values": "130824101000",
                                        "label": "长山峪镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130824102",
                                        "values": "130824102000",
                                        "label": "红旗镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130824103",
                                        "values": "130824103000",
                                        "label": "金沟屯镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130824104",
                                        "values": "130824104000",
                                        "label": "虎什哈镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130824105",
                                        "values": "130824105000",
                                        "label": "巴克什营镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130824106",
                                        "values": "130824106000",
                                        "label": "张百湾镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130824107",
                                        "values": "130824107000",
                                        "label": "付营子镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130824108",
                                        "values": "130824108000",
                                        "label": "大屯镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130824109",
                                        "values": "130824109000",
                                        "label": "火斗山镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130824200",
                                        "values": "130824200000",
                                        "label": "平坊满族乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130824201",
                                        "values": "130824201000",
                                        "label": "安纯沟门满族乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130824205",
                                        "values": "130824205000",
                                        "label": "小营满族乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130824206",
                                        "values": "130824206000",
                                        "label": "西沟满族乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130824207",
                                        "values": "130824207000",
                                        "label": "邓厂满族乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130824208",
                                        "values": "130824208000",
                                        "label": "五道营子满族乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130824209",
                                        "values": "130824209000",
                                        "label": "马营子满族乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130824210",
                                        "values": "130824210000",
                                        "label": "付家店满族乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130824212",
                                        "values": "130824212000",
                                        "label": "两间房乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130824213",
                                        "values": "130824213000",
                                        "label": "涝洼乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130825",
                                "values": "130825000000",
                                "label": "隆化县",
                                "children": [
                                    {
                                        "value": "130825001",
                                        "values": "130825001000",
                                        "label": "安州街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130825100",
                                        "values": "130825100000",
                                        "label": "隆化镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130825101",
                                        "values": "130825101000",
                                        "label": "韩麻营镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130825102",
                                        "values": "130825102000",
                                        "label": "中关镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130825103",
                                        "values": "130825103000",
                                        "label": "七家镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130825104",
                                        "values": "130825104000",
                                        "label": "汤头沟镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130825105",
                                        "values": "130825105000",
                                        "label": "张三营镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130825106",
                                        "values": "130825106000",
                                        "label": "唐三营镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130825107",
                                        "values": "130825107000",
                                        "label": "蓝旗镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130825108",
                                        "values": "130825108000",
                                        "label": "步古沟镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130825109",
                                        "values": "130825109000",
                                        "label": "郭家屯镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130825200",
                                        "values": "130825200000",
                                        "label": "荒地乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130825201",
                                        "values": "130825201000",
                                        "label": "章吉营乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130825202",
                                        "values": "130825202000",
                                        "label": "茅荆坝乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130825203",
                                        "values": "130825203000",
                                        "label": "尹家营满族乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130825204",
                                        "values": "130825204000",
                                        "label": "庙子沟蒙古族满族乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130825205",
                                        "values": "130825205000",
                                        "label": "偏坡营满族乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130825206",
                                        "values": "130825206000",
                                        "label": "山湾乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130825207",
                                        "values": "130825207000",
                                        "label": "八达营蒙古族乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130825208",
                                        "values": "130825208000",
                                        "label": "太平庄满族乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130825209",
                                        "values": "130825209000",
                                        "label": "旧屯满族乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130825210",
                                        "values": "130825210000",
                                        "label": "西阿超满族蒙古族乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130825211",
                                        "values": "130825211000",
                                        "label": "白虎沟满族蒙古族乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130825212",
                                        "values": "130825212000",
                                        "label": "碱房乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130825213",
                                        "values": "130825213000",
                                        "label": "韩家店乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130825214",
                                        "values": "130825214000",
                                        "label": "湾沟门乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130826",
                                "values": "130826000000",
                                "label": "丰宁满族自治县",
                                "children": [
                                    {
                                        "value": "130826001",
                                        "values": "130826001000",
                                        "label": "新丰路街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130826100",
                                        "values": "130826100000",
                                        "label": "大阁镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130826101",
                                        "values": "130826101000",
                                        "label": "大滩镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130826102",
                                        "values": "130826102000",
                                        "label": "鱼儿山镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130826103",
                                        "values": "130826103000",
                                        "label": "土城镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130826104",
                                        "values": "130826104000",
                                        "label": "黄旗镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130826105",
                                        "values": "130826105000",
                                        "label": "凤山镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130826106",
                                        "values": "130826106000",
                                        "label": "波罗诺镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130826107",
                                        "values": "130826107000",
                                        "label": "黑山咀镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130826108",
                                        "values": "130826108000",
                                        "label": "天桥镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130826109",
                                        "values": "130826109000",
                                        "label": "胡麻营镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130826200",
                                        "values": "130826200000",
                                        "label": "万胜永乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130826201",
                                        "values": "130826201000",
                                        "label": "四岔口乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130826202",
                                        "values": "130826202000",
                                        "label": "苏家店乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130826203",
                                        "values": "130826203000",
                                        "label": "外沟门乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130826204",
                                        "values": "130826204000",
                                        "label": "草原乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130826205",
                                        "values": "130826205000",
                                        "label": "窟窿山乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130826206",
                                        "values": "130826206000",
                                        "label": "小坝子乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130826207",
                                        "values": "130826207000",
                                        "label": "五道营乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130826208",
                                        "values": "130826208000",
                                        "label": "南关蒙古族乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130826209",
                                        "values": "130826209000",
                                        "label": "选将营乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130826210",
                                        "values": "130826210000",
                                        "label": "西官营乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130826211",
                                        "values": "130826211000",
                                        "label": "王营乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130826212",
                                        "values": "130826212000",
                                        "label": "北头营乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130826214",
                                        "values": "130826214000",
                                        "label": "石人沟乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130826215",
                                        "values": "130826215000",
                                        "label": "汤河乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130826216",
                                        "values": "130826216000",
                                        "label": "杨木栅子乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130827",
                                "values": "130827000000",
                                "label": "宽城满族自治县",
                                "children": [
                                    {
                                        "value": "130827100",
                                        "values": "130827100000",
                                        "label": "宽城镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130827101",
                                        "values": "130827101000",
                                        "label": "龙须门镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130827102",
                                        "values": "130827102000",
                                        "label": "峪耳崖镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130827103",
                                        "values": "130827103000",
                                        "label": "板城镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130827104",
                                        "values": "130827104000",
                                        "label": "汤道河镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130827105",
                                        "values": "130827105000",
                                        "label": "桲罗台镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130827106",
                                        "values": "130827106000",
                                        "label": "碾子峪镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130827107",
                                        "values": "130827107000",
                                        "label": "亮甲台镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130827108",
                                        "values": "130827108000",
                                        "label": "化皮溜子镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130827109",
                                        "values": "130827109000",
                                        "label": "松岭镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130827201",
                                        "values": "130827201000",
                                        "label": "塌山乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130827203",
                                        "values": "130827203000",
                                        "label": "孟子岭乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130827204",
                                        "values": "130827204000",
                                        "label": "独石沟乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130827207",
                                        "values": "130827207000",
                                        "label": "铧尖乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130827208",
                                        "values": "130827208000",
                                        "label": "东黄花川乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130827210",
                                        "values": "130827210000",
                                        "label": "苇子沟乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130827211",
                                        "values": "130827211000",
                                        "label": "大字沟乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130827212",
                                        "values": "130827212000",
                                        "label": "大石柱子乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130828",
                                "values": "130828000000",
                                "label": "围场满族蒙古族自治县",
                                "children": [
                                    {
                                        "value": "130828100",
                                        "values": "130828100000",
                                        "label": "围场镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130828101",
                                        "values": "130828101000",
                                        "label": "四合永镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130828102",
                                        "values": "130828102000",
                                        "label": "克勒沟镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130828103",
                                        "values": "130828103000",
                                        "label": "棋盘山镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130828104",
                                        "values": "130828104000",
                                        "label": "半截塔镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130828105",
                                        "values": "130828105000",
                                        "label": "朝阳地镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130828106",
                                        "values": "130828106000",
                                        "label": "朝阳湾镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130828107",
                                        "values": "130828107000",
                                        "label": "腰站镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130828108",
                                        "values": "130828108000",
                                        "label": "龙头山镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130828109",
                                        "values": "130828109000",
                                        "label": "新拨镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130828110",
                                        "values": "130828110000",
                                        "label": "御道口镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130828111",
                                        "values": "130828111000",
                                        "label": "城子镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130828200",
                                        "values": "130828200000",
                                        "label": "道坝子乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130828203",
                                        "values": "130828203000",
                                        "label": "黄土坎乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130828204",
                                        "values": "130828204000",
                                        "label": "四道沟乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130828205",
                                        "values": "130828205000",
                                        "label": "兰旗卡伦乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130828206",
                                        "values": "130828206000",
                                        "label": "银窝沟乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130828207",
                                        "values": "130828207000",
                                        "label": "新地乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130828208",
                                        "values": "130828208000",
                                        "label": "广发永乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130828209",
                                        "values": "130828209000",
                                        "label": "育太和乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130828210",
                                        "values": "130828210000",
                                        "label": "郭家湾乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130828211",
                                        "values": "130828211000",
                                        "label": "杨家湾乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130828212",
                                        "values": "130828212000",
                                        "label": "大唤起乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130828213",
                                        "values": "130828213000",
                                        "label": "哈里哈乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130828215",
                                        "values": "130828215000",
                                        "label": "张家湾乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130828216",
                                        "values": "130828216000",
                                        "label": "宝元栈乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130828217",
                                        "values": "130828217000",
                                        "label": "山湾子乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130828218",
                                        "values": "130828218000",
                                        "label": "三义永乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130828219",
                                        "values": "130828219000",
                                        "label": "姜家店乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130828220",
                                        "values": "130828220000",
                                        "label": "下伙房乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130828221",
                                        "values": "130828221000",
                                        "label": "燕格柏乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130828222",
                                        "values": "130828222000",
                                        "label": "牌楼乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130828224",
                                        "values": "130828224000",
                                        "label": "老窝铺乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130828226",
                                        "values": "130828226000",
                                        "label": "石桌子乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130828227",
                                        "values": "130828227000",
                                        "label": "大头山乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130828228",
                                        "values": "130828228000",
                                        "label": "南山嘴乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130828229",
                                        "values": "130828229000",
                                        "label": "西龙头乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130828450",
                                        "values": "130828450000",
                                        "label": "塞罕坝机械林场",
                                        "children": []
                                    },
                                    {
                                        "value": "130828500",
                                        "values": "130828500000",
                                        "label": "国营御道口牧场",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130871",
                                "values": "130871000000",
                                "label": "承德高新技术产业开发区",
                                "children": [
                                    {
                                        "value": "130871104",
                                        "values": "130871104000",
                                        "label": "冯营子镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130871106",
                                        "values": "130871106000",
                                        "label": "上板城镇",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130881",
                                "values": "130881000000",
                                "label": "平泉市",
                                "children": [
                                    {
                                        "value": "130881100",
                                        "values": "130881100000",
                                        "label": "平泉镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130881101",
                                        "values": "130881101000",
                                        "label": "黄土梁子镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130881102",
                                        "values": "130881102000",
                                        "label": "榆树林子镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130881103",
                                        "values": "130881103000",
                                        "label": "杨树岭镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130881104",
                                        "values": "130881104000",
                                        "label": "七沟镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130881105",
                                        "values": "130881105000",
                                        "label": "小寺沟镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130881106",
                                        "values": "130881106000",
                                        "label": "党坝镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130881107",
                                        "values": "130881107000",
                                        "label": "卧龙镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130881108",
                                        "values": "130881108000",
                                        "label": "南五十家子镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130881109",
                                        "values": "130881109000",
                                        "label": "北五十家子镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130881110",
                                        "values": "130881110000",
                                        "label": "桲椤树镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130881111",
                                        "values": "130881111000",
                                        "label": "柳溪镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130881112",
                                        "values": "130881112000",
                                        "label": "平北镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130881113",
                                        "values": "130881113000",
                                        "label": "青河镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130881114",
                                        "values": "130881114000",
                                        "label": "台头山镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130881200",
                                        "values": "130881200000",
                                        "label": "王土房乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130881203",
                                        "values": "130881203000",
                                        "label": "七家岱满族乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130881206",
                                        "values": "130881206000",
                                        "label": "茅兰沟满族蒙古族乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130881210",
                                        "values": "130881210000",
                                        "label": "道虎沟乡",
                                        "children": []
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "value": "1309",
                        "values": "130900000000",
                        "label": "沧州市",
                        "children": [
                            {
                                "value": "130901",
                                "values": "130901000000",
                                "label": "市辖区",
                                "children": []
                            },
                            {
                                "value": "130902",
                                "values": "130902000000",
                                "label": "新华区",
                                "children": [
                                    {
                                        "value": "130902001",
                                        "values": "130902001000",
                                        "label": "建设北街街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130902002",
                                        "values": "130902002000",
                                        "label": "车站街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130902003",
                                        "values": "130902003000",
                                        "label": "南大街街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130902004",
                                        "values": "130902004000",
                                        "label": "东环街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130902005",
                                        "values": "130902005000",
                                        "label": "道东街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130902200",
                                        "values": "130902200000",
                                        "label": "小赵庄乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130903",
                                "values": "130903000000",
                                "label": "运河区",
                                "children": [
                                    {
                                        "value": "130903001",
                                        "values": "130903001000",
                                        "label": "水月寺街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130903002",
                                        "values": "130903002000",
                                        "label": "南环中路街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130903003",
                                        "values": "130903003000",
                                        "label": "南湖街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130903004",
                                        "values": "130903004000",
                                        "label": "市场街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130903005",
                                        "values": "130903005000",
                                        "label": "西环中街街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130903006",
                                        "values": "130903006000",
                                        "label": "公园街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130903100",
                                        "values": "130903100000",
                                        "label": "小王庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130903200",
                                        "values": "130903200000",
                                        "label": "南陈屯乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130921",
                                "values": "130921000000",
                                "label": "沧县",
                                "children": [
                                    {
                                        "value": "130921100",
                                        "values": "130921100000",
                                        "label": "旧州镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130921101",
                                        "values": "130921101000",
                                        "label": "兴济镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130921102",
                                        "values": "130921102000",
                                        "label": "杜生镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130921103",
                                        "values": "130921103000",
                                        "label": "崔尔庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130921200",
                                        "values": "130921200000",
                                        "label": "薛官屯乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130921201",
                                        "values": "130921201000",
                                        "label": "捷地回族乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130921202",
                                        "values": "130921202000",
                                        "label": "张官屯乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130921203",
                                        "values": "130921203000",
                                        "label": "李天木回族乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130921204",
                                        "values": "130921204000",
                                        "label": "风化店乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130921205",
                                        "values": "130921205000",
                                        "label": "姚官屯乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130921206",
                                        "values": "130921206000",
                                        "label": "杜林回族乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130921207",
                                        "values": "130921207000",
                                        "label": "汪家铺乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130921208",
                                        "values": "130921208000",
                                        "label": "刘家庙乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130921209",
                                        "values": "130921209000",
                                        "label": "仵龙堂乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130921210",
                                        "values": "130921210000",
                                        "label": "大官厅乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130921211",
                                        "values": "130921211000",
                                        "label": "高川乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130921212",
                                        "values": "130921212000",
                                        "label": "黄递铺乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130921213",
                                        "values": "130921213000",
                                        "label": "大褚村回族乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130921214",
                                        "values": "130921214000",
                                        "label": "纸房头乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130922",
                                "values": "130922000000",
                                "label": "青县",
                                "children": [
                                    {
                                        "value": "130922100",
                                        "values": "130922100000",
                                        "label": "清州镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130922101",
                                        "values": "130922101000",
                                        "label": "金牛镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130922102",
                                        "values": "130922102000",
                                        "label": "新兴镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130922103",
                                        "values": "130922103000",
                                        "label": "流河镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130922104",
                                        "values": "130922104000",
                                        "label": "木门店镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130922105",
                                        "values": "130922105000",
                                        "label": "马厂镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130922106",
                                        "values": "130922106000",
                                        "label": "盘古镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130922200",
                                        "values": "130922200000",
                                        "label": "上伍乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130922201",
                                        "values": "130922201000",
                                        "label": "曹寺乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130922203",
                                        "values": "130922203000",
                                        "label": "陈嘴乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130922400",
                                        "values": "130922400000",
                                        "label": "青县农场",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130923",
                                "values": "130923000000",
                                "label": "东光县",
                                "children": [
                                    {
                                        "value": "130923100",
                                        "values": "130923100000",
                                        "label": "东光镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130923101",
                                        "values": "130923101000",
                                        "label": "连镇镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130923102",
                                        "values": "130923102000",
                                        "label": "找王镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130923103",
                                        "values": "130923103000",
                                        "label": "秦村镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130923104",
                                        "values": "130923104000",
                                        "label": "灯明寺镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130923105",
                                        "values": "130923105000",
                                        "label": "南霞口镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130923106",
                                        "values": "130923106000",
                                        "label": "大单镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130923107",
                                        "values": "130923107000",
                                        "label": "龙王李镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130923201",
                                        "values": "130923201000",
                                        "label": "于桥乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130924",
                                "values": "130924000000",
                                "label": "海兴县",
                                "children": [
                                    {
                                        "value": "130924100",
                                        "values": "130924100000",
                                        "label": "苏基镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130924101",
                                        "values": "130924101000",
                                        "label": "辛集镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130924102",
                                        "values": "130924102000",
                                        "label": "高湾镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130924200",
                                        "values": "130924200000",
                                        "label": "赵毛陶乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130924201",
                                        "values": "130924201000",
                                        "label": "香坊乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130924202",
                                        "values": "130924202000",
                                        "label": "小山乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130924203",
                                        "values": "130924203000",
                                        "label": "张会亭乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130924400",
                                        "values": "130924400000",
                                        "label": "海兴县农场",
                                        "children": []
                                    },
                                    {
                                        "value": "130924401",
                                        "values": "130924401000",
                                        "label": "青先农场",
                                        "children": []
                                    },
                                    {
                                        "value": "130924402",
                                        "values": "130924402000",
                                        "label": "青锋农场",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130925",
                                "values": "130925000000",
                                "label": "盐山县",
                                "children": [
                                    {
                                        "value": "130925100",
                                        "values": "130925100000",
                                        "label": "盐山镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130925101",
                                        "values": "130925101000",
                                        "label": "望树镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130925102",
                                        "values": "130925102000",
                                        "label": "庆云镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130925103",
                                        "values": "130925103000",
                                        "label": "韩集镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130925104",
                                        "values": "130925104000",
                                        "label": "千童镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130925105",
                                        "values": "130925105000",
                                        "label": "圣佛镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130925200",
                                        "values": "130925200000",
                                        "label": "边务乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130925201",
                                        "values": "130925201000",
                                        "label": "小营乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130925202",
                                        "values": "130925202000",
                                        "label": "杨集乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130925203",
                                        "values": "130925203000",
                                        "label": "孟店乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130925204",
                                        "values": "130925204000",
                                        "label": "常庄乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130925205",
                                        "values": "130925205000",
                                        "label": "小庄乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130926",
                                "values": "130926000000",
                                "label": "肃宁县",
                                "children": [
                                    {
                                        "value": "130926100",
                                        "values": "130926100000",
                                        "label": "肃宁镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130926101",
                                        "values": "130926101000",
                                        "label": "梁家村镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130926102",
                                        "values": "130926102000",
                                        "label": "窝北镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130926103",
                                        "values": "130926103000",
                                        "label": "尚村镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130926104",
                                        "values": "130926104000",
                                        "label": "万里镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130926105",
                                        "values": "130926105000",
                                        "label": "师素镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130926106",
                                        "values": "130926106000",
                                        "label": "河北留善寺镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130926204",
                                        "values": "130926204000",
                                        "label": "付家佐乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130926205",
                                        "values": "130926205000",
                                        "label": "邵庄乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130927",
                                "values": "130927000000",
                                "label": "南皮县",
                                "children": [
                                    {
                                        "value": "130927100",
                                        "values": "130927100000",
                                        "label": "南皮镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130927101",
                                        "values": "130927101000",
                                        "label": "冯家口镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130927102",
                                        "values": "130927102000",
                                        "label": "寨子镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130927103",
                                        "values": "130927103000",
                                        "label": "鲍官屯镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130927104",
                                        "values": "130927104000",
                                        "label": "王寺镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130927105",
                                        "values": "130927105000",
                                        "label": "乌马营镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130927200",
                                        "values": "130927200000",
                                        "label": "大浪淀乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130927201",
                                        "values": "130927201000",
                                        "label": "刘八里乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130927202",
                                        "values": "130927202000",
                                        "label": "潞灌乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130928",
                                "values": "130928000000",
                                "label": "吴桥县",
                                "children": [
                                    {
                                        "value": "130928100",
                                        "values": "130928100000",
                                        "label": "桑园镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130928101",
                                        "values": "130928101000",
                                        "label": "铁城镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130928102",
                                        "values": "130928102000",
                                        "label": "于集镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130928103",
                                        "values": "130928103000",
                                        "label": "梁集镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130928104",
                                        "values": "130928104000",
                                        "label": "安陵镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130928200",
                                        "values": "130928200000",
                                        "label": "曹家洼乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130928201",
                                        "values": "130928201000",
                                        "label": "宋门乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130928202",
                                        "values": "130928202000",
                                        "label": "杨家寺乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130928203",
                                        "values": "130928203000",
                                        "label": "沟店铺乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130928204",
                                        "values": "130928204000",
                                        "label": "何庄乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130928400",
                                        "values": "130928400000",
                                        "label": "吴桥经济技术开发区",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130929",
                                "values": "130929000000",
                                "label": "献县",
                                "children": [
                                    {
                                        "value": "130929100",
                                        "values": "130929100000",
                                        "label": "乐寿镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130929101",
                                        "values": "130929101000",
                                        "label": "淮镇镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130929102",
                                        "values": "130929102000",
                                        "label": "郭庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130929103",
                                        "values": "130929103000",
                                        "label": "河城街镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130929104",
                                        "values": "130929104000",
                                        "label": "韩村镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130929105",
                                        "values": "130929105000",
                                        "label": "陌南镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130929106",
                                        "values": "130929106000",
                                        "label": "陈庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130929107",
                                        "values": "130929107000",
                                        "label": "段村镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130929108",
                                        "values": "130929108000",
                                        "label": "高官镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130929206",
                                        "values": "130929206000",
                                        "label": "商林乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130929209",
                                        "values": "130929209000",
                                        "label": "张村乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130929210",
                                        "values": "130929210000",
                                        "label": "临河乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130929211",
                                        "values": "130929211000",
                                        "label": "小平王乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130929212",
                                        "values": "130929212000",
                                        "label": "十五级乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130929213",
                                        "values": "130929213000",
                                        "label": "垒头乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130929214",
                                        "values": "130929214000",
                                        "label": "南河头乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130929215",
                                        "values": "130929215000",
                                        "label": "西城乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130929216",
                                        "values": "130929216000",
                                        "label": "本斋回族乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130929400",
                                        "values": "130929400000",
                                        "label": "农场",
                                        "children": []
                                    },
                                    {
                                        "value": "130929401",
                                        "values": "130929401000",
                                        "label": "献县经济开发区",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130930",
                                "values": "130930000000",
                                "label": "孟村回族自治县",
                                "children": [
                                    {
                                        "value": "130930100",
                                        "values": "130930100000",
                                        "label": "孟村镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130930101",
                                        "values": "130930101000",
                                        "label": "新县镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130930102",
                                        "values": "130930102000",
                                        "label": "辛店镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130930103",
                                        "values": "130930103000",
                                        "label": "高寨镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130930200",
                                        "values": "130930200000",
                                        "label": "宋庄子乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130930201",
                                        "values": "130930201000",
                                        "label": "牛进庄乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130971",
                                "values": "130971000000",
                                "label": "河北沧州经济开发区",
                                "children": [
                                    {
                                        "value": "130971400",
                                        "values": "130971400000",
                                        "label": "开发区乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130972",
                                "values": "130972000000",
                                "label": "沧州高新技术产业开发区",
                                "children": [
                                    {
                                        "value": "130972400",
                                        "values": "130972400000",
                                        "label": "高新乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130973",
                                "values": "130973000000",
                                "label": "沧州渤海新区",
                                "children": [
                                    {
                                        "value": "130973206",
                                        "values": "130973206000",
                                        "label": "新村回族乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130973400",
                                        "values": "130973400000",
                                        "label": "南大港管理区",
                                        "children": []
                                    },
                                    {
                                        "value": "130973401",
                                        "values": "130973401000",
                                        "label": "国营中捷农场",
                                        "children": []
                                    },
                                    {
                                        "value": "130973402",
                                        "values": "130973402000",
                                        "label": "港城街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130973403",
                                        "values": "130973403000",
                                        "label": "临港化工园区",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130981",
                                "values": "130981000000",
                                "label": "泊头市",
                                "children": [
                                    {
                                        "value": "130981001",
                                        "values": "130981001000",
                                        "label": "解放街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130981002",
                                        "values": "130981002000",
                                        "label": "河东街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130981003",
                                        "values": "130981003000",
                                        "label": "鼓楼街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130981100",
                                        "values": "130981100000",
                                        "label": "泊镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130981101",
                                        "values": "130981101000",
                                        "label": "交河镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130981102",
                                        "values": "130981102000",
                                        "label": "齐桥镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130981103",
                                        "values": "130981103000",
                                        "label": "寺门村镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130981104",
                                        "values": "130981104000",
                                        "label": "郝村镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130981105",
                                        "values": "130981105000",
                                        "label": "富镇镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130981106",
                                        "values": "130981106000",
                                        "label": "文庙镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130981107",
                                        "values": "130981107000",
                                        "label": "洼里王镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130981201",
                                        "values": "130981201000",
                                        "label": "王武庄乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130981202",
                                        "values": "130981202000",
                                        "label": "营子乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130981203",
                                        "values": "130981203000",
                                        "label": "四营乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130981204",
                                        "values": "130981204000",
                                        "label": "西辛店乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130981400",
                                        "values": "130981400000",
                                        "label": "河北泊头经济开发区",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130982",
                                "values": "130982000000",
                                "label": "任丘市",
                                "children": [
                                    {
                                        "value": "130982001",
                                        "values": "130982001000",
                                        "label": "新华路街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130982002",
                                        "values": "130982002000",
                                        "label": "西环路街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130982003",
                                        "values": "130982003000",
                                        "label": "永丰路街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130982004",
                                        "values": "130982004000",
                                        "label": "中华路街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130982005",
                                        "values": "130982005000",
                                        "label": "渤海路街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130982006",
                                        "values": "130982006000",
                                        "label": "油建路街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130982007",
                                        "values": "130982007000",
                                        "label": "会战道街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130982100",
                                        "values": "130982100000",
                                        "label": "出岸镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130982101",
                                        "values": "130982101000",
                                        "label": "石门桥镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130982102",
                                        "values": "130982102000",
                                        "label": "吕公堡镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130982103",
                                        "values": "130982103000",
                                        "label": "长丰镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130982106",
                                        "values": "130982106000",
                                        "label": "梁召镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130982107",
                                        "values": "130982107000",
                                        "label": "辛中驿镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130982108",
                                        "values": "130982108000",
                                        "label": "麻家坞镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130982109",
                                        "values": "130982109000",
                                        "label": "北辛庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130982110",
                                        "values": "130982110000",
                                        "label": "议论堡镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130982204",
                                        "values": "130982204000",
                                        "label": "青塔乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130982207",
                                        "values": "130982207000",
                                        "label": "北汉乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130982208",
                                        "values": "130982208000",
                                        "label": "于村乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130982400",
                                        "values": "130982400000",
                                        "label": "华北石油管理局",
                                        "children": []
                                    },
                                    {
                                        "value": "130982500",
                                        "values": "130982500000",
                                        "label": "开发区管理委员会",
                                        "children": []
                                    },
                                    {
                                        "value": "130982501",
                                        "values": "130982501000",
                                        "label": "河北任丘雁翎工业园区",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130983",
                                "values": "130983000000",
                                "label": "黄骅市",
                                "children": [
                                    {
                                        "value": "130983001",
                                        "values": "130983001000",
                                        "label": "骅东街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130983002",
                                        "values": "130983002000",
                                        "label": "骅中街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130983003",
                                        "values": "130983003000",
                                        "label": "骅西街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130983100",
                                        "values": "130983100000",
                                        "label": "黄骅镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130983101",
                                        "values": "130983101000",
                                        "label": "南排河镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130983102",
                                        "values": "130983102000",
                                        "label": "吕桥镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130983103",
                                        "values": "130983103000",
                                        "label": "旧城镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130983104",
                                        "values": "130983104000",
                                        "label": "齐家务镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130983105",
                                        "values": "130983105000",
                                        "label": "滕庄子镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130983200",
                                        "values": "130983200000",
                                        "label": "羊二庄回族乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130983202",
                                        "values": "130983202000",
                                        "label": "常郭乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130983204",
                                        "values": "130983204000",
                                        "label": "官庄乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130983207",
                                        "values": "130983207000",
                                        "label": "羊三木回族乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "130984",
                                "values": "130984000000",
                                "label": "河间市",
                                "children": [
                                    {
                                        "value": "130984001",
                                        "values": "130984001000",
                                        "label": "瀛州路街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130984002",
                                        "values": "130984002000",
                                        "label": "城垣西路街道",
                                        "children": []
                                    },
                                    {
                                        "value": "130984101",
                                        "values": "130984101000",
                                        "label": "米各庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130984102",
                                        "values": "130984102000",
                                        "label": "景和镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130984103",
                                        "values": "130984103000",
                                        "label": "卧佛堂镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130984104",
                                        "values": "130984104000",
                                        "label": "束城镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130984105",
                                        "values": "130984105000",
                                        "label": "留古寺镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130984106",
                                        "values": "130984106000",
                                        "label": "沙河桥镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130984107",
                                        "values": "130984107000",
                                        "label": "诗经村镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130984108",
                                        "values": "130984108000",
                                        "label": "尊祖庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130984109",
                                        "values": "130984109000",
                                        "label": "兴村镇",
                                        "children": []
                                    },
                                    {
                                        "value": "130984200",
                                        "values": "130984200000",
                                        "label": "故仙乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130984201",
                                        "values": "130984201000",
                                        "label": "黎民居乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130984203",
                                        "values": "130984203000",
                                        "label": "沙洼乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130984204",
                                        "values": "130984204000",
                                        "label": "西九吉乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130984205",
                                        "values": "130984205000",
                                        "label": "北石槽乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130984209",
                                        "values": "130984209000",
                                        "label": "时村乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130984210",
                                        "values": "130984210000",
                                        "label": "行别营乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130984212",
                                        "values": "130984212000",
                                        "label": "龙华店乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130984213",
                                        "values": "130984213000",
                                        "label": "果子洼回族乡",
                                        "children": []
                                    },
                                    {
                                        "value": "130984403",
                                        "values": "130984403000",
                                        "label": "河北河间经济开发区",
                                        "children": []
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "value": "1310",
                        "values": "131000000000",
                        "label": "廊坊市",
                        "children": [
                            {
                                "value": "131001",
                                "values": "131001000000",
                                "label": "市辖区",
                                "children": []
                            },
                            {
                                "value": "131002",
                                "values": "131002000000",
                                "label": "安次区",
                                "children": [
                                    {
                                        "value": "131002001",
                                        "values": "131002001000",
                                        "label": "银河南路街道",
                                        "children": []
                                    },
                                    {
                                        "value": "131002002",
                                        "values": "131002002000",
                                        "label": "光明西道街道",
                                        "children": []
                                    },
                                    {
                                        "value": "131002003",
                                        "values": "131002003000",
                                        "label": "永华道街道",
                                        "children": []
                                    },
                                    {
                                        "value": "131002100",
                                        "values": "131002100000",
                                        "label": "落垡镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131002101",
                                        "values": "131002101000",
                                        "label": "码头镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131002102",
                                        "values": "131002102000",
                                        "label": "葛渔城镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131002103",
                                        "values": "131002103000",
                                        "label": "东沽港镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131002104",
                                        "values": "131002104000",
                                        "label": "调河头镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131002200",
                                        "values": "131002200000",
                                        "label": "杨税务乡",
                                        "children": []
                                    },
                                    {
                                        "value": "131002201",
                                        "values": "131002201000",
                                        "label": "仇庄乡",
                                        "children": []
                                    },
                                    {
                                        "value": "131002203",
                                        "values": "131002203000",
                                        "label": "北史家务乡",
                                        "children": []
                                    },
                                    {
                                        "value": "131002400",
                                        "values": "131002400000",
                                        "label": "廊坊龙河高新技术产业开发区",
                                        "children": []
                                    },
                                    {
                                        "value": "131002401",
                                        "values": "131002401000",
                                        "label": "河北廊坊高新技术产业开发区",
                                        "children": []
                                    },
                                    {
                                        "value": "131002402",
                                        "values": "131002402000",
                                        "label": "河北廊坊龙港经济开发区",
                                        "children": []
                                    },
                                    {
                                        "value": "131002403",
                                        "values": "131002403000",
                                        "label": "河北安次经济开发区",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "131003",
                                "values": "131003000000",
                                "label": "广阳区",
                                "children": [
                                    {
                                        "value": "131003001",
                                        "values": "131003001000",
                                        "label": "银河北路街道",
                                        "children": []
                                    },
                                    {
                                        "value": "131003002",
                                        "values": "131003002000",
                                        "label": "爱民东道街道",
                                        "children": []
                                    },
                                    {
                                        "value": "131003003",
                                        "values": "131003003000",
                                        "label": "解放道街道",
                                        "children": []
                                    },
                                    {
                                        "value": "131003004",
                                        "values": "131003004000",
                                        "label": "新开路街道",
                                        "children": []
                                    },
                                    {
                                        "value": "131003005",
                                        "values": "131003005000",
                                        "label": "新源道街道",
                                        "children": []
                                    },
                                    {
                                        "value": "131003100",
                                        "values": "131003100000",
                                        "label": "南尖塔镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131003101",
                                        "values": "131003101000",
                                        "label": "万庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131003102",
                                        "values": "131003102000",
                                        "label": "九州镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131003200",
                                        "values": "131003200000",
                                        "label": "北旺乡",
                                        "children": []
                                    },
                                    {
                                        "value": "131003500",
                                        "values": "131003500000",
                                        "label": "新世纪步行街管理委员会",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "131022",
                                "values": "131022000000",
                                "label": "固安县",
                                "children": [
                                    {
                                        "value": "131022100",
                                        "values": "131022100000",
                                        "label": "固安镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131022101",
                                        "values": "131022101000",
                                        "label": "宫村镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131022102",
                                        "values": "131022102000",
                                        "label": "柳泉镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131022103",
                                        "values": "131022103000",
                                        "label": "牛驼镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131022104",
                                        "values": "131022104000",
                                        "label": "马庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131022200",
                                        "values": "131022200000",
                                        "label": "东湾乡",
                                        "children": []
                                    },
                                    {
                                        "value": "131022201",
                                        "values": "131022201000",
                                        "label": "彭村乡",
                                        "children": []
                                    },
                                    {
                                        "value": "131022202",
                                        "values": "131022202000",
                                        "label": "渠沟乡",
                                        "children": []
                                    },
                                    {
                                        "value": "131022203",
                                        "values": "131022203000",
                                        "label": "礼让店乡",
                                        "children": []
                                    },
                                    {
                                        "value": "131022400",
                                        "values": "131022400000",
                                        "label": "固安温泉休闲商务产业园区",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "131023",
                                "values": "131023000000",
                                "label": "永清县",
                                "children": [
                                    {
                                        "value": "131023001",
                                        "values": "131023001000",
                                        "label": "城区街道",
                                        "children": []
                                    },
                                    {
                                        "value": "131023100",
                                        "values": "131023100000",
                                        "label": "永清镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131023101",
                                        "values": "131023101000",
                                        "label": "韩村镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131023102",
                                        "values": "131023102000",
                                        "label": "后奕镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131023103",
                                        "values": "131023103000",
                                        "label": "别古庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131023104",
                                        "values": "131023104000",
                                        "label": "里澜城镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131023200",
                                        "values": "131023200000",
                                        "label": "管家务回族乡",
                                        "children": []
                                    },
                                    {
                                        "value": "131023201",
                                        "values": "131023201000",
                                        "label": "曹家务乡",
                                        "children": []
                                    },
                                    {
                                        "value": "131023202",
                                        "values": "131023202000",
                                        "label": "龙虎庄乡",
                                        "children": []
                                    },
                                    {
                                        "value": "131023203",
                                        "values": "131023203000",
                                        "label": "刘街乡",
                                        "children": []
                                    },
                                    {
                                        "value": "131023204",
                                        "values": "131023204000",
                                        "label": "三圣口乡",
                                        "children": []
                                    },
                                    {
                                        "value": "131023400",
                                        "values": "131023400000",
                                        "label": "河北永清经济开发区",
                                        "children": []
                                    },
                                    {
                                        "value": "131023401",
                                        "values": "131023401000",
                                        "label": "北京亦庄.永清高新技术产业开发区",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "131024",
                                "values": "131024000000",
                                "label": "香河县",
                                "children": [
                                    {
                                        "value": "131024100",
                                        "values": "131024100000",
                                        "label": "淑阳镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131024101",
                                        "values": "131024101000",
                                        "label": "蒋辛屯镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131024102",
                                        "values": "131024102000",
                                        "label": "渠口镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131024103",
                                        "values": "131024103000",
                                        "label": "安头屯镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131024104",
                                        "values": "131024104000",
                                        "label": "安平镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131024105",
                                        "values": "131024105000",
                                        "label": "刘宋镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131024106",
                                        "values": "131024106000",
                                        "label": "五百户镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131024107",
                                        "values": "131024107000",
                                        "label": "钱旺镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131024108",
                                        "values": "131024108000",
                                        "label": "钳屯镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131024500",
                                        "values": "131024500000",
                                        "label": "香河开发区",
                                        "children": []
                                    },
                                    {
                                        "value": "131024501",
                                        "values": "131024501000",
                                        "label": "香河新兴产业示范区",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "131025",
                                "values": "131025000000",
                                "label": "大城县",
                                "children": [
                                    {
                                        "value": "131025100",
                                        "values": "131025100000",
                                        "label": "平舒镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131025101",
                                        "values": "131025101000",
                                        "label": "旺村镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131025102",
                                        "values": "131025102000",
                                        "label": "大尚屯镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131025103",
                                        "values": "131025103000",
                                        "label": "南赵扶镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131025104",
                                        "values": "131025104000",
                                        "label": "留各庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131025105",
                                        "values": "131025105000",
                                        "label": "权村镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131025106",
                                        "values": "131025106000",
                                        "label": "里坦镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131025107",
                                        "values": "131025107000",
                                        "label": "广安镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131025108",
                                        "values": "131025108000",
                                        "label": "北魏镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131025109",
                                        "values": "131025109000",
                                        "label": "臧屯镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131025400",
                                        "values": "131025400000",
                                        "label": "河北大城经济开发区",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "131026",
                                "values": "131026000000",
                                "label": "文安县",
                                "children": [
                                    {
                                        "value": "131026100",
                                        "values": "131026100000",
                                        "label": "文安镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131026101",
                                        "values": "131026101000",
                                        "label": "新镇镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131026102",
                                        "values": "131026102000",
                                        "label": "苏桥镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131026103",
                                        "values": "131026103000",
                                        "label": "大柳河镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131026104",
                                        "values": "131026104000",
                                        "label": "左各庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131026105",
                                        "values": "131026105000",
                                        "label": "滩里镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131026106",
                                        "values": "131026106000",
                                        "label": "史各庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131026107",
                                        "values": "131026107000",
                                        "label": "赵各庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131026108",
                                        "values": "131026108000",
                                        "label": "兴隆宫镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131026109",
                                        "values": "131026109000",
                                        "label": "大留镇镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131026110",
                                        "values": "131026110000",
                                        "label": "孙氏镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131026111",
                                        "values": "131026111000",
                                        "label": "德归镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131026200",
                                        "values": "131026200000",
                                        "label": "大围河回族满族乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "131028",
                                "values": "131028000000",
                                "label": "大厂回族自治县",
                                "children": [
                                    {
                                        "value": "131028001",
                                        "values": "131028001000",
                                        "label": "北辰街道",
                                        "children": []
                                    },
                                    {
                                        "value": "131028100",
                                        "values": "131028100000",
                                        "label": "大厂镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131028101",
                                        "values": "131028101000",
                                        "label": "夏垫镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131028102",
                                        "values": "131028102000",
                                        "label": "祁各庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131028103",
                                        "values": "131028103000",
                                        "label": "邵府镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131028104",
                                        "values": "131028104000",
                                        "label": "陈府镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131028400",
                                        "values": "131028400000",
                                        "label": "河北大厂高新技术产业开发区",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "131071",
                                "values": "131071000000",
                                "label": "廊坊经济技术开发区",
                                "children": [
                                    {
                                        "value": "131071450",
                                        "values": "131071450000",
                                        "label": "耀华道街道办事处",
                                        "children": []
                                    },
                                    {
                                        "value": "131071451",
                                        "values": "131071451000",
                                        "label": "云鹏道街道办事处",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "131081",
                                "values": "131081000000",
                                "label": "霸州市",
                                "children": [
                                    {
                                        "value": "131081001",
                                        "values": "131081001000",
                                        "label": "裕华街道",
                                        "children": []
                                    },
                                    {
                                        "value": "131081100",
                                        "values": "131081100000",
                                        "label": "霸州镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131081101",
                                        "values": "131081101000",
                                        "label": "南孟镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131081102",
                                        "values": "131081102000",
                                        "label": "信安镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131081103",
                                        "values": "131081103000",
                                        "label": "堂二里镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131081104",
                                        "values": "131081104000",
                                        "label": "煎茶铺镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131081105",
                                        "values": "131081105000",
                                        "label": "胜芳镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131081106",
                                        "values": "131081106000",
                                        "label": "杨芬港镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131081107",
                                        "values": "131081107000",
                                        "label": "康仙庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131081108",
                                        "values": "131081108000",
                                        "label": "王庄子镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131081200",
                                        "values": "131081200000",
                                        "label": "岔河集乡",
                                        "children": []
                                    },
                                    {
                                        "value": "131081202",
                                        "values": "131081202000",
                                        "label": "东杨庄乡",
                                        "children": []
                                    },
                                    {
                                        "value": "131081204",
                                        "values": "131081204000",
                                        "label": "东段乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "131082",
                                "values": "131082000000",
                                "label": "三河市",
                                "children": [
                                    {
                                        "value": "131082001",
                                        "values": "131082001000",
                                        "label": "鼎盛东街道",
                                        "children": []
                                    },
                                    {
                                        "value": "131082002",
                                        "values": "131082002000",
                                        "label": "泃阳西街道",
                                        "children": []
                                    },
                                    {
                                        "value": "131082003",
                                        "values": "131082003000",
                                        "label": "行宫东街道",
                                        "children": []
                                    },
                                    {
                                        "value": "131082004",
                                        "values": "131082004000",
                                        "label": "迎宾北路街道",
                                        "children": []
                                    },
                                    {
                                        "value": "131082005",
                                        "values": "131082005000",
                                        "label": "燕顺路街道",
                                        "children": []
                                    },
                                    {
                                        "value": "131082006",
                                        "values": "131082006000",
                                        "label": "康城街道",
                                        "children": []
                                    },
                                    {
                                        "value": "131082100",
                                        "values": "131082100000",
                                        "label": "泃阳镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131082101",
                                        "values": "131082101000",
                                        "label": "李旗庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131082102",
                                        "values": "131082102000",
                                        "label": "杨庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131082103",
                                        "values": "131082103000",
                                        "label": "皇庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131082104",
                                        "values": "131082104000",
                                        "label": "新集镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131082105",
                                        "values": "131082105000",
                                        "label": "段甲岭镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131082106",
                                        "values": "131082106000",
                                        "label": "黄土庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131082107",
                                        "values": "131082107000",
                                        "label": "高楼镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131082108",
                                        "values": "131082108000",
                                        "label": "齐心庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131082109",
                                        "values": "131082109000",
                                        "label": "燕郊镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131082450",
                                        "values": "131082450000",
                                        "label": "燕郊经济技术开发区",
                                        "children": []
                                    },
                                    {
                                        "value": "131082451",
                                        "values": "131082451000",
                                        "label": "三河市农业高新技术园区",
                                        "children": []
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "value": "1311",
                        "values": "131100000000",
                        "label": "衡水市",
                        "children": [
                            {
                                "value": "131101",
                                "values": "131101000000",
                                "label": "市辖区",
                                "children": []
                            },
                            {
                                "value": "131102",
                                "values": "131102000000",
                                "label": "桃城区",
                                "children": [
                                    {
                                        "value": "131102001",
                                        "values": "131102001000",
                                        "label": "河西街道",
                                        "children": []
                                    },
                                    {
                                        "value": "131102002",
                                        "values": "131102002000",
                                        "label": "河东街道",
                                        "children": []
                                    },
                                    {
                                        "value": "131102003",
                                        "values": "131102003000",
                                        "label": "路北街道",
                                        "children": []
                                    },
                                    {
                                        "value": "131102004",
                                        "values": "131102004000",
                                        "label": "中华大街街道",
                                        "children": []
                                    },
                                    {
                                        "value": "131102100",
                                        "values": "131102100000",
                                        "label": "郑家河沿镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131102101",
                                        "values": "131102101000",
                                        "label": "赵家圈镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131102102",
                                        "values": "131102102000",
                                        "label": "邓庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131102200",
                                        "values": "131102200000",
                                        "label": "何家庄乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "131103",
                                "values": "131103000000",
                                "label": "冀州区",
                                "children": [
                                    {
                                        "value": "131103100",
                                        "values": "131103100000",
                                        "label": "冀州镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131103102",
                                        "values": "131103102000",
                                        "label": "官道李镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131103103",
                                        "values": "131103103000",
                                        "label": "南午村镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131103104",
                                        "values": "131103104000",
                                        "label": "周村镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131103105",
                                        "values": "131103105000",
                                        "label": "码头李镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131103106",
                                        "values": "131103106000",
                                        "label": "西王镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131103200",
                                        "values": "131103200000",
                                        "label": "门家庄乡",
                                        "children": []
                                    },
                                    {
                                        "value": "131103201",
                                        "values": "131103201000",
                                        "label": "徐家庄乡",
                                        "children": []
                                    },
                                    {
                                        "value": "131103202",
                                        "values": "131103202000",
                                        "label": "北漳淮乡",
                                        "children": []
                                    },
                                    {
                                        "value": "131103203",
                                        "values": "131103203000",
                                        "label": "小寨乡",
                                        "children": []
                                    },
                                    {
                                        "value": "131103400",
                                        "values": "131103400000",
                                        "label": "衡水市冀州区社区建设办公室",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "131121",
                                "values": "131121000000",
                                "label": "枣强县",
                                "children": [
                                    {
                                        "value": "131121100",
                                        "values": "131121100000",
                                        "label": "枣强镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131121101",
                                        "values": "131121101000",
                                        "label": "恩察镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131121102",
                                        "values": "131121102000",
                                        "label": "大营镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131121103",
                                        "values": "131121103000",
                                        "label": "嘉会镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131121104",
                                        "values": "131121104000",
                                        "label": "马屯镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131121105",
                                        "values": "131121105000",
                                        "label": "肖张镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131121106",
                                        "values": "131121106000",
                                        "label": "张秀屯镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131121107",
                                        "values": "131121107000",
                                        "label": "新屯镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131121108",
                                        "values": "131121108000",
                                        "label": "唐林镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131121202",
                                        "values": "131121202000",
                                        "label": "王均乡",
                                        "children": []
                                    },
                                    {
                                        "value": "131121204",
                                        "values": "131121204000",
                                        "label": "王常乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "131122",
                                "values": "131122000000",
                                "label": "武邑县",
                                "children": [
                                    {
                                        "value": "131122100",
                                        "values": "131122100000",
                                        "label": "武邑镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131122101",
                                        "values": "131122101000",
                                        "label": "清凉店镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131122102",
                                        "values": "131122102000",
                                        "label": "审坡镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131122103",
                                        "values": "131122103000",
                                        "label": "赵桥镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131122104",
                                        "values": "131122104000",
                                        "label": "韩庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131122105",
                                        "values": "131122105000",
                                        "label": "肖桥头镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131122106",
                                        "values": "131122106000",
                                        "label": "龙店镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131122201",
                                        "values": "131122201000",
                                        "label": "圈头乡",
                                        "children": []
                                    },
                                    {
                                        "value": "131122203",
                                        "values": "131122203000",
                                        "label": "大紫塔乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "131123",
                                "values": "131123000000",
                                "label": "武强县",
                                "children": [
                                    {
                                        "value": "131123100",
                                        "values": "131123100000",
                                        "label": "武强镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131123101",
                                        "values": "131123101000",
                                        "label": "街关镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131123102",
                                        "values": "131123102000",
                                        "label": "周窝镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131123103",
                                        "values": "131123103000",
                                        "label": "东孙庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131123200",
                                        "values": "131123200000",
                                        "label": "豆村乡",
                                        "children": []
                                    },
                                    {
                                        "value": "131123201",
                                        "values": "131123201000",
                                        "label": "北代乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "131124",
                                "values": "131124000000",
                                "label": "饶阳县",
                                "children": [
                                    {
                                        "value": "131124100",
                                        "values": "131124100000",
                                        "label": "饶阳镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131124101",
                                        "values": "131124101000",
                                        "label": "大尹村镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131124102",
                                        "values": "131124102000",
                                        "label": "五公镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131124103",
                                        "values": "131124103000",
                                        "label": "大官亭镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131124104",
                                        "values": "131124104000",
                                        "label": "王同岳镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131124202",
                                        "values": "131124202000",
                                        "label": "留楚乡",
                                        "children": []
                                    },
                                    {
                                        "value": "131124203",
                                        "values": "131124203000",
                                        "label": "东里满乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "131125",
                                "values": "131125000000",
                                "label": "安平县",
                                "children": [
                                    {
                                        "value": "131125100",
                                        "values": "131125100000",
                                        "label": "安平镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131125101",
                                        "values": "131125101000",
                                        "label": "马店镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131125102",
                                        "values": "131125102000",
                                        "label": "南王庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131125103",
                                        "values": "131125103000",
                                        "label": "大子文镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131125104",
                                        "values": "131125104000",
                                        "label": "东黄城镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131125200",
                                        "values": "131125200000",
                                        "label": "大何庄乡",
                                        "children": []
                                    },
                                    {
                                        "value": "131125201",
                                        "values": "131125201000",
                                        "label": "程油子乡",
                                        "children": []
                                    },
                                    {
                                        "value": "131125202",
                                        "values": "131125202000",
                                        "label": "西两洼乡",
                                        "children": []
                                    },
                                    {
                                        "value": "131125400",
                                        "values": "131125400000",
                                        "label": "河北安平高新技术产业开发区",
                                        "children": []
                                    },
                                    {
                                        "value": "131125401",
                                        "values": "131125401000",
                                        "label": "安平县社区建设办公室",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "131126",
                                "values": "131126000000",
                                "label": "故城县",
                                "children": [
                                    {
                                        "value": "131126100",
                                        "values": "131126100000",
                                        "label": "郑口镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131126101",
                                        "values": "131126101000",
                                        "label": "夏庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131126102",
                                        "values": "131126102000",
                                        "label": "青罕镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131126103",
                                        "values": "131126103000",
                                        "label": "故城镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131126104",
                                        "values": "131126104000",
                                        "label": "武官寨镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131126105",
                                        "values": "131126105000",
                                        "label": "饶阳店镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131126106",
                                        "values": "131126106000",
                                        "label": "军屯镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131126107",
                                        "values": "131126107000",
                                        "label": "建国镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131126108",
                                        "values": "131126108000",
                                        "label": "西半屯镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131126109",
                                        "values": "131126109000",
                                        "label": "房庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131126110",
                                        "values": "131126110000",
                                        "label": "三朗镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131126200",
                                        "values": "131126200000",
                                        "label": "辛庄乡",
                                        "children": []
                                    },
                                    {
                                        "value": "131126201",
                                        "values": "131126201000",
                                        "label": "里老乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "131127",
                                "values": "131127000000",
                                "label": "景县",
                                "children": [
                                    {
                                        "value": "131127100",
                                        "values": "131127100000",
                                        "label": "景州镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131127101",
                                        "values": "131127101000",
                                        "label": "龙华镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131127102",
                                        "values": "131127102000",
                                        "label": "广川镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131127103",
                                        "values": "131127103000",
                                        "label": "王瞳镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131127104",
                                        "values": "131127104000",
                                        "label": "洚河流镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131127105",
                                        "values": "131127105000",
                                        "label": "安陵镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131127106",
                                        "values": "131127106000",
                                        "label": "杜桥镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131127107",
                                        "values": "131127107000",
                                        "label": "王谦寺镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131127108",
                                        "values": "131127108000",
                                        "label": "北留智镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131127109",
                                        "values": "131127109000",
                                        "label": "留智庙镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131127110",
                                        "values": "131127110000",
                                        "label": "梁集镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131127200",
                                        "values": "131127200000",
                                        "label": "刘集乡",
                                        "children": []
                                    },
                                    {
                                        "value": "131127201",
                                        "values": "131127201000",
                                        "label": "连镇乡",
                                        "children": []
                                    },
                                    {
                                        "value": "131127203",
                                        "values": "131127203000",
                                        "label": "温城乡",
                                        "children": []
                                    },
                                    {
                                        "value": "131127204",
                                        "values": "131127204000",
                                        "label": "后留名府乡",
                                        "children": []
                                    },
                                    {
                                        "value": "131127205",
                                        "values": "131127205000",
                                        "label": "青兰乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "131128",
                                "values": "131128000000",
                                "label": "阜城县",
                                "children": [
                                    {
                                        "value": "131128100",
                                        "values": "131128100000",
                                        "label": "阜城镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131128101",
                                        "values": "131128101000",
                                        "label": "古城镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131128102",
                                        "values": "131128102000",
                                        "label": "码头镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131128103",
                                        "values": "131128103000",
                                        "label": "霞口镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131128104",
                                        "values": "131128104000",
                                        "label": "崔家庙镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131128105",
                                        "values": "131128105000",
                                        "label": "漫河镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131128201",
                                        "values": "131128201000",
                                        "label": "建桥乡",
                                        "children": []
                                    },
                                    {
                                        "value": "131128202",
                                        "values": "131128202000",
                                        "label": "蒋坊乡",
                                        "children": []
                                    },
                                    {
                                        "value": "131128203",
                                        "values": "131128203000",
                                        "label": "大白乡",
                                        "children": []
                                    },
                                    {
                                        "value": "131128204",
                                        "values": "131128204000",
                                        "label": "王集乡",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "131171",
                                "values": "131171000000",
                                "label": "河北衡水高新技术产业开发区",
                                "children": [
                                    {
                                        "value": "131171201",
                                        "values": "131171201000",
                                        "label": "大麻森乡",
                                        "children": []
                                    },
                                    {
                                        "value": "131171400",
                                        "values": "131171400000",
                                        "label": "衡水高新技术产业开发区",
                                        "children": []
                                    },
                                    {
                                        "value": "131171401",
                                        "values": "131171401000",
                                        "label": "新型功能材料产业园",
                                        "children": []
                                    },
                                    {
                                        "value": "131171402",
                                        "values": "131171402000",
                                        "label": "苏正办事处",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "131172",
                                "values": "131172000000",
                                "label": "衡水滨湖新区",
                                "children": [
                                    {
                                        "value": "131172101",
                                        "values": "131172101000",
                                        "label": "魏家屯镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131172203",
                                        "values": "131172203000",
                                        "label": "彭杜村乡",
                                        "children": []
                                    },
                                    {
                                        "value": "131172401",
                                        "values": "131172401000",
                                        "label": "滨湖新区教育园区",
                                        "children": []
                                    }
                                ]
                            },
                            {
                                "value": "131182",
                                "values": "131182000000",
                                "label": "深州市",
                                "children": [
                                    {
                                        "value": "131182100",
                                        "values": "131182100000",
                                        "label": "唐奉镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131182101",
                                        "values": "131182101000",
                                        "label": "深州镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131182102",
                                        "values": "131182102000",
                                        "label": "辰时镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131182103",
                                        "values": "131182103000",
                                        "label": "榆科镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131182104",
                                        "values": "131182104000",
                                        "label": "魏家桥镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131182105",
                                        "values": "131182105000",
                                        "label": "大堤镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131182106",
                                        "values": "131182106000",
                                        "label": "前磨头镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131182107",
                                        "values": "131182107000",
                                        "label": "王家井镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131182108",
                                        "values": "131182108000",
                                        "label": "护驾迟镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131182109",
                                        "values": "131182109000",
                                        "label": "大屯镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131182110",
                                        "values": "131182110000",
                                        "label": "高古庄镇",
                                        "children": []
                                    },
                                    {
                                        "value": "131182200",
                                        "values": "131182200000",
                                        "label": "兵曹乡",
                                        "children": []
                                    },
                                    {
                                        "value": "131182201",
                                        "values": "131182201000",
                                        "label": "穆村乡",
                                        "children": []
                                    },
                                    {
                                        "value": "131182202",
                                        "values": "131182202000",
                                        "label": "东安庄乡",
                                        "children": []
                                    },
                                    {
                                        "value": "131182203",
                                        "values": "131182203000",
                                        "label": "北溪村乡",
                                        "children": []
                                    },
                                    {
                                        "value": "131182204",
                                        "values": "131182204000",
                                        "label": "大冯营乡",
                                        "children": []
                                    },
                                    {
                                        "value": "131182206",
                                        "values": "131182206000",
                                        "label": "乔屯乡",
                                        "children": []
                                    },
                                    {
                                        "value": "131182400",
                                        "values": "131182400000",
                                        "label": "河北省深州监狱",
                                        "children": []
                                    },
                                    {
                                        "value": "131182401",
                                        "values": "131182401000",
                                        "label": "深州市城市新区管理委员会",
                                        "children": []
                                    },
                                    {
                                        "value": "131182403",
                                        "values": "131182403000",
                                        "label": "经济开发区",
                                        "children": []
                                    }
                                ]
                            }
                        ]
                    }
                ]
        }
    },
    methods: {}
}
export default mixin