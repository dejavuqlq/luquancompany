import router from '../router'

const mixin = {
  data() {
    return {
      /* router跳转相关*/
      // pathMixin:"",
      queryParamsMixin: null
    }
  },
  created() {
    // 通用获取页面参数
    // console.log(this.$route)
    // this.pathMixin = this.$route
    if (this.$route?.query?.params) {
      this.queryParamsMixin = JSON.parse(this.$route.query.params)
    }
    document.title = this.$route.meta.title || ""
  },
  methods: {
    // vue router跳转
    routerPushMixin(name, params) {
      router.push({
        path: name,
        query: params
      })
    },
    routerPushNewMixin(name, params) {
      let routeData = router.resolve({ path:name, query: params });
      window.open(routeData.href, '_blank');
    },
    routerReplaceMixin(name, params) {
      router.replace({
        name: name,
        query: params
      })
    },
    routerBackMixin() {
      router.go(-1)
    }
  }
}

export default mixin
