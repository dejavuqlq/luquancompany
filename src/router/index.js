import { createRouter, createWebHistory } from 'vue-router'
import mainChildren from './serverMenu'
import shareChildren from './setShare'

const routes = [
  { path: '/', component: () => import('../views/login.vue'), meta: { title: '登录' } },
  { 
    path: '/main',
    component: () => import('../views/main.vue'),
    meta: { title: '企业后台' },
    children: mainChildren
  },
  { path: '/menu', component: () => import('../views/menu.vue'), meta: { title: '整理格式' } },
  { 
    path: '/share',
    component: () => import('../views/share.vue'),
    meta: { title: '企融平台' },
    children: shareChildren
  },
  { 
    path: '/chartpersion',
    component: () => import('../views/chartpersion/index.vue'),
    meta: { title: '聊天咨询', keepAlive:true },
    children: [
      { 
        path: '/chartpersiondetail',
        component: () => import('../views/chartpersion/chart.vue'),
        meta: { title: '人才咨询', keepAlive:true },
        children: []
      }
    ]
  },
  { 
    path: '/chartmechanism',
    component: () => import('../views/chartmechanism/index.vue'),
    meta: { title: '聊天咨询', keepAlive:true },
    children: [
      { 
        path: '/chartmechanismdetail',
        component: () => import('../views/chartmechanism/chart.vue'),
        meta: { title: '机构咨询', keepAlive:true },
        children: []
      }
    ]
  },
]
const router = createRouter({
  history: createWebHistory(),
  routes
})
export default router