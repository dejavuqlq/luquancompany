const mainChildren = [
    {
        path: "",
        component: () => import('../pages/home.vue'),
        meta: { title: "欢迎" },
    },{
        path: "/staff",
        component: () => import('../pages/staff/list.vue'),
        meta: { title: "员工信息" },
    },{
        path: "/recruit",
        component: () => import('../pages/recruit/recruit.vue'),
        meta: { title: "招聘信息", keepAlive:true },
    },{
        path: "/persion",
        component: () => import('../pages/recruit/persion.vue'),
        meta: { title: "人才信息" },
    },{
        path: "/applyfor",
        component: () => import('../pages/recruit/apply.vue'),
        meta: { title: "应聘信息" },
    },
    // {
    //     path: "/chart",
    //     component: () => import('../pages/chart/index.vue'),
    //     meta: { title: "在线聊天" }
    // },
    {
        path: "/my_train",
        component: () => import('../pages/my/train.vue'),
        meta: { title: "培训课程" },
    },{
        path: "/my_jobfair",
        component: () => import('../pages/my/jobfair.vue'),
        meta: { title: "招聘会" },
    },{
        path: "/my_persion",
        component: () => import('../pages/my/persion.vue'),
        meta: { title: "我的人才库" },
    },{
        path: "/news",
        component: () => import('../pages/news/list.vue'),
        meta: { title: "园区资讯" },
    },{
        path: "/docking",
        component: () => import('../pages/news/docking.vue'),
        meta: { title: "融资信息发布" },
    },{
        path: "/banking",
        component: () => import('../pages/public/banking.vue'),
        meta: { title: "金融产品" },
    },{
        path: "/policy",
        component: () => import('../pages/public/policy.vue'),
        meta: { title: "金融政策" },
    },{
        path: "/insurance",
        component: () => import('../pages/public/insurance.vue'),
        meta: { title: "保险产品" },
    },{
        path: "/train",
        component: () => import('../pages/public/train.vue'),
        meta: { title: "培训课程" },
    },{
        path: "/video",
        component: () => import('../pages/public/video.vue'),
        meta: { title: "视频课程" },
    },{
        path: "/jobfair",
        component: () => import('../pages/public/jobfair.vue'),
        meta: { title: "招聘会" },
    },{
        path: "/parking",
        component: () => import('../pages/property/parking.vue'),
        meta: { title: "车位租赁申请" },
    },{
        path: "/repair",
        component: () => import('../pages/property/repair.vue'),
        meta: { title: "物业报修" },
    },{
        path: "/life",
        component: () => import('../pages/property/life.vue'),
        meta: { title: "生活服务" },
    },{
        path: "/visitor",
        component: () => import('../pages/visitor/visitor.vue'),
        meta: { title: "访客申请列表" },
    }
]

export default mainChildren