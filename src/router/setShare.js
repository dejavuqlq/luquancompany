const shareChildren = [
    {
        path: "/share/recruit",
        component: () => import('../pages/share/recruit/list.vue'),
        meta: { title: "招聘信息" },
        children: []
    },{
        path: "/share/recruit/detail",
        component: () => import('../pages/share/recruit/detail.vue'),
        meta: { title: "金融产品详情" },
    },{
        path: "/share/policy",
        component: () => import('../pages/share/policy/list.vue'),
        meta: { title: "金融政策" },
        children: []
    },{
        path: "/share/policy/detail",
        component: () => import('../pages/share/policy/detail.vue'),
        meta: { title: "金融政策详情" },
    },{
        path: "/share/banking",
        component: () => import('../pages/share/banking/list.vue'),
        meta: { title: "金融产品" },
        children: []
    },{
        path: "/share/banking/detail",
        component: () => import('../pages/share/banking/detail.vue'),
        meta: { title: "金融产品详情" },
    },{
        path: "/share/train",
        component: () => import('../pages/share/train/list.vue'),
        meta: { title: "培训课程" },
        children: []
    },{
        path: "/share/train/detail",
        component: () => import('../pages/share/train/detail.vue'),
        meta: { title: "培训课程详情" },
    },
]

export default shareChildren