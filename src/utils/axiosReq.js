import axios from 'axios'
import { ElLoading, ElMessage } from 'element-plus'
import settings from '../setting'
import router from '../router'
import utils from './utils.js'
// import 'element-plus/theme-chalk/elmessage.css'
// import { getToken, setToken } from '@/utils/auth'
let requestData
let loadingE

const service = axios.create({
  // baseURL: "http://localhost:8081/admin",
  // baseURL: globalUrl.server,
  timeout: 5000 // 超时时间
})

// 请求拦截
service.interceptors.request.use(
  (request) => {
    // console.log('request', request)
    // token配置
    request.headers['token'] = sessionStorage.getItem("token") || ""
    /* 下载文件*/
    if (request.isDownLoadFile) {
      request.responseType = 'blob'
    }
    if (request.isUploadFile) {
      request.headers['Content-Type'] = 'multipart/form-data'
    }
    if(request.progress){
      request.onUploadProgress = request.progress
    } 
    requestData = request
    if (request.bfLoading) {
      loadingE = ElLoading.service({
        lock: true,
        text: '数据载入中',
        spinner: 'el-icon-loading',
        background: 'rgba(0, 0, 0, 0.1)'
      })
    }
    /*
     *params会拼接到url上
     * */
    if (request.isParams) {
      request.params = request.data
      request.data = {}
    }
    return request
  },
  (err) => {
    Promise.reject(err)
  }
)
// 响应拦截
service.interceptors.response.use(
  (res) => {
    if (requestData.afHLoading && loadingE) {
      loadingE.close()
    }
    // 如果是下载文件直接返回
    if (requestData.isDownLoadFile) {
      return res.data
    }
    const { success, err } = res.data
    if (!success && requestData.isAlertErrorMsg) { //失败，且前端指定要弹框
      console.log(err);
      if(err == "登录已过期，请重新登录" || err == "token无效" || err == "token失效"){
        utils.debounce(()=>{
          ElMessage({ message: `${err} 请重新登录`, type: 'error', duration: 2 * 1000 })
        })
        router.push({path: '/',query: {}})
      }else{
        ElMessage({ message: err, type: 'error', duration: 2 * 1000 })
      }
      return Promise.reject(err)
    }
    return res.data
  },
  (err) => {
    if (loadingE) loadingE.close()
    if(err == "登录已过期，请重新登录" || err == "token无效" || err == "token失效"){
      utils.debounce(()=>{
        ElMessage({ message: `${err} 请重新登录`, type: 'error', duration: 2 * 1000 })
      })
      router.push({path: '/',query: {}})
    }else{
      ElMessage({ message: err, type: 'error', duration: 2 * 1000 })
    }
    
    return Promise.reject(err)
  }
)

export default function khReqMethod({
  url,
  data,
  method,
  progress,
  isParams,
  bfLoading,
  afHLoading,
  isUploadFile,
  isDownLoadFile,
  baseURL,
  timeout,
  isAlertErrorMsg
}) {
  return service({
    url: url,
    method: method ?? 'post',
    data: data ?? {},
    progress: progress,
    isParams: isParams ?? false,
    bfLoading: bfLoading ?? true,
    afHLoading: afHLoading ?? true,
    isUploadFile: isUploadFile ?? false,
    isDownLoadFile: isDownLoadFile ?? false,
    isAlertErrorMsg: isAlertErrorMsg ?? true,
    baseURL: baseURL ? baseURL : (settings.globalUrl.server+"/company" ?? import.meta.env.VITE_APP_BASE_URL), // 设置基本基础url
    timeout: timeout // 配置默认超时时间
  })
}
