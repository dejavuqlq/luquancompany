let timeout = null;
export default {
  getManagement(proxy) {
    return new Promise((resolve, reject) => {
        proxy.$ajax({
          baseURL:proxy.$setting.globalUrl.base,
          url: 'root/management/lists',
          data: {},
          bfLoading: true
        }).then(d => {
            resolve(d.msg.list)
        }).catch(e => {
            reject(e)
        })
    })
  },
  getGarden(proxy,id) {
    return new Promise((resolve, reject) => {
        proxy.$ajax({
          baseURL:proxy.$setting.globalUrl.base,
          url: 'root/garden/lists',
          data: {management_id:id},
          bfLoading: true
        }).then(d => {
            resolve(d.msg.list)
        }).catch(e => {
            reject(e)
        })
    })
  },
  getNewsType(proxy) {
    return new Promise((resolve, reject) => {
        proxy.$ajax({
          baseURL:proxy.$setting.globalUrl.base,
          url: 'root/newstype/lists',
          data: {},
          bfLoading: true
        }).then(d => {
            resolve(d.msg.list)
        }).catch(e => {
            reject(e)
        })
    })
  },
  getFinancialPolicyType(proxy) {
    return new Promise((resolve, reject) => {
      proxy.$ajax({
        baseURL:proxy.$setting.globalUrl.base,
        url: 'root/financialpolicytype/lists',
        data: {},
        bfLoading: true
      }).then(d => {
          resolve(d.msg.list)
      }).catch(e => {
          reject(e)
      })
  })
  },
  
  getProperty(proxy, type) {
    return new Promise((resolve, reject) => {
        proxy.$ajax({
          url: 'property/lists',
          data: { type: type },
          bfLoading: true
        }).then(d => {
            resolve(d.msg.list)
        }).catch(e => {
            reject(e)
        })
    })
  },
  getDevice(proxy) {
    return new Promise((resolve, reject) => {
        proxy.$ajax({
          url: 'device/lists',
          data: { },
          bfLoading: true
        }).then(d => {
            resolve(d.msg.list)
        }).catch(e => {
            reject(e)
        })
    })
  },
  xlsImport(proxy,filename) {
    return new Promise((resolve, reject) => {
        proxy.$ajax({
          baseURL:proxy.$setting.globalUrl.base,
          url: 'root/xlsImport',
          data: {filename:filename},
          bfLoading: true
        }).then(d => {
            resolve(d.msg.list)
        }).catch(e => {
            reject(e)
        })
    })
  },

  showImgs(proxy, imgs, target) {
    if(imgs!=''){
      return imgs.split(',').map(t => {
          return `<a href="${proxy.$setting.globalUrl.fileurl}/${target}/${t}" target="_blank"><img src="${proxy.$setting.globalUrl.fileurl}/${target}/${t}" /></a>`
      }).join()
    }
    return ""
  },
  find(list, value ){
    let tmp = list.find(t => t.value==value)
    if(tmp) return tmp.text
    return ""
  },

  padding(num, length) {
    //这里用slice和substr均可
    return (Array(length).join("0") + num).slice(-length);
  },

  dateFormat(d,fmt) {
    var o = {                  //年
      "M+" : d.getMonth()+1,                 //月份 
      "d+" : d.getDate(),                    //日 
      "h+" : d.getHours(),                   //小时 
      "m+" : d.getMinutes(),                 //分 
      "s+" : d.getSeconds(),                 //秒 
      "q+" : Math.floor((d.getMonth()+3)/3), //季度 
      "S" : d.getMilliseconds()             //毫秒 
      }; 
      
      if(/(y+)/.test(fmt)) {
        fmt=fmt.replace(RegExp.$1, (d.getFullYear()+"").substr(4 - RegExp.$1.length)); 
      }
      
      for(var k in o) {
        if(new RegExp("("+ k +")").test(fmt)){
          fmt = fmt.replace(RegExp.$1, (RegExp.$1.length==1) ? (o[k]) : (("00"+ o[k]).substr((""+ o[k]).length)));
        }
      }
      
      return fmt;
  },

  //获取当月最后一天的日期
  getLastDay(){
    var y = new Date().getFullYear(); //获取年份
    var m = new Date().getMonth() + 1; //获取月份
    var d = new Date(y, m, 0).getDate(); //获取当月最后一日
    m = m < 10 ? '0' + m : m; //月份补 0
    d = d < 10 ? '0' + d : d; //日数补 0
    return [y,m,d].join("-")
  },

  //获取当月第一天的日期
  getFirstDay(){
    var y = new Date().getFullYear(); //获取年份
    var m = new Date().getMonth() + 1; //获取月份
    var d = '1'; //获取当月第一日
    m = m < 10 ? '0' + m : m; //月份补 0
    d = d < 10 ? '0' + d : d; //日数补 0
    return [y,m,d].join("-")
  },

  dateAddMinute(d,minute,fmt){
    let nd = new Date(d.getTime()+minute*60*1000);
    if(fmt)
      return this.dateFormat(nd, fmt)
    return nd
  },
  dateAddHour(d,hour,fmt){
    let nd = new Date(d.getTime()+hour*60*60*1000);
    if(fmt)
      return this.dateFormat(nd, fmt)
    return nd
  },
  dateAddDay(d,day,fmt){
    let nd = new Date(d.getTime()+day*24*60*60*1000)
    if(fmt)
      return this.dateFormat(nd, fmt)
    return nd
  },
  dateAddMonth(d,month,fmt){
    
  },
  dateAddYear(d,year,fmt){
    
  },
  removeHTML(str, len){
    if (str == undefined || str == "") return "";
    str = str.replace(/<[^>]+>/g,' ');
    if(len){
      if(str.length > len){
        str = str.substr(0,len) + "...";
      }
    } 
    return str;
  },
  getColor() {
    return "#"+Math.floor(Math.random()*0xffffff).toString(16)
  },

  // 比较计算时间间隔
 compareTimeInterval(time1,time2,time){
    if(!time1||!time2) return false
    let d1 = new Date(time1.replace(/-/g,'/'))
    let t1 = d1.getTime()
    let d2 = new Date(time2.replace(/-/g,'/'))
    let t2 = d2.getTime()
    const t = time||600000
    return t1-t2> t?true:false
  },

  
  debounce(func,obj,wait = 500, immediate = false) {
    // 清除定时器
    if (timeout !== null) clearTimeout(timeout);
    // 立即执行，此类情况一般用不到
    if (immediate) {
      var callNow = !timeout;
      timeout = setTimeout(function() {
        timeout = null;
      }, wait);
      if (callNow) typeof func === 'function' && (obj?func(obj):func());
    } else {
      // 设置定时器，当最后一次操作后，timeout不会再被清除，所以在延时wait毫秒后执行func回调方法
      timeout = setTimeout(function() {
        // console.log(typeof func)
        typeof func === 'function' && (obj?func(obj):func());
      }, wait);
    }
  },

  // 通过值查找到对象 并返回 非嵌套
  findObj(value,arr,key){
    if(value==null||value==undefined||value==''){
      return {}
    }
    let v = value
    if(typeof value == 'object'){
      v = value[0]
    }
    const obj = arr.find(function (o) {
      return o[key||'id'] === v
    })
    return obj||{}
  },

  // 是否过期/未到时间 0未到时间 1在时间范围内 2过期
  checkMeetTime(dtime,start,end,tip) {
    if(!dtime || !start || !end){
      return '未传入时间数据'
    }
    let dtimeFormat = isNaN(dtime);
    let startFormat = isNaN(start); //true 2020-12-15格式  false 毫秒格式
    let endFormat = isNaN(end);
    
    let dateTime = dtimeFormat?new Date(dtime.replace(/-/g,'/')).getTime():dtime;
    let startTime = startFormat?new Date(start.replace(/-/g,'/')).getTime():start;
    let endTime = endFormat?new Date(end.replace(/-/g,'/')).getTime():end;
    if(dateTime<startTime){//小于开始时间
      return 0;
    }else if(dateTime>=startTime&&dateTime<endTime){
      return 1;
    }else if(dateTime>=endTime){
      return 2;
    }
    return 3;
  }
}