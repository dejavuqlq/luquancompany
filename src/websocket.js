import Stomp from "stompjs"
import { ref, reactive } from 'vue'
let baseURL = 'ws://smartgarden.server.zh8848.top/chat';
let subscribeList = [];

export default {
	monitordata: ref({}),
	stompClient: null,
    init() {
        return new Promise((resolve, reject) => {
			this.stompClient = Stomp.client(baseURL);
			this.stompClient.connect({},
				success => {
					console.log("websoket服务器连接成功...");
					resolve(this.stompClient)
					setInterval(()=>{this.stompClient.send('');},5000)
					//监听广播
					const userType = sessionStorage.getItem("userType");
					
				},
				err =>{
					console.log("websoket服务器断开了...");
					this.init();
				}
			);
		})
    },
	addSubscribe(title){
		this.stompClient.subscribe(title, res=> {
			const body = JSON.parse(res.body)
			this.monitordata.value = body;
		})
	},
	delSubscribe(title){
	},
    disconnect() {
    },
    sendMessage(message) {
    },
    closeSocket() {
    },
};
