import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import path from "path";

// import ElementPlus from 'unplugin-element-plus/vite'

// import AutoImport from 'unplugin-auto-import/vite'
// import Components from 'unplugin-vue-components/vite'
// import { ElementPlusResolver } from 'unplugin-vue-components/resolvers'

import styleImport from 'vite-plugin-style-import'
import ViteComponents, { ElementPlusResolver } from 'vite-plugin-components'


// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    // ElementPlus({
    //   useSource: true,
    // }),

    // AutoImport({
    //   resolvers: [ElementPlusResolver()],
    // }),
    // Components({
    //   resolvers: [ElementPlusResolver()],
    // }),
    
    ViteComponents({
      customComponentResolvers: [ElementPlusResolver()],
    }),
    styleImport({
        libs: [
            {
                libraryName: 'element-plus',
                esModule: true,
                resolveStyle: name => {
                    return `element-plus/theme-chalk/${name}.css`
                },
            },
        ],
    }),
  ],
  resolve: {
    alias: {
      "@": path.resolve(__dirname, "./src") // map '@' to './src' 
    },
  },
  server:{
    port:8800
  },
  build:{
    brotliSize: false, // 消除打包大小超过500kb警告
    // assetsDir: 'assets/img',
  }
})
